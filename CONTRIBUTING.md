# Contributing

## How to contribute
If you want to help improving this project please try to follow this workflow:

* Clone the project to your local computer.
* Create an [GitLab issue](https://codebase.helmholtz.cloud/aida/aida_experiment_analysis/-/issues) for the task you want to work on.
* Create a new branch for that specific issue.
* Work on the issue and make sure to follow the guidelines for code style and documentation.
* Create a merge request.
* Get your branch merged into the master branch by a maintainer ... done.

This project was written using the integrated development environment (IDE) [spyder](https://www.spyder-ide.org/).
Nevertheless, any other development environment works as well.


### Code style guidelines
The project follows the code style proposed in

- [PEP8](https://www.python.org/dev/peps/pep-0008/) (Style Guide for Python Code)
- [PEP20](https://www.python.org/dev/peps/pep-0020/) (The Zen of Python)
- [NumPy docstring guide](https://numpydoc.readthedocs.io/en/latest/format.html), [NumPy docstring style example](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_numpy.html)


## Contributing persons
This file acknowledges the people who contributed to this project.

* Tobias Schorr (main author 2019-2023)
* Alexander Böhmländer
* Barbara Bertozzi
* Julia Schneider


## Acknowledgments
* Older IDL analysis scripts were used as a guidance to write major parts of the data import and parts of the data processing. Those scripts were originally written by Ottmar Möhler under a GPL license and later on maintained by Romy Fösig.
