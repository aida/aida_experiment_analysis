# aida_experiment_analysis README
This python project contains scripts to analyze and visualize data from the AIDA cloud chamber at [IMK-AAF](https://www.imk-aaf.kit.edu/) ([KIT](https://www.kit.edu/english/)).

---

## Short description of main files and functionalities

### calc_aida_all.py
This file includes the **main analysis routine** for AIDA experiments.
The function returns a dictionary containing all data, both raw and processed. It can be saved as *.pkl file.

### plot_aida_all.py
The code in this file generates an overview plot for data from `calc_aida_all()`. Through True/False flags one can choose which data to visualize.

### calc_inas.py
Calculate ice-nucleating active site (INAS) densities as a trajectory during an expansion experiment.

### wall_flux.calc_aida_wall_flux.py
Determine wall flux of temperature and water vapor pressure between AIDA gas and walls. Note, water vapor wall flux can only be determined through "reference activations", since ice nucleation acts as an additional sink of water which would be attributed to the wall flux.

### toolbox.py
A collection of useful functions that can be imported and used in other scripts.

### tools_import.py
A collection of functions for data import from different instruments.

### tools_parametrizations.py
This file includes parametrizations like for was vapor pressure of the homogeneous freezing threshold.

---

## How to setup

### Clone or download the project
There are two possible ways to import the project to your computer.
Via clone you can seamlessly interact with git, however a simple download is useable as well.

Clone the project by running
```shell
$ git clone https://codebase.helmholtz.cloud/aida/aida_experiment_analysis.git
```
or simply download the project as a zip file and unzip it on your computer.

### Install dependencies
The AIDA database runs on mariadb. The respective python package needs the *MariaCB Connector/C* installed on your system to work.
You can download it [here](https://mariadb.com/docs/skysql-previous-release/connect/programming-languages/c/install/#overview) for various platforms.
Make sure you choose the CS (Community Server) repository.

Note for Arch (and derivatives) users:  
This platform is not officially supported, however you can find a respective package in the Arch User Repository (AUR).

### Setup a fresh python environment
To avoid conflicting package versions the recommended way is to setup a new python environment using provided files.

#### with pip
If you use pip as package manager ensure you have the python package `virtualenv` installed. This can be done via pip
```shell
pip install virtualenv
```
or directly as the system package `python-virtualenv`.

Then you can setup a virtual python environment `venv` and install the dependencies with
```shell
$ python -m virtualenv venv
$ source venv/bin/activate
(venv) $ pip install -r requirements.txt
```
Note, you can also use the file requirements_dev.txt to add some development packages.


#### with conda
If you use conda as package manager you can also create a new pyhton environment with a prepared file:
```shell
$ conda env create -f environment.yml
$ conda activate pyAIDA
(pyAIDA) $
```

Note, the `environment.yml` file was created by running
```shell
$ conda env export > environment.yml
```

## Contributing

**Any contribution to this project is welcome!**

In the file `CONTRIBUTING.md` you can find:
* A basic guide about how to contribute.
* A list of all contributing persons.
* Further acknowledgements.


---

## License
Copyright (C) 2019-2023 KIT/IMK-AAF
(Karlsruhe Institute of Technology, Institute of Meteorology and
Climate Research, Atmospheric Aerosol Research)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License can be found in the LICENSE file that
comes along with this program, or see <http://www.gnu.org/licenses/>.
