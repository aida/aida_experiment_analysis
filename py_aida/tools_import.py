#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Collection of scripts to import data from different sources.

This file is part of the project `py_aida`.
Copyright (c) 2019-2023, GPL v3, KIT/IMK-AAF
(Karlsruhe Institute of Technology, Institute of Meteorology and
Climate Research, Atmospheric Aerosol Research)
For full license details see the file LICENSE.md or
go to https://www.gnu.org/licenses/gpl-3.0.html.
"""

# %% Import data from .ods-sheet


def ods2df(ods_filename, verbose=False):
    """Import content from .ods spreadsheet (LibreOffice) into a pd.DataFrame.
    
    Parameters
    ----------
    ods_filename = :obj:`str`
        path of .ods file
    verbose : :obj:`bool`, *optional*
        Print informational statements during function execution
    
    Returns
    -------
    :obj:`pd.DataFrame`
        contains data from spreadsheet
    
    Notes
    -----
    #. Column names / header informatio will be taken from first row of .ods file
    #. Index column of output DataFrame is not specified
    #. Script reads only filled cells; if empty distrance in ods file is too large the content can be cut off!
    
    Examples
    --------
    >>> df = ods2df('./exp_concentrations.ods')
    >>> df
         flag   Campaign  ExpIndex  ...  RHi_ice_onset_hom fice_het fice_hom
    0     0.0  AWICIT01B    3590.0  ...                NaN      NaN      NaN
    1     NaN  AWICIT01B    3591.0  ...                NaN      NaN      NaN
    2     NaN  AWICIT01B    3592.0  ...                NaN      NaN      NaN
    3     0.0  AWICIT01B    3593.0  ...                NaN      NaN      NaN
    [...]
    """
    import pandas as pd
    import ezodf
    from py_aida.toolbox import get_ts
    import warnings
    
    warnings.warn("You can now use pd.read_excel() also for.ods files."
                  "Please switch to that function to reduce dependencies.", DeprecationWarning)
    
    doc = ezodf.opendoc(ods_filename)
    
    if verbose:
        print(get_ts(), "Imported spreadsheet '" + ods_filename + "' contains %d sheet(s)." % len(doc.sheets))
        for sheet in doc.sheets:
            print("-" * 40)
            print("   Sheet name : '%s'" % sheet.name)
            print("Size of Sheet : (rows=%d, cols=%d)" % (sheet.nrows(), sheet.ncols()))
    
    # convert the first sheet to a pandas.DataFrame
    sheet = doc.sheets[0]
    df_dict = {}
    for i, row in enumerate(sheet.rows()):
        # row is a list of cells
        # assume the header is on the first row
        if i == 0:
            # columns as lists in a dictionary
            df_dict = {cell.value: [] for cell in row}
            # create index for the column headers
            col_index = {j: cell.value for j, cell in enumerate(row)}
            continue
        for j, cell in enumerate(row):
            # use header instead of column index
            df_dict[col_index[j]].append(cell.value)
            
    # debug: delete empty column
    del df_dict[None]
    
    # and convert to a DataFrame
    df = pd.DataFrame(df_dict)

    return df
    

# %% Import mySQL database entries
    
def sql_query(database, query, username=None, password=None,
              ip='141.52.172.3', port='3306', output_mode='df',
              df_index_col=None, verbose=False):
    """Fetch data from IMK-AAF MySQL database, returns pd.DataFrame or dictionary.
    
    Parameters
    ----------
    database : str
        Database name, e.g. *'aida'* or *'device_data'* or *'naua'*.
    query : str
        SQL query to fetch data from database, e.g.
    >>> query = "SELECT * FROM in_ergebnisse WHERE Kampagne = 'AWICIT04'"

    username : str
        user name to authenticate to SQL DB
    password : str
        Password corresponding to username to authenticate to SQL DB.
    ip : str
        IP address for SQL database
    port : str
        Port number to address SQL database

    output_mode : `str` ('df' or 'dict')
        mode = 'df'
            Returns multiple rows as DataFrame.
        mode = 'dict'
            Returns a single row as dictionary.
    df_index_col : str
        Specify which column name in *'select'* should be taken as :obj:`pd.DataFrame.index`.
    
    Returns
    -------
    :obj:`pd.DataFrame` or `dict`
        contains all data fetched via SQL query,
    
    with:
        
        :obj:`pd.DataFrame.columns`
            defined by 'select'
        :obj:`pd.DataFrame.index`
            column defined by 'df_index_col'
    
    Examples
    --------
    >>> q = sql_query('aida', 'in_ergebnisse', select='*',
    ...         where="Kampagne LIKE 'AWICIT04'", df_index_col = 'tref')
    >>> q
                         index  ExperimentID  ... modus_cpc3776  modus_cpc3772
    tref                                      ...
    2018-01-12 10:53:00   3914          3862  ...           0.0            0.0
    2018-01-12 15:56:00   3915          3863  ...           0.0            0.0
    2018-01-15 08:47:20   3916          3864  ...           0.0            0.0
    2018-01-15 10:26:00   3917          3865  ...           0.0            0.0
    ...
    """
    import os
    import warnings
    import getpass
    import pandas as pd
    import sqlalchemy
    from dotenv import load_dotenv, find_dotenv
    
    # a) first check if credentials are already stored in environment variables
    username = os.getenv('MARIADB_USERNAME')
    password = os.getenv('MARIADB_PASSWORD')
    
    # b) try to find credentials in .env file
    if not username and not password:
        load_dotenv(find_dotenv())
        username = os.getenv('MARIADB_USERNAME')
        password = os.getenv('MARIADB_PASSWORD')
        
        if verbose and (not username or not password):
            warnings.warn("Could not read database credentials from .env file. "
                          "Please set environment variables `MARIADB_USERNAME` and `MARIADB_PASSWORD` in .env file.")
    
    # c) ask for credentials and save in environment variables
    if not username and not password:
        username = input("To access the mariaDB database, please enter your username:\n")
        os.environ['MARIADB_USERNAME'] = username
        password = getpass.getpass('\nTo access the mariaDB database, please enter your password:\n')
        os.environ['MARIADB_PASSWORD'] = password
    
    database_url = f"mariadb+mariadbconnector://{username}:{password}@{ip}:{port}/{database}"
    engine = sqlalchemy.create_engine(database_url)
    
    # fetch multiple rows of data and return as pd.DataFrame
    result = pd.read_sql(query, engine, index_col=df_index_col)
    engine.dispose()  # clean up connection
    
    # try to return single returned row as dictionary
    if output_mode == 'dict':
        n_rows = len(result)
        if n_rows == 0:
            result = dict()
        elif n_rows == 1:
            result = result.iloc[0].to_dict()  # raises error if len>1
        elif n_rows > 1:
            raise ValueError(f"Expected a single row from database, but got {len(result)} rows.")
    
    return result


# %% Import IDL-generated .sav-file, which include all data for a experiment

def sav2dict(path='./'):
    """
    Import AIDA data from .sav file created by IDL, returns dictionary.

    Parameters
    ----------
    path : :obj:`str`
        path to .sav file, typically includes *Campaign* and *ExpNo*
    
    Returns
    -------
    Sav : :obj:`dict`
        experimental parameters and data can be adressed through keys in Sav.keys()
    
    References
    ----------
    For source of .sav files, see IDL routine *calc_aida_all.pro*
    
    Notes
    -----
    * Field names can be displayed by
        >>> Sav['data'].dtype.names
    * Data can be adressed by
        >>> Sav['data'][<field name>]
    * if file lies on a Windows server the path differs depending on OS (linux vs windows vs iOS)
        example path for ubuntu: '/run/user/1000/gvfs/smb-share:server=imkaaf-srv1,share=aida-mess/Messkampagnen/AWICIT04/DataSets/AWICIT04_aida_sav/AWICIT04_10_aida.sav'
    
    Examples
    --------
    >>> sav_file_path = '/run/user/1000/gvfs/smb-share:server=imkaaf-srv1,share=aida-mess/Messkampagnen/'+ Campaign + '/DataSets/' + Campaign + '_aida_sav/' + Campaign + '_' + ExpNo + '_aida.sav'
    >>> Sav = sav2dict(sav_file_path)
    >>> ImportElements = ['cn_welas', 'cn_welas_dmin', 'trel_welas', 'cn_welas2',
    ...                   'cn_welas2_dmin', 'trel_welas2']
    >>> Sav_dict = {}
    >>> for element in ImportElements:
    ...     Sav_dict[element] = Sav[element][0]*1
    """
    from scipy.io import readsav
    
    infile = readsav(file_name=path, python_dict=True, verbose=False)
    Sav = infile["data"]
    
    return Sav


# %% data import via smb server connection

def smb_data_import(ip, share, file_path, user='', pw='', separator='\t', verbose=False):
    r"""Import structured text file from server through SMB connection.
    
    This function is written for and tested with welas single particle data.
    
    Parameters
    ----------
    ip : :obj:`str`
        if of server to be adressed, e.g. *'141.52.172.9'* for IMKAAF-SRV1
    share : :obj:`str`
        Name of the share volume on server, e.g. *'opc'* for welas spd
    file_path : :obj:`str`
        file path continuing after share, e.g. *'WELAS_Data/PCS_Lin/AWICIT04/Awicit04_180116_03_welas1.txt'*
    user : :obj:`str`
        user shortcut for authentification with the server, e.g. KIT login in format *'ab1234'*
    pw : :obj:`str`
        password corresponding to account *'user'*
    separator : :obj:`str`
        define separator of structured data the file, e.g. tab r'\\t'
    verbose : :obj:`bool`, *optional*
        Print informational statements during function execution
    
    Returns
    -------
    :obj:`pd.DataFrame`
        contains data from text file.
        
        For import of welas single particle data:
        
            :obj:`pd.DataFrame.columns`
                ['<start_timestamp>', 'up1', 'up2', 'Ti']
                
                * <start_timestamp>: microseconds since <start_timestamp>
                * up1 : channel number (1) *(default)*
                * up2 : channel number (2)
                * Ti : pulse length, in 1e-5 seconds
            
            :obj:`pd.DataFrame.index`
                RangeIndex(start=0, stop=<len(data)>, step=1)
                
    See Also
    --------
    :func:`read_welas_spdfile`, for application of this script
    
    :func:`calc_aida_all.convert_welas_spd2sd` for further data processing.
    
    :func:`smb.SMBHandler`
    
    :mod:`keyring`, used to save passwort securely in OS keyring
    
    Notes
    -----
    * first row of text file gets treated as header and used for column names in output
    * password can be saved in local OS keyring, tested with Linux Mint 18.2, should also work on other OS
    * privacy-friendly password input is not possible in Qt console like spyder
    * Default separator is '\t' for welas single particle data, but this script can be also used for all other txt/csv/dat files
    
    Examples
    --------
    >>> data = smb_data_import(ip = '141.52.172.9', share = 'opc',
                               file_path = 'WELAS_Data/PCS_Lin/AWICIT04/Awicit04_180116_03_welas1.txt',
                               user = '', pw='')
    >>> data.head()
       17:19:36  up1  up2   Ti
    0     80750   20   21  213
    1     80990   13   13  179
    2     89780   26   27  217
    3     89930   13   13  180
    4     90430   17   18  197
    """
    import os
    import pandas as pd
    import getpass
    from dotenv import load_dotenv, find_dotenv
    from py_aida.toolbox import get_ts
    
    def prompt_smb_username():
        user = input("Please enter user name (like 'ab1234') for data import via SMB:\n")
        return user
    
    def prompt_smb_password():
        pw = getpass.getpass('\nPlease enter password for data import via SMB:\n')
        return pw
    
    def smb_retrieve_file(user, pw, ip, share, file_path, domain='kit.edu'):
        """Import file through smb connection."""
        from easypysmb import EasyPySMB
        
        e = EasyPySMB(ip,
                      username=user,
                      password=pw,
                      share_name=share,
                      domain=domain
                      )
        f = e.retrieve_file(file_path)
        e.close()
        return f
    
    def file_to_df(f, sep):
        return pd.read_csv(f, sep=separator)
    
    # a) check if SMB credentials are already stored in environment variable
    if not user or not pw:
        user = os.getenv('SMB_USER')
        pw = os.getenv('SMB_PW')
    
    # b) load SMB credentials from .env file, if available
    if not user and not pw:
        load_dotenv(find_dotenv())
        user = os.getenv('SMB_USER')
        pw = os.getenv('SMB_PW')
        
        if verbose and user and pw:
            print(get_ts(), "Loaded SMB credentials from .env file.")
    
    # c) ask for SMB login credentials, then store in environment variable
    if user == '':
        os.environ['SMB_USER'] = str(prompt_smb_username())
        user = os.environ.get('SMB_USER')

    if pw == '':
        os.environ['SMB_PW'] = str(prompt_smb_password())
        pw = os.environ.get('SMB_PW')
    
    login_tries_left = 3
    while login_tries_left > 0:
        
        try:
            f = smb_retrieve_file(user, pw, ip, share, file_path)
            df = file_to_df(f, sep=separator)
            break
        except ModuleNotFoundError as ModuleError:
            raise ModuleNotFoundError(ModuleError)
        except Exception as err_msg:
            login_tries_left -= 1
            print('\n', get_ts(), "ERROR! Retrieving file failed with the error message %s.\
            You can reenter your login credentials... tries left: %i" % (err_msg, login_tries_left))
            
            os.environ['SMB_USER'] = prompt_smb_username()
            user = os.environ.get('SMB_USER')
            
            os.environ['SMB_PW'] = prompt_smb_password()
            pw = os.environ.get('SMB_PW')
            continue
        
    if login_tries_left == 0:
        raise ConnectionError("Could not retrieve file from SMB server after 3 attempts.")
    
    return df  # return imported data as DataFrame


# %% read event log and calculator_config


def read_aida_event_log(t_start, t_end, prev_time_h=12):
    """
    Read log from database containing all control events at AIDA.
    
    Contains valves, flaps, pumps, MFCs, pressure balance, mixing fan and programs.

    Parameters
    ----------
    t_start : str or pd.Timestamp
        Start timestamp, pandas timestamp or string in the format 'YYYY-MM-DD HH:MM:SS' (or shorter).
    t_end : str or pd.Timestamp
        End timestamp, pandas timestamp or string in the format 'YYYY-MM-DD HH:MM:SS' (or shorter).
    prev_time_h : int/float, optional
        Specify a time interval in hours before t_start to be included.
        This can be useful if you want to include the previous cleaning program. The default is 12.

    Returns
    -------
    event_log : pd.DataFrame
        Returns a dataframe with the columns ['Zeitpunkt', 'Schalter', 'Wert'].

    """
    import pandas as pd
    
    t_start = pd.Timestamp(t_start) - pd.Timedelta(hours=prev_time_h)
    query = ("SELECT * FROM aida_betrieb "
             "WHERE Zeitpunkt "
             f"BETWEEN '{str(t_start)}' AND '{str(t_end)}'")
    event_log = sql_query('aida', query)
    return event_log


def read_calculator_config(Campaign):
    """
    Read the `aida.calculator_config` table.

    Parameters
    ----------
    Campaign : str
        Campaign name, e.g. 'AWICIT04'.

    Returns
    -------
    pd.DataFrame
        Content of database table aida.calculator_config for the specified campaign.

    """
    def get_config_id(Campaign):
        """Read Config_ID of a specific campaign."""
        query = ("SELECT Config_ID FROM aida.labview_config "
                 f"WHERE Campaign = '{Campaign}'")
        config_id = sql_query('aida', query).loc[0, 'Config_ID']
        return config_id
    
    config_id = get_config_id(Campaign)
    
    query = ("SELECT * FROM aida.calculator_config "
             f"WHERE Config_ID = {config_id}")
    calculator_config = sql_query('aida', query)
    return calculator_config


# %% import welas single particle data
    
def read_welas_spdfile(Campaign, ExpNo, calibration_id='Dp_1_33', verbose=False):
    """
    Read welas single particle file and map bin number to particle diameter.
    
    Parameters
    ----------
    Campaign : :obj:`str`
        Campaign name, e.g. 'AWICIT04'
    ExpNo : :obj:`str`
        Experiment number corresponding to *Campaign*, e.g. '15'
    calibration_id : :obj:`str`, *optional*
        Select calibration table corresponding to welas devices
        
        calibration_id : str = :obj:`'Dp_1_33'` *(default)*
            standard calibration table
        calibration_id : str = :obj:`'Dp_1_45'`
            ??? alternative calibration table
        calibration_id : str = :obj:`'Dp_1_10'`
            do not use this option (empty table)
    verbose : :obj:`bool`, *optional*
        Print informational statements during function execution
            
    Returns
    -------
    dict : keys relate to different welas devices, e.g. 'welas1' or 'welas2'
        ...
        
        content of dict keys : :obj:`pd.DataFrame`
             :obj:`pd.DataFrame`.index : datetime
             
             :obj:`pd.DataFrame`.columnns : ['up1', 'up2', 'Ti', 'Dp']
             
             * 'up1' : pandas.core.series.Series
                 channel numbers (1)
             * 'up2' : pandas.core.series.Series
                 channel numbers (2)
             * 'Ti' : pandas.core.series.Series
                 pulse length in 1e-6 seconds
             * 'Dp' : pandas.core.series.Series
                 Particle diameter in µm corresponding to :obj:`'up1'`
                 
    Raises
    ------
    ValueError
        if an invalid string is passed to parameter *mode*
    
    See Also
    --------
    :func:`sql_query`
        read 'modus_welas' from mySQL database aida.in_ergebnisse
        read welas calibration table from mySQL database device_data.welas<x>_modus
        
    :func:`smb_data_import`
        read path of raw data file from mySQL database device_data.welas<x>_file_exp
        
    :func:`get_ts`
        generates timestamp for verbose output
        
    References
    ----------
    Parts of this code are adopted from IDL code of Ottmar Möhler
    
    Examples
    --------
    >>> spd = read_welas_spdfile(Campaign = 'AWICIT04', ExpNo = '21', verbose=False)
    >>> spd
    {'welas1':                          up1  up2    Ti        Dp
     timestamp
     2018-01-22 13:50:58.140   65   65   8.8   2.36877
     2018-01-22 13:50:58.310  233  232  21.9   6.85452
     2018-01-22 13:50:58.320  102  102  24.9   3.48944
     [...]
    """
    import os
    import sys
    from pathlib import Path
    import pandas as pd
    from py_aida.toolbox import get_ts
    
    def read_in_ergebnisse_to_dict(Campaign, ExpNo) -> dict:
        """Query database for a campaign entry in aida.in_ergebnisse."""
        in_ergebnisse = sql_query(database='aida',
                                  query="SELECT modus_welas, welas_cf_dp, "
                                        "modus_welas2, welas2_cf_dp, modus_welas3, "
                                        "welas3_cf_dp FROM in_ergebnisse WHERE "
                                        "Kampagne = '%s' AND ExperimentNr = %s"
                                        % (Campaign, ExpNo),
                                  output_mode='dict')
        return in_ergebnisse
        
    def read_in_ergebnisse_welas_cf_dp(in_ergebnisse) -> dict:
        """Check if welas flags are set in in_ergebnisse; for welas1, welas2, welas3"""
        welas_cf_dp = {}  # correction factor for particle diameter
        welas_cf_dp['welas1'] = in_ergebnisse['welas_cf_dp']
        welas_cf_dp['welas2'] = in_ergebnisse['welas2_cf_dp']
        welas_cf_dp['welas3'] = in_ergebnisse['welas3_cf_dp']
        return welas_cf_dp
        
    def get_spd_file_path(Campaign, ExpNo, sensor):
        """Get spd filename for a specific experiment and sensor ('welas1'/'welas2'/'welas3')."""
        spd_path = sql_query(database='device_data',
                             query=(f"SELECT File FROM {sensor}_file_exp "
                                    f"WHERE Kampagne = '{Campaign}' and Exp_Nr = {ExpNo}"),
                             output_mode='dict')['File']
        return spd_path
    
    if verbose:
        print(get_ts(), "Start 'read_welas_spdfile', import welas "
              "single particle data and calibration files...")
    
    # Define calibration modus, default = 1
    modus = str(1)    
    
    # initialize data structure
    opc_sensors = pd.DataFrame()
    spd = dict()  # single particle data
    cal = dict()  # calibration tables for spd; map channel numbers to particle diameters
    
    # read particle diameter correction factors
    in_ergebnisse = read_in_ergebnisse_to_dict(Campaign, ExpNo)
    
    opc_sensors = pd.concat(
        [
            opc_sensors,
            pd.DataFrame.from_dict(
                data=read_in_ergebnisse_welas_cf_dp(in_ergebnisse),
                orient='index',
                columns=['dp_correction_factor'])
            ]
        )
    
    # read modus, get selection of used sensors
    opc_sensors.loc['welas1', 'modus'] = in_ergebnisse['modus_welas']
    opc_sensors.loc['welas2', 'modus'] = in_ergebnisse['modus_welas2']
    opc_sensors.loc['welas3', 'modus'] = in_ergebnisse['modus_welas3']
    opc_sensors.drop(opc_sensors[opc_sensors['modus'] == 0].index, inplace=True)  # drop unused sensors (modus == 0) from list
    
    # collect various information about used sensors
    for sensor in opc_sensors.index:
        # get path to data file
        opc_sensors.loc[sensor, 'data_path'] = Path(get_spd_file_path(
            Campaign, ExpNo, sensor).split('\\', 4)[-1].replace('\\', '/'))
        # specify control unit type
        if opc_sensors.loc[sensor, 'modus'] < 3:
            opc_sensors.loc[sensor, 'control_unit'] = 'welas'
        elif opc_sensors.loc[sensor, 'modus'] == 3:
            opc_sensors.loc[sensor, 'control_unit'] = 'promo'
        # define lower channel thresholds
        if sensor == 'welas1':
            opc_sensors.loc[sensor, 'lower_channel_threshold'] = 12
        elif sensor == 'welas2':
            opc_sensors.loc[sensor, 'lower_channel_threshold'] = 2
        # get path to calibration file
        opc_sensors.loc[sensor, 'lower_channel_threshold']
        
    # read calibration tables
    for sensor in opc_sensors.index:
        if (sensor == 'welas1'):
            if opc_sensors.loc[sensor, 'control_unit'] == 'welas':
                cal['welas1'] = sql_query(database='device_data',
                                        query="SELECT Kanal, %s FROM welas_modus \
                                        WHERE Modus =%s" % (calibration_id, modus))
            elif opc_sensors.loc[sensor, 'control_unit'] == 'promo':
                if os.name == 'nt':  # ensure to be connected to KIT intranet
                    smb_path = Path(r"\\imkaaf-srv1\opc\welas-info\Kalibration\Kalibrationstabellen Promo\welasc1_1_cal_range2_133.txt")
                    with open(smb_path) as f:
                        cal[sensor] = pd.read_csv(f, sep='\t')
                elif os.name == 'posix':
                    cal[sensor] = smb_data_import(
                        ip='141.52.172.9',
                        share='opc',
                        file_path="welas-info\Kalibration\Kalibrationstabellen Promo\welasc1_1_cal_range2_133.txt")
                else:
                    raise NameError("Check the output of sys.platform and integrate \
                                    support for your platform into read_welas_spdfile().")
                
        elif sensor == 'welas2':
            if opc_sensors.loc[sensor, 'control_unit'] == 'welas':
                cal['welas2'] = sql_query(database='device_data',
                                          query="SELECT Kanal, %s FROM welas2_modus \
                                          WHERE Modus =%s" % (calibration_id, modus))
            elif opc_sensors.loc[sensor, 'control_unit'] == 'promo':
                if os.name == 'nt':  # ensure to be connected to KIT intranet
                    smb_path = Path(r"\\imkaaf-srv1\opc\welas-info\Kalibration\Kalibrationstabellen Promo\welasc1_1_cal_range4_133.txt")
                    with open(smb_path) as f:
                        cal[sensor] = pd.read_csv(f, sep='\t')
                elif os.name == 'posix':
                    cal[sensor] = smb_data_import(
                        ip='141.52.172.9',
                        share='opc',
                        file_path="welas-info\Kalibration\Kalibrationstabellen Promo\welasc1_1_cal_range4_133.txt")
                else:
                    raise NameError("Check the output of sys.platform and integrate \
                                    support for your platform into read_welas_spdfile().")

        elif sensor == 'welas3':
            if opc_sensors.loc[sensor, 'control_unit'] == 'welas':
                cal['welas3'] = sql_query(database='device_data',
                                          query="SELECT Kanal, %s FROM welas3_modus \
                                          WHERE Modus =%s" % (calibration_id, modus))
            elif opc_sensors.loc[sensor, 'control_unit'] == 'promo':
                raise FileNotFoundError("Calibration table for sensor welas3 with "
                                        "control unit 'Promo' not specified.")
                
        # unify column names to match database naming scheme (also those from promo files)
        cal[sensor].columns = ['Kanal', calibration_id]
        cal[sensor].set_index('Kanal', inplace=True)
    
    # import single particle data from smb server
    for sensor in opc_sensors.index:
        # import data from smb server into pd.Dataframe
        if os.name == 'nt':
            # ensure to be connected to KIT intranet
            smb_path = '//IMKAAF-SRV1/opc/' + str(opc_sensors.loc[sensor, 'data_path'])
            with open(smb_path) as f:
                spd[sensor] = pd.read_csv(f, sep='\t')
        elif os.name == 'posix':
            spd[sensor] = smb_data_import(
                ip='141.52.172.9',
                share='opc',
                file_path=str(opc_sensors.loc[sensor, 'data_path']))
        else:
            raise NameError("Check the output of sys.platform and integrate \
                            support for your platform into read_welas_spdfile().")

        # set datetime index, rename columns
        if opc_sensors.loc[sensor, 'control_unit'] == 'welas':
            # correct imported pulselengths Ti with factor 10 to µs
            spd[sensor]['Ti'] = spd[sensor]['Ti'] / 10
            # set datetime column as index
            date = opc_sensors.loc[sensor, 'data_path'].name.rsplit('_', 3)[-3]
            timestamp_offset = pd.to_datetime(date + ' ' + spd[sensor].keys()[0], format='%y%m%d %H:%M:%S')
            spd[sensor]['timestamp'] = [
                timestamp_offset + pd.Timedelta(milliseconds=int(spd[sensor][spd[sensor].keys()[0]][i]))
                for i in range(len(spd[sensor]))]
            spd[sensor] = spd[sensor].set_index('timestamp')
        elif opc_sensors.loc[sensor, 'control_unit'] == 'promo':
            # specify columns; up1 and Ti chosen to match column names from welas control unit
            spd[sensor].columns = ['timestamp', 'ms', 'up1', 'Ti']
            spd[sensor]['timestamp'] = pd.to_datetime(spd[sensor]['timestamp']) + pd.Series(map(pd.Timedelta, spd[sensor]['ms'] * 1e9))
            spd[sensor].set_index('timestamp', inplace=True)

        # drop rows below channel threshold
        mask = (spd[sensor].up1 > opc_sensors.loc[sensor, 'lower_channel_threshold'])
        spd[sensor] = spd[sensor][mask]
        
        # cut exceeding channels numbers at 4095
        if sensor == 'welas2':
            mask = spd[sensor].up1 > 4095
            spd[sensor][mask] = 4095
        
    # map diameters from calibration table to channels
    for sensor in opc_sensors.index:
        spd[sensor]['Dp'] = cal[sensor].loc[spd[sensor]['up1']][calibration_id].to_numpy()
        spd[sensor]['Dp'] = spd[sensor]['Dp'] * opc_sensors.loc[sensor, 'dp_correction_factor']  # apply correction factor on particle size
    
    if verbose:
        print(get_ts(), "Finished function 'read_welas_spdfile', \
        welas single particle data imported.")

    return spd

    
# %% read pT from second logger

def read_logger_temperature(campaign,
                            chamber,
                            resolution,
                            exp_num=None,
                            verbose=False):
    """
    Read relevant temperature data from logger for a specific campaign.
    You have to select the chamber and time resolution.
    You can choose to slice only a specific experiment number.

    Parameters
    ----------
    campaign : str
        Campaign name.
    chamber : str
        Define the taget database through the chamber name (e.g. 'aida' or 'naua' or 'aida2')
    resolution : str
        Select the time resolution between 'sec' or 'min'.
    exp_num : int or str
        Number of experiment in `campaign`.
        If specified only the time interval of `exp_num` will be returned.
        The default is None.
    verbose : bool, optional
        Print informational statements during function execution. The default is False.

    See Also
    --------
    In the output of read_calculator_config() is defined how the mean temperature
    is calculated for AIDA.

    Returns
    -------
    temp : pd.DataFrame
        Time series of relevant temperature sensors.
        
        if chamber == 'aida':
            Columns 'tg' and 'tw' are mean temperatures in Kelvin.
            Units of all other collumns are indicated by a suffix '_C' or '_K'.
            
            Vertically distributed temperature sensors are:
            'T_gasver1_thck_K': height 50mm (bottom)
            'T_gasver10_thck_K': height 6630mm
            'T_gasver3_thck_K': height 1380mm
            'T_gasver8_thck_K': height 5130mm
            'T_gasver5_thck_K': height 2880mm
            'T_gasver6_thck_K': height 3630mm
            'T_gasver7_thck_K': height 4380mm
            'T_gasver4_thck_K': height 2130mm
            'T_gasver9_thck_K': height 5880mm
            'T_gasver2_thck_K': height 630mm
            'T_gasver11_thck_K': height 7380mm (top)
            
            Horizontally distributed temperature sensors are:
            'T_gashor1A3_thck_K': position 330mm (A3.90)
            'T_gashor2A3_thck_K': position 660mm
            'T_gashor3A3_thck_K': position 990mm
            'T_gashor4A3_thck_K': position 1320mm
            'T_gashor5A3_thck_K': position 1650mm
            'T_gashor6A3_thck_K': position 1980mm
            'T_gashor7A3_thck_K': position 2310mm
            'T_gashor8A3_thck_K': position 2640mm
            'T_gashor9A3_thck_K': position 2970mm
            'T_gashor10A3_thck_K': position 3300mm
            'T_gashor11A3_thck_K': position 3630mm (A3.270)
            
            
        if chamber == 'naua':
            Column 'tg' is the chamber mean temperature in Kelvin.
            The units for the individual sensors, indicated by '_pt100_C', are in °C.
            
            Vertically distributed temperature sensors:
            'T_1NAUA_pt100_C': bottom
            'T_2NAUA_pt100_C': top
            'T_3NAUA_pt100_C': middle

    """
    import pandas as pd
    from py_aida.toolbox import get_ts
    
    thermocouple_delay = 3  # sec
    chamber = chamber.lower()
    
    # get time interval from `{chamber}.experimentliste` if `exp_num` is defined
    if exp_num:
        exp_num = str(exp_num).zfill(2)
        
        query_t_start = f"SELECT ExpStart FROM {chamber}.experimentliste WHERE Kampagne = '{campaign}' AND ExperimentNr = {exp_num}"
        t_start = sql_query('aida', query_t_start, output_mode='dict')['ExpStart']
        query_t_end = f"SELECT ExpEnde FROM {chamber}.experimentliste WHERE Kampagne = '{campaign}' AND ExperimentNr = {exp_num}"
        t_end = sql_query('aida', query_t_end, output_mode='dict')['ExpEnde']
        
    
    # specify columns to be queried
    temp_index_column = ['Zeitpunkt']
    
    if chamber == 'aida':
        temp_columns_mean = ['T_gas1mean_C', 'T_wall1mean_C']
                        
        # vertical chain of temperature sensors from bottom to top
        temp_columns_vertical_chain = [
            'T_gasver1_thck_K',  # Vertikale Temperatur-Messkette, Höhe 50mm (unten)
            'T_gasver10_thck_K',  # Vertikale Temperatur-Messkette, Höhe 6630mm
            'T_gasver3_thck_K',  # Vertikale Temperatur-Messkette, Höhe 1380mm
            'T_gasver8_thck_K',  # Vertikale Temperatur-Messkette, Höhe 5130mm
            'T_gasver5_thck_K',  # Vertikale Temperatur-Messkette, Höhe 2880mm
            'T_gasver6_thck_K',  # Vertikale Temperatur-Messkette, Höhe 3630mm
            'T_gasver7_thck_K',  # Vertikale Temperatur-Messkette, Höhe 4380mm
            'T_gasver4_thck_K',  # Vertikale Temperatur-Messkette, Höhe 2130mm
            'T_gasver9_thck_K',  # Vertikale Temperatur-Messkette, Höhe 5880mm
            'T_gasver2_thck_K',  # Vertikale Temperatur-Messkette, Höhe 630mm
            'T_gasver11_thck_K',  # Vertikale Temperatur-Messkette, Höhe 7380mm (oben)
        ]
        
        # horizontal chain of temperature sensors
        temp_columns_horizontal_chain = [
            'T_gashor1A3_thck_K',  # Horizontale Temperatur-Messkette, 330mm (A3.90)
            'T_gashor2A3_thck_K',  # Horizontale Temperatur-Messkette, 660mm
            'T_gashor3A3_thck_K',  # Horizontale Temperatur-Messkette, 990mm
            'T_gashor4A3_thck_K',  # Horizontale Temperatur-Messkette, 1320mm
            'T_gashor5A3_thck_K',  # Horizontale Temperatur-Messkette, 1650mm
            'T_gashor6A3_thck_K',  # Horizontale Temperatur-Messkette, 1980mm
            'T_gashor7A3_thck_K',  # Horizontale Temperatur-Messkette, 2310mm
            'T_gashor8A3_thck_K',  # Horizontale Temperatur-Messkette, 2640mm
            'T_gashor9A3_thck_K',  # Horizontale Temperatur-Messkette, 2970mm
            'T_gashor10A3_thck_K',  # Horizontale Temperatur-Messkette, 3300mm
            'T_gashor11A3_thck_K',  # Horizontale Temperatur-Messkette, 3630mm (A3.270)
        ]
        
        temp_columns_all = (temp_index_column
                            + temp_columns_mean
                            + temp_columns_vertical_chain
                            + temp_columns_horizontal_chain)
        
    elif chamber == 'naua':
        temp_columns_vertical_chain = [
            'T_1NAUA_pt100_C',  # bottom
            'T_2NAUA_pt100_C',  # top
            'T_3NAUA_pt100_C'   # middle
        ]
        
        temp_columns_all = (temp_index_column
                            + temp_columns_vertical_chain)
    
    # NAUA data gets logged into the AIDA logger, therefore query aida database also for naua
    switched_chamber_specifier = False
    if chamber.lower() == 'naua':
        import warnings
        
        chamber = 'aida'
        switched_chamber_specifier = True
        warnings.warn(get_ts() + " Will fetch temperature data from AIDA logger, even though "
                      "you specified the chamber to `naua`. "
                      "If this is unintended complain to the one who decided to "
                      "save NAUA data in the AIDA logger!",
                      UserWarning)
    
    # run query and get temperature data
    query = (f"SELECT {', '.join(temp_columns_all)} "
             f"FROM {chamber}.{campaign.lower()}_{chamber}_{resolution} ")
    if exp_num:
        query += f"WHERE Zeitpunkt BETWEEN '{t_start}' AND '{t_end + pd.Timedelta(seconds=thermocouple_delay)}'"
    temperature = sql_query('aida', query, df_index_col='Zeitpunkt')
        
    # add old ambiguous column names to ensure backwards compatibility
    if chamber == 'aida' and not switched_chamber_specifier:
        temperature['tg'] = temperature['T_gas1mean_C'] + 273.15
        temperature['tw'] = temperature['T_wall1mean_C'] + 273.15
        
    elif chamber == 'naua' or switched_chamber_specifier:
        for sensor in temp_columns_vertical_chain:  # transform from °C to K
            temperature[sensor.rsplit('_', 1)[0] + '_K'] = temperature[sensor] + 273.15
        temperature['tg'] = temperature.loc[:, temp_columns_vertical_chain].mean(axis=1) + 273.15
    
    if verbose:
        print(get_ts(), "Imported temperature data from database")
    
    return temperature
    

def read_logger_pressure(campaign, chamber, resolution, exp_num, verbose):
    """
    Reads pressure data from logger for a specific campaign.
    You have to select the chamber and time resolution.
    You can choose to slice only a specific experiment number.

    Parameters
    ----------
    campaign : str
        Name of the campaign.
    chamber : str
        Name of the chamber (e.g. 'aida', 'naua' or 'aida2').
    resolution : str
        Select the time resolution between 'sec' or 'min'.
    exp_num : int or str
        Number of an experiment in `campaign`.
    verbose : bool
        Print informational statements during function execution. The default is False.

    Returns
    -------
    pressure : pd.Series
        Pressure time series in Pa.

    """
    from py_aida.toolbox import get_ts
    
    chamber = chamber.lower()
    
    if chamber == 'aida':
        pressure_column_name = 'p_AIDA1_hPa'
    elif chamber == 'naua':
        pressure_column_name = 'p_NAUA_V'  # in hPa
        
    query = f"SELECT Zeitpunkt, {pressure_column_name} FROM {chamber}.{campaign.lower()}_{chamber}_{resolution} "
    
    if exp_num:
        exp_num = str(exp_num).zfill(2)
        
        query_t_start = f"SELECT ExpStart FROM {chamber}.experimentliste WHERE Kampagne = '{campaign}' AND ExperimentNr = {exp_num}"
        t_start = sql_query('aida', query_t_start, output_mode='dict')['ExpStart']
        query_t_end = f"SELECT ExpEnde FROM {chamber}.experimentliste WHERE Kampagne = '{campaign}' AND ExperimentNr = {exp_num}"
        t_end = sql_query('aida', query_t_end, output_mode='dict')['ExpEnde']
        
        query += f"WHERE Zeitpunkt BETWEEN '{t_start}' AND '{t_end}'"
    
    pressure_df = sql_query('aida', query, df_index_col='Zeitpunkt') # in hPa
    pressure_series = pressure_df[pressure_column_name].rename('p') * 100  # in Pa
    
    if verbose:
        print(get_ts(), f"Imported pressure data from {chamber.upper()} with '{resolution}' time resolution.")
    
    return pressure_series

def read_pT_seclogger(Campaign, ExpNo, t_start=None, t_end=None, verbose=False):
    """Read presure, gas temperature and wall temperature from second logger for a time interval between `t_start` and `t_end`.
    
    Parameters
    ----------
    Campaign : :obj:`str`
        Name of campaign
    ExpNo : :obj:`str`
        Number of experiment in *Campaign*
    t_start : :obj:`pd.Timestamp()`, *optional*
        Start of experiment, get from database table :obj:`aida.experimentliste`
        If not specified timestamp gets taken from aida.experimentliste
    t_end : :obj:`pd.Timestamp()`, *optional*
        End of experiment, get from database table :obj:`aida.experimentliste`.
        If not specified timestamp gets taken from aida.experimentliste
    verbose : :obj:`bool`, *optional*
        Print informational statements during function execution
        
    Returns
    -------
    :obj:`pd.DataFrame`
        Contains temperature and pressure data
        
        :obj:`pd.DataFrame.index` : DatetimeIndex
            ...
        
        :obj:`pd.DataFrame.columns` : ['tg', 'tw', 'p']
            * tg : gas temperature, in K
            * tw : wall temperature, in K
            * p : pressure, in Pa
    
    See Also
    --------
    :func:`sql_query`

    Notes
    -----
    For the definition how temperatures *tg* and *tw* are calculated check in database table aida.calculator_config
    
    Examples
    --------
    >>> Campaign = 'AWICIT04'
    >>> ExpNo = '21'
    >>> experimentliste = sql_query(database='aida',
                             query = "SELECT * FROM experimentliste \

                             WHERE Kampagne LIKE '%s'AND ExperimentNr = %s"
                             %(Campaign, ExpNo), output_mode='dict')
    >>> pT = read_pT_seclogger(Campaign, ExpNo,
                               experimentliste['ExpStart'],
                               experimentliste['ExpEnde'],
                               verbose = False)
    >>> pT.head()
                               tg        tw          p
    Zeitpunkt
    2018-01-22 12:20:00  213.6798  211.5567  99756.47
    2018-01-22 12:20:01  213.6584  211.5699  99761.96
    2018-01-22 12:20:02  213.6613  211.5547  99750.98
    2018-01-22 12:20:03  213.6650  211.5538  99745.48
    2018-01-22 12:20:04  213.6584  211.5683  99739.99
    """
    # TODO: Error of Temperature and pressure?
    
    import pandas as pd
    from py_aida.toolbox import get_ts
    
    thermocouple_delay = 3  # sec
    
    ExpNo = str(ExpNo).zfill(2)
    
    if t_start is None:
        query = f"SELECT ExpStart FROM experimentliste WHERE Kampagne = '{Campaign}' AND ExperimentNr = {ExpNo}"
        t_start = sql_query('aida', query, output_mode='dict')['ExpStart']
    if t_end is None:
        query = f"SELECT ExpEnde FROM experimentliste WHERE Kampagne = '{Campaign}' AND ExperimentNr = {ExpNo}"
        t_end = sql_query('aida', query, output_mode='dict')['ExpEnde']
    
    query = ("SELECT Zeitpunkt, T_gas1mean_C, T_wall1mean_C, p_AIDA1_hPa "
             f"FROM {Campaign.lower()}_aida_sec "
             f"WHERE Zeitpunkt BETWEEN '{t_start}' AND '{t_end + pd.Timedelta(seconds=thermocouple_delay)}'")
    pT = sql_query('aida', query, df_index_col='Zeitpunkt')
    pT = pT.rename(columns={'p_AIDA1_hPa': 'p',
                            'T_gas1mean_C': 'tg',
                            'T_wall1mean_C': 'tw'})
    
    # shift gas temperature for thermocouple_delay
    pT.tg = pT.tg.shift(-thermocouple_delay)
        
    pT['tg'] = pT.tg + 273.15  # convert Temp from °C to K
    pT['tw'] = pT.tw + 273.15  # convert Temp from °C to K
    pT['p'] = pT.p * 100  # convert pressure from hPa to Pa
    
    if verbose:
        print(get_ts(), "Imported temperature and pressure data from database")
    
    return pT.loc[t_start: t_end]
    

# %% read CPC3010 data from database
    
def read_cn_cpc3010(Campaign, t_start, t_end, verbose=False):
    """Read CPC-3010 data from SQL database for time interval defined in experimentliste.
    
    Parameters
    ----------
    Campaign : :obj:`str`
        Name of campaign
    t_start : :obj:`pd.Timestamp()`
        Start of experiment, get from database table :obj:`aida.experimentliste`
    t_end : :obj:`pd.Timestamp()`
        End of experiment, get from database table :obj:`aida.experimentliste`
    verbose : :obj:`bool`, *optional*
        Print informational statements during function execution
    
    Returns
    -------
    :obj:`pd.DataFrame`
        containing CPC-3010 data in cm⁻³
        
        :obj:`pd.DataFrame.index`
            DatetimeIndex
            
        :obj:`pd.DataFrame.columns` : ['Particles_per_ccm']
            measured CPC concentration in cm⁻³
            
    See Also
    --------
    :func:`sql_query`,  :func:`read_cn_cpc3776`
    
    Notes
    -----
    TODO: Combine this function with :func:`read_cn_cpc3776` for generalization of CPC calls
    
    Examples
    --------
    >>> Campaign = 'AWICIT01B'
    >>> ExpNo = '15'
    >>> experimentliste = sql_query('aida',
            query= "SELECT * FROM experimentliste \

            WHERE Kampagne LIKE '%s'AND ExperimentNr = %s" %(Campaign, ExpNo),
            output_mode='dict')
    >>> cpc3010 = read_cn_cpc3010(Campaign, t_start = experimentliste['ExpStart'],
            t_end = experimentliste['ExpEnde'], verbose = False)
    >>> cpc3010
                             Particles_per_ccm
    Zeitpunkt
    2017-03-30 13:30:00               0.00
    2017-03-30 13:30:01               0.08
    2017-03-30 13:30:02               0.00
    2017-03-30 13:30:03               0.00
    [...]
    """
    from py_aida.toolbox import get_ts
    
    db_table = Campaign.lower() + '_aida_cpc'

    cpc3010 = sql_query(database='aida',
                        query="SELECT * FROM %s \
                        WHERE Zeitpunkt BETWEEN '%s' AND '%s'"
                        % (db_table, t_start, t_end),
                        df_index_col='Zeitpunkt')

    if verbose:
        print(get_ts(), "Imported CPC-3010 data from database")
    
    return cpc3010


# %% Read CPC3776 Data
    
def read_cn_cpc3776(Campaign, t_start, t_end, verbose=False):
    """Read CPC-3776 data from SQL database for defined time interval.
    
    Parameters
    ----------
    Campaign : :obj:`str`
        name of campaign
    t_start : :obj:`pd.Timestamp()`
        Start of experiment, get from database table :obj:`aida.experimentliste`
    t_end : :obj:`pd.Timestamp()`
        End of experiment, get from database table :obj:`aida.experimentliste`
    verbose : :obj:`bool`, *optional*
        Print informational statements during function execution
    
    Returns
    -------
    :obj:`pd.DataFrame`
        containing CPC-3776 data in cm⁻³
        
        :obj:`pd.DataFrame.index`
            DatetimeIndex
            
        :obj:`pd.DataFrame.columns` : ['Particles_per_ccm']
            measured CPC concentration in cm⁻³

    See Also
    --------
    :func:`sql_query`, :func:`read_cn_cpc3010`
    
    Examples
    --------
    >>> Campaign = 'AWICIT01B'
    >>> ExpNo = '15'
    >>> experimentliste = sql_query('aida',
            query= "SELECT * FROM experimentliste \

            WHERE Kampagne LIKE '%s'AND ExperimentNr = %s" %(Campaign, ExpNo),
            output_mode='dict')
    >>> cpc3776 = read_cn_cpc3776(Campaign, t_start = experimentliste['ExpStart'],
            t_end = experimentliste['ExpEnde'], verbose = False)
    >>> cpc3776
                             Particles_per_ccm
    Zeitpunkt
    2017-03-30 13:30:00               0.00
    2017-03-30 13:30:01               0.08
    2017-03-30 13:30:02               0.00
    2017-03-30 13:30:03               0.00
    [...]
    """
    from py_aida.toolbox import get_ts
    
    cpc3776 = sql_query(database='aida',
                        query="SELECT Zeitpunkt, CNAIDA3_A1_ppc FROM %s_aida_sec \
                        WHERE Zeitpunkt BETWEEN '%s' AND '%s'" % (Campaign.lower(), t_start, t_end),
                        df_index_col='Zeitpunkt')
    cpc3776 = cpc3776.rename(columns={'CNAIDA3_A1_ppc': 'cn'})  # in cm⁻³
    
    if verbose:
        print(get_ts(), "Imported CPC-3776 data from database")
        
    return cpc3776
    

# %%  read SIMONE data from database

def read_simone(Campaign, t_start, t_end, verbose=False):
    """Read SIMONE data from SQL database for time interval defined in experimentliste and smooth data.
   
    Parameters
    ----------
    Campaign : :obj:`str`
        name of campaign
    t_start : :obj:`pd.Timestamp()`
        Start of experiment, get from database table :obj:`aida.experimentliste`
    t_end : :obj:`pd.Timestamp()`
        End of experiment, get from database table :obj:`aida.experimentliste`
    verbose : :obj:`bool`, *optional*
        Print informational statements during function execution
    
    Returns
    -------
    :obj:`pd.DataFrame`
        contains SIMONE data in arbitraty units

        :obj:`pd.DataFrame.columns` :
            all intensities in arbitrary units
            
            par_2
                Forward scattering intenity, parallel polarization
            par_178
                Backward scattering intensity, parallel polarization
            per_178
                Backward scattering intensity, perpendicular polarization
            par_2_smooth
                smoothed forward scattering intenity, parallel polarization
            par_2_smooth_diff
                differentiated intensity dI/dt for forward scattering data
            par_178_smooth
                smoothed backward scattering intensity, parallel polarization
            per_178_smooth
                smoothed backward scattering intensity, perpendicular polarization
            depolarization_smooth
                smoothed depolarization ratio between *par_178_smooth* and *per_178_smooth*
            
        :obj:`pd.DataFrame.index`
            DatetimeIndex
            
    
    See Also
    --------
    :func:`sql_query`
    
    :func:`scipy.signal.savgol_filter`
    
    Notes
    -----
    TODO: Discuss with Robert about background treatment of SIMONE data
    
    Examples
    --------
    >>> Campaign = 'AWICIT01B'
    >>> ExpNo = '15'
    >>> experimentliste = sql_query('aida',
            query= "SELECT * FROM experimentliste \

            WHERE Kampagne LIKE '%s'AND ExperimentNr = %s" %(Campaign, ExpNo),
            output_mode='dict')
    >>> s = read_simone(Campaign, experimentliste['ExpStart'], experimentliste['ExpEnde'],
    ...                 verbose = False)
    >>> s
                          par_2  par_178  ...  per_178_smooth  depolarization_smooth
    Zeitpunkt                             ...
    2017-03-30 13:30:00    2198    21295  ...     1358.986014               0.063348
    2017-03-30 13:30:01    2317    20903  ...     1409.181818               0.064743
    2017-03-30 13:30:02    2290    22499  ...     1444.932401               0.065514
    [...]
    """
    import pandas as pd
    from scipy.signal import savgol_filter
    from py_aida.toolbox import get_ts

    if verbose:
        print(get_ts(), "Start import of SIMONE data ...")
    
    if t_start < pd.Timestamp('2019') and t_end < pd.Timestamp('2019'):  # exp before 2019
        db = 'device_data'
        db_table = 'simone_new'
    elif t_start >= pd.Timestamp('2019') and t_end >= pd.Timestamp('2019'):  # exp since 2019
        db = 'aida'
        db_table = Campaign.lower() + '_aida_simone'
    
    query = (f"SELECT Zeitpunkt, par_2, par_178, per_178 FROM {db_table} "
             f"WHERE Zeitpunkt BETWEEN '{t_start}' AND '{t_end}'")
    
    simone = sql_query(db, query, df_index_col='Zeitpunkt')
    
    simone['par_2_smooth'] = savgol_filter(simone.par_2, 21, 3)  # Smooth foward scattering data
    simone['par_2_smooth_diff'] = simone.par_2_smooth.diff(periods=5)  # Derivation of smoothened SIMONE forward scattering data
    
    simone['par_178_smooth'] = savgol_filter(simone.par_178, 51, 3)  # Smooth backward scattering par
    simone['per_178_smooth'] = savgol_filter(simone.per_178, 11, 3)  # Smooth backward scattering per
    
    # Calculate Depolarization from smoothened background scattering data
    # TODO: check if some corrections need to be done here, e.g. background substraction
    simone['depolarization_smooth'] = simone.per_178_smooth / simone.par_178_smooth
    
    if verbose:
        print(get_ts(), "Imported SIMONE data from database and smoothened data")
    
    return simone


# %% read MBW data from database
    
def read_h2o_total_mbw(Campaign, t_start, t_end, verbose=False):
    """Read dew point mirror (MBW) data from SQL database to get total water content.
                    
    Parameters
    ----------
    Campaign : :obj:`str`
        name of campaign
    t_start : :obj:`pd.Timestamp()`
        Start of experiment, get from database table :obj:`aida.experimentliste`
    t_end : :obj:`pd.Timestamp()`
        End of experiment, get from database table :obj:`aida.experimentliste`
    temperature : :obj:`pandas.core.series.Series`
        Contains gas temperature,
        temperature.index : :obj:`pandas.core.indexes.datetimes.DatetimeIndex`,
        dtype='datetime64[ns]'
    verbose : :obj:`bool`, *optional*
        Print informational statements during function execution
    
    Returns
    -------
    :obj:`pd.DataFrame`
        contains dew point mirror (MBW) data and temperature data
        
        :obj:`pd.DataFrame.columns`
            MBW_VMR_ppm
                Volume mixing ratio of H2O molecules, in ppm
            MBW_Pabs_hPa
                Absolute pressure, in hPa
            pw
                Water vapor pressure, in Pa
            tg
                Gas temperature, in K
        
        :obj:`pd.DataFrame.index`
            DatetimeIndex
    
    See Also
    --------
    :func:`numpy.interp`
    
    :func:`sql_query`
    
    :func:`read_h2o_total_apet`
    
    Notes
    -----
    * Duplicated entries per second are removed (use only first entry per second)
    * Gas temperature for later calculation of relative humidity to returned pd.DataFrame
    
    Examples
    --------
    >>> Campaign = 'AWICIT01B'
    >>> ExpNo = '15'
    >>> experimentliste = sql_query('aida',
            query= "SELECT * FROM experimentliste \

            WHERE Kampagne LIKE '%s'AND ExperimentNr = %s" %(Campaign, ExpNo),
            output_mode='dict')
    >>> pT = read_pT_seclogger(Campaign, ExpNo,
            experimentliste['ExpStart'],
            experimentliste['ExpEnde'],
            verbose = False)
    >>> h2o_total = read_h2o_total_mbw(Campaign, experimentliste['ExpStart'],
                                       experimentliste['ExpEnde'], verbose = verbose)
    >>> h2o_total
                         MBW_VMR_ppm  MBW_Pabs_hPa        pw
    Zeitpunkt
    2017-03-30 13:30:00      24.5141        1010.1  2.476169
    2017-03-30 13:30:01      24.5132        1010.1  2.476078
    2017-03-30 13:30:02      24.5117        1010.1  2.475927
    [...]
    """
    from py_aida.toolbox import get_ts
    from py_aida.tools_parametrizations import p_sat_h2o
    
    # import data from database
    query = f"SELECT Zeitpunkt, MBW_Tdew_K, MBW_Tfrost_K, MBW_VMR_ppm, MBW_Pabs_hPa FROM {Campaign.lower() + '_aida_mbw'} WHERE Zeitpunkt BETWEEN '{t_start}' AND '{t_end}'"
    h2o_total = sql_query('aida', query, df_index_col='Zeitpunkt')

    h2o_total = h2o_total[~h2o_total.index.duplicated(keep='first')]  # remove duplicate values (for value/sec >1)
    
    def calib_mbw_frost_temperature(temp_frost_mbw, verbose=False):
        """
        Correct MBW frost point temperature according to calibration from 2020-07-07.
    
        Parameters
        ----------
        temp_frost_mbw : list-like
            Frost point temperature of MBW, in K.
        verbose : bool, optional
            Print informational statements during function execution. The default is False.
    
        Returns
        -------
        T_frost : list-like
            Corrected MBW frost point temperature according to calibration of 2020-07-07, in K.
        
        Notes
        -----
        The function applied here is derived by a fit through the deviations
        identified from the calibration done from 26 June 2020 - 7 July 2020.
        Calculation was done by Julia Schneider.
    
        """
        from py_aida.toolbox import get_ts
        
        temp_frost_recalibrated = -0.000744 * (temp_frost_mbw - 273.15) - 0.14903982 + temp_frost_mbw
        
        if verbose:
            print(get_ts(), "MBW frost point temperature corrected according to MBW calibration on 7 July 2020")
            
        return temp_frost_recalibrated
    
    mbw_frost_temperature = calib_mbw_frost_temperature(h2o_total['MBW_Tfrost_K'], verbose=verbose)
    
    h2o_total = h2o_total.rename({'MBW_VMR_ppm': "MBW_VMR_ppm_deprecated"}, axis=1)  # deprecated since new calibration. Keep column to make clear that the database still keeps data following old calibration
    h2o_total = h2o_total.rename({'MBW_Tfrost_K': "MBW_Tfrost_K_deprecated"}, axis=1)  # deprecated since new calibration. Keep column to make clear that the database still keeps data following old calibration
    
    h2o_total['MBW_Tfrost_K'] = mbw_frost_temperature
    h2o_total['pw'] = p_sat_h2o(mbw_frost_temperature, over_surface='ice')  # water vapor pressure, in Pa
    h2o_total['MBW_VMR_ppm'] = h2o_total['pw'] / (h2o_total['MBW_Pabs_hPa'] * 100) * 1e6
    
    if verbose:
        print(get_ts(), "Imported MBW data from database and calculated water vapor pressure")
    
    return h2o_total


# %% Read APET data from database
    
def read_h2o_total_apet(Campaign, t_start, t_end, temperature, verbose=False):
    """Read APET data from SQL databasev to get total water content.
    
    Parameters
    ----------
    Campaign : :obj:`str`
        name of campaign
    t_start : :obj:`pd.Timestamp()`
        Start of experiment, get from database table :obj:`aida.experimentliste`
    t_end : :obj:`pd.Timestamp()`
        End of experiment, get from database table :obj:`aida.experimentliste`
    temperature : :obj:`pandas.core.series.Series`
        Contains gas temperature,
        temperature.index : :obj:`pandas.core.indexes.datetimes.DatetimeIndex`,
        dtype='datetime64[ns]'
    verbose : :obj:`bool`, *optional*
        Print informational statements during function execution
    
    Returns
    -------
    :obj:`pd.DataFrame`
        contains dew point mirror (MBW) data and temperature data
        
        :obj:`pd.DataFrame.columns`
            VMR_H2O_ppm
                Volume mixing ratio of H2O molecules, in ppm
            p_AIDA_hPa
                Absolute pressure, in hPa
            pw
                Water vapor pressure, in Pa
            tg
                Gas temperature, in K
        
        :obj:`pd.DataFrame.index`
            DatetimeIndex
    
    See Also
    --------
    :func:`read_h2o_total_mbw`
    
    :func:`numpy.interp`
    
    :func:`sql_query`
    
    Notes
    -----
    * Duplicated entries per second are removed (use only first entry per second)
    * Gas temperature for later calculation of relative humidity to returned pd.DataFrame
    
    Examples
    --------
    >>> Campaign = 'AWICIT01B'
    >>> ExpNo = '15'
    >>> experimentliste = sql_query('aida',
            query= "SELECT * FROM experimentliste \

            WHERE Kampagne LIKE '%s'AND ExperimentNr = %s" %(Campaign, ExpNo),
            output_mode='dict')
    >>> pT = read_pT_seclogger(Campaign, ExpNo,
            experimentliste['ExpStart'],
            experimentliste['ExpEnde'],
            verbose = False)
    >>> h2o_total = read_h2o_total_apet(Campaign, experimentliste['ExpStart'],
            experimentliste['ExpEnde'], pT.tg)
    >>> h2o_total
                         VMR_H2O_ppm   p_AIDA_hPa        pw        tg
    Zeitpunkt
    2017-03-30 13:30:00    25.225319  1011.194011  2.550769  220.9283
    2017-03-30 13:30:01    25.235187  1011.118917  2.551578  220.9210
    2017-03-30 13:30:02    25.217274  1011.097938  2.549713  220.9318
    [...]
    """
    from py_aida.toolbox import get_ts
    
    # import data from database
    query = f"SELECT Zeitpunkt, VMR_H2O_ppm, p_AIDA_hPa FROM {Campaign.lower() + '_aida_apet'} WHERE Zeitpunkt BETWEEN '{t_start}' AND '{t_end}'"
    h2o_total = sql_query('aida', query, df_index_col='Zeitpunkt')

    # remove duplicate values (for value/sec >1)
    h2o_total = h2o_total[~h2o_total.index.duplicated(keep='first')]
    h2o_total['pw'] = (h2o_total.VMR_H2O_ppm * 1E-6) * (h2o_total.p_AIDA_hPa * 100)
    
    if verbose:
        print(get_ts(), "Imported APET data from database and calculated water vapor pressure")
        
    return h2o_total

# Example function call
# h2o_total = read_h2o_total_apet(Campaign, experimentliste.ExpStart[0], experimentliste.ExpEnde[0], pT.tg, verbose = verbose)


# %% Read single path apict SPAPICT data from database

def read_h2o_spapict(Campaign, t_start, t_end, verbose=False):
    """Read tunable diode laser (TDL) *SPAPICT* data from SQL database to get water content.
    
    Calulate water vapor pressure from volume mixing ratio and pressure
    
    Parameters
    ----------
    Campaign : :obj:`str`
        name of campaign
    t_start : :obj:`pd.Timestamp()`
        Start of experiment, get from database table :obj:`aida.experimentliste`
    t_end : :obj:`pd.Timestamp()`
        End of experiment, get from database table :obj:`aida.experimentliste`
    verbose : :obj:`bool`, *optional*
        Print informational statements during function execution
    
    Returns
    -------
    :obj:`pd.DataFrame`
        contains dew point mirror (MBW) data and temperature data
        
        :obj:`pd.DataFrame.columns`
            VMR_H2O_ppm
                Volume mixing ratio of H2O molecules, in ppm
            p_AIDA_hPa
                Absolute pressure, in hPa
            pw
                Water vapor pressure, in Pa
            T_gas_in_K
                Gas temperature, in K
        
        :obj:`pd.DataFrame.index`
            DatetimeIndex
    
    See Also
    --------
    :func:`read_h2o_total_mbw`, :func:`read_h2o_total_apet`, :func:`read_h2o_apict`
    
    :func:`sql_query`
    
    Notes
    -----
    * Duplicated entries per second are removed (use only first entry per second)
    
    Examples
    --------
    >>> Campaign = 'AWICIT01B'
    >>> ExpNo = '15'
    >>> experimentliste = sql_query('aida',
            query= "SELECT * FROM experimentliste \

            WHERE Kampagne LIKE '%s'AND ExperimentNr = %s" %(Campaign, ExpNo),
            output_mode='dict')
    >>> pT = read_pT_seclogger(Campaign, ExpNo,
            experimentliste['ExpStart'],
            experimentliste['ExpEnde'],
            verbose = False)
    >>> h2o = read_h2o_spapict(Campaign, experimentliste['ExpStart'],
                               experimentliste['ExpEnde'])
    >>> h2o
                      p_AIDA_hPa  VMR_H2O_ppm  T_gas_in_K        pw
    Zeitpunkt
    2017-03-30 13:30:00  1014.337158    26.674977  220.923978  2.705742
    2017-03-30 13:30:01  1014.282227    26.671928  220.916721  2.705286
    2017-03-30 13:30:02  1014.172363    26.674528  220.920322  2.705257
    [...]
    """
    import pandas as pd
    from py_aida.toolbox import get_ts
    
    if verbose:
        print(get_ts(), "Start import single path TDL ('SPAPICT') data from database...")

    db_table = Campaign.lower() + '_aida_spapict'

    h2o = sql_query(database='aida',
                    query="SELECT Zeitpunkt, p_AIDA_hPa, VMR_H2O_ppm, T_gas_in_K FROM %s \
                    WHERE Zeitpunkt BETWEEN '%s' AND '%s'" % (db_table, t_start, t_end),
                    df_index_col='Zeitpunkt')

    h2o = h2o.dropna(axis='rows')  # remove entries containing broken data like nan,
    h2o = h2o[~h2o.index.duplicated(keep='first')]  # remove duplicate values (for value/sec >1)
    
    pw = pd.Series(data=h2o['VMR_H2O_ppm'] * 1e-6 * h2o['p_AIDA_hPa'] * 100, name='pw')  # derive water vapor pressure
    h2o = h2o.merge(pw, left_index=True, right_index=True)

    if verbose:
        print(get_ts(), "Finished import of single path APICT data from database and calculated water vapor pressure")
    
    return h2o

# Example function call:
# h2o = read_h2o_spapict(Campaign, experimentliste.ExpStart[0], experimentliste.ExpEnde[0], verbose = verbose)


# %% Read APICT data from database

def read_h2o_apict(Campaign, t_start, t_end, verbose=False):
    """Read tunable diode laser (multipath TDL) *APICT* data from SQL database to get water content.
    
    Calulate water vapor pressure from volume mixing ratio and pressure
    
    Parameters
    ----------
    Campaign : :obj:`str`
        name of campaign
    t_start : :obj:`pd.Timestamp()`
        Start of experiment, get from database table :obj:`aida.experimentliste`
    t_end : :obj:`pd.Timestamp()`
        End of experiment, get from database table :obj:`aida.experimentliste`
    verbose : :obj:`bool`, *optional*
        Print informational statements during function execution
    
    Returns
    -------
    :obj:`pd.DataFrame`
        contains dew point mirror (MBW) data and temperature data
        
        :obj:`pd.DataFrame.columns`
            VMR_H2O_ppm
                Volume mixing ratio of H2O molecules, in ppm
            p_AIDA_hPa
                Absolute pressure, in hPa
            pw
                Water vapor pressure, in Pa
            T_gas_in_K
                Gas temperature, in K
        
        :obj:`pd.DataFrame.index`
            DatetimeIndex
    
    See Also
    --------
    :func:`read_h2o_total_mbw`, :func:`read_h2o_total_apet`, :func:`read_h2o_spapict`
    
    :func:`sql_query`
    
    Notes
    -----
    * Duplicated entries per second are removed (use only first entry per second)
    
    Examples
    --------
    >>> Campaign = 'AWICIT01B'
    >>> ExpNo = '15'
    >>> experimentliste = sql_query('aida',
            query= "SELECT * FROM experimentliste \

            WHERE Kampagne LIKE '%s'AND ExperimentNr = %s" %(Campaign, ExpNo),
            output_mode='dict')
    >>> h2o = read_h2o_apict(Campaign, experimentliste['ExpStart'],
                             experimentliste['ExpEnde'])
    >>> h2o
                      p_AIDA_hPa  VMR_H2O_ppm  T_gas_in_K        pw
    Zeitpunkt
    2017-03-30 13:30:00  1014.337158    25.156530  220.923978  2.551720
    2017-03-30 13:30:01  1014.282227    25.228239  220.916721  2.558855
    2017-03-30 13:30:02  1014.172363    25.131118  220.920322  2.548729
    [...]
    """
    import pandas as pd
    from py_aida.toolbox import get_ts
    
    if verbose:
        print(get_ts(), "Start import multi path TDL ('APICT') data from database...")

    db_table = Campaign.lower() + '_aida_apict'

    h2o = sql_query(database='aida',
                    query="SELECT Zeitpunkt, p_AIDA_hPa, VMR_H2O_ppm, T_gas_in_K FROM %s \
                    WHERE Zeitpunkt BETWEEN '%s' AND '%s'" % (db_table, t_start, t_end),
                    df_index_col='Zeitpunkt')

    
    h2o = h2o.dropna(axis='rows')  # remove entries containing broken data like nan,
    h2o = h2o[~h2o.index.duplicated(keep='first')]  # remove duplicate values (for value/sec >1)
    
    pw = pd.Series(data=h2o['VMR_H2O_ppm'] * 1e-6 * h2o['p_AIDA_hPa'] * 100, name='pw')  # derive water vapor pressure
    h2o = h2o.merge(pw, left_index=True, right_index=True)
    
    if verbose:
        print(get_ts(), "Finished import of multi path APICT data from database and calculated water vapor pressure")
        
    return h2o

# Example function call:
# h2o = read_h2o_apict(Campaign, experimentliste.ExpStart[0], experimentliste.ExpEnde[0], verbose = verbose)
