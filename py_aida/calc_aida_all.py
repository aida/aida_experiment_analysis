#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Run a full standard analysis of AIDA experiments.

This file is part of the project `py_aida`.
Copyright (c) 2019-2023, GPL v3, KIT/IMK-AAF
(Karlsruhe Institute of Technology, Institute of Meteorology and
Climate Research, Atmospheric Aerosol Research)
For full license details see the file LICENSE.md or
go to https://www.gnu.org/licenses/gpl-3.0.html.
"""
import warnings
from pathlib import Path
import numpy as np
import pandas as pd
from scipy.signal import savgol_filter

from py_aida import tools_import
from py_aida.tools_parametrizations import p_sat_h2o
from py_aida.toolbox import get_ts


# %% functions for adiabatic conditions

def calc_T_adiabatic(pressure:pd.Series, temperature_ref:float, pressure_ref:float, verbose:bool=False) -> pd.Series:
    """Calculate adiabatic temperature profile for dataset of temperature and pressure.
    
    Parameters
    ----------
    pressure : pd.Series
        Gas pressure (in [Pa]).
    temperature_ref : float
        Reference temperature in Kelvin.
    pressure_ref : float
        Reference pressure in Pascal.
    verbose : bool, optional
        Print log information during code execution. The default is False.

    Returns
    -------
    pT : pd.DataFrame
        index type = DatetimeIndex
        
        columns = ['tg', 'tw', 'p', 'tg_adiabatic']
        
        units: [tg] = K, [tw] = K, [p] = Pa, [tg_adiabatic] = K

    """
    # TODO: Error of Temperature and pressure ???
    tg_adiabatic = temperature_ref * (pressure_ref / pressure)**((1 - 1.4) / 1.4)
    
    if verbose:
        print(get_ts(), "Calculated adiabatic temperature profile")
    
    return tg_adiabatic.rename('tg_adiabatic')


def calc_adiabatic_updraft_velocity(temp, humid=False, press=None, p_h2o=None):
    """
    Calculate the adiabatic updraft velocity equivalent to the cooling rate in AIDA.

    Parameters
    ----------
    temp : pd.Series
        Temperature, in K.
    humid : bool, optional
        Decide if specific heat capacity of dry or humid air is used.
        It often makes only a slight difference.
        The default is False.
    press : pd.Series, optional
        Pressure in Pa. Only used if humid=True.
        The default is None.
    p_h2o : pd.Series, optional
        Water vapor pressure in Pa. Only used if humid=True.
        The default is None.

    Returns
    -------
    w : pd.Series
        Adiabatic updraft velocity as an equivalent to AIDAs cooling rate, in m/s.

    """
    from scipy import constants as const
    
    if not humid:
        from py_aida.tools_parametrizations import heat_capacity_air_dry
        c_p = heat_capacity_air_dry(temp)
    elif humid:
        from py_aida.tools_parametrizations import heat_capacity_air_humid
        c_p = heat_capacity_air_humid(temp, press, p_h2o)
    
    dT = temp.diff()
    dt = temp.index.to_series().diff().dt.total_seconds()
    
    w = - c_p / const.g * dT / dt
    return w


# %% Functions to process TDL and MBW data

def calc_RH_from_pw(h2o, temp, verbose=False):
    """Calculate relative humidity with respect to ice and water.

    Parameters
    ----------
    h2o : pd.DataFrame
        Includes water vapor pressure 'pw' and temperature.
    verbose : bool, optional
        Print log information during code execution. The default is False.

    Raises
    ------
    TypeError
        If the known column names for temperature data are not available.

    Returns
    -------
    h2o : pd.DataFrame
        index type = DatetimeIndex
        
        new columns: ['RHi', 'RHi_err', 'RHi_smooth', 'RHw', 'RHw_err', 'RHw_smooth']
        
        units of new columns: all in %.
    """
    # Define relative humidity w.r.t. ice:
    if 'pw_corr' in h2o.keys():
        RHi = pd.Series(data=h2o['pw_corr'] / p_sat_h2o(temp, over_surface='ice') * 100, name='RHi')  # in %
    else:
        RHi = pd.Series(data=h2o.pw / p_sat_h2o(temp, over_surface='ice') * 100, name='RHi')  # in %
    
    h2o = h2o.merge(RHi, left_index=True, right_index=True)
    h2o.loc[:, 'RHi_err'] = 5  # in %
    
    try:
        RHi_smooth = pd.Series(data=savgol_filter(h2o['RHi'].dropna(), 35, 3),
                               index=h2o['RHi'].dropna().index,
                               name='RHi_smooth')
        h2o = h2o.merge(RHi_smooth, left_index=True, right_index=True)
    except:
        print("!!! WARNING !!!\n"
              "Smoothing of RH_ice data did raise an error!\n"
              "No smoothing applied!")
        h2o['RHi_smooth'] = np.NaN
    
    # Define relative humidity w.r.t. water
    if 'pw_corr' in h2o.keys():
        RHw = pd.Series(data=h2o.pw_corr / p_sat_h2o(temp, over_surface='water') * 100, name='RHw')  # in %
    else:
        RHw = pd.Series(data=h2o.pw / p_sat_h2o(temp, over_surface='water') * 100, name='RHw')  # in %
        
    h2o = h2o.merge(RHw, left_index=True, right_index=True)
    h2o.loc[:, 'RHw_err'] = 5  # ???: is err for single and multipath TDL both 5% ?
    
    try:
        RHw_smooth = pd.Series(data=savgol_filter(h2o['RHw'].dropna(), 35, 3),
                               index=h2o['RHw'].dropna().index,
                               name='RHw_smooth')
        h2o = h2o.merge(RHw_smooth, left_index=True, right_index=True)
    except:
        print("!!! WARNING !!!\nSmoothing of RH_water data did raise an error!\
        \nNo smoothing applied!")
        h2o['RHw_smooth'] = np.NaN
    
    if verbose:
        print(get_ts(), "Calculated relative humidity from water vapor pressure (w.r.t. ice & water)")
    
    return h2o


def calc_h2o_pw_offset_correction(h2o, h2o_total, RefZeit, dt_average_time, verbose=False):
    """Correct offset of (SP)APICT by adjusting it to offset of total water (MBW/APET).
    
    The reference value is determined from average over 'dt_average_time' before expansion.
    
    Parameters
    ----------
    h2o : pd.DataFrame
        Includes water vapor pressure 'pw' in Pa.
    h2o_total : pd.DataFrame
        Includes total water vapor pressure 'pw' in Pa.
    RefZeit : pd.Timestamp
        Start time of expansion, get from aida.experimentliste.RefZeit.
    dt_average_time : int
        Time interval to average total water vapor pressure before experiment.
    verbose : bool, optional
        Print log information during code execution. The default is False.

    Returns
    -------
    h2o : pd.DataFrame
        index type: DatetimeIndex
        
        new column in input dataframe ``h2o``: ['pw_corr']
        
        unit of new column: [Pa].
    """
    if verbose:
        print(get_ts(), "Start offset correction for water vapor pressure ...")
    
    # if there is no data during 'dt_average_time' seconds before expansion
    if (h2o.index.min() > RefZeit + pd.Timedelta(seconds=dt_average_time)) or \
       (h2o_total.index.min() > RefZeit + pd.Timedelta(seconds=dt_average_time)):
        print("WARNING: Can not apply offset correction on water vapor "
              "pressure since not enough water measurement data available "
              "before experiment start!\n"
              "Continue with uncorrected water vapor pressure data...")
        
        h2o['pw_corr'] = h2o.pw
        return h2o
    
    else:
        pw_total_offset = h2o_total.pw.loc[RefZeit + pd.Timedelta(seconds=dt_average_time): RefZeit].mean()
        pw_h2o_offset = h2o.pw.loc[RefZeit + pd.Timedelta(seconds=dt_average_time): RefZeit].mean()
        
        pw_corr = pd.Series(data=h2o.pw + (pw_total_offset - pw_h2o_offset), name='pw_corr')
        h2o = h2o.merge(pw_corr, left_index=True, right_index=True)
        
        if verbose:
            print(get_ts(), "Calculated correction of water vapor pressure "
                  "(TDL) to offset of total water vapor pressure (MBW/APET)")
    
        return h2o


# %% Functions to process WELAS data


def convert_welas_spd2sd(spd, welas_sensortype, dt=10, size_range=(1e-1, 1e3), n_bins=80, verbose=False):
    """Convert welas single particle data into size distribution.

    Parameters
    ----------
    spd : pd.DataFrame
        Single particle data from optical particle counter.
        Use element of dict as returned by `tools_import.read_welas_spdfile()`.
    welas_sensortype : str
        Define welas device as welas1/welas2/welas3.
    dt : int/float, optional
        Widths of time bins in seconds.
        The default is 10.
    size_range : tuple/list of two numerics, optional
        Specifies the (min, max) boundaries of the size distribution in µm.
        The default is (1e-1, 1e3).
    n_bins : int, optional
        Number of size bins.
        A reasonable value is 20 bins per decade (wrt to `size_range`).
        The default is 80.
    verbose : bool, optional
        Print log information during code execution. The default is False.

    Returns
    -------
    sd : dict
        Contains size distributions of number, pulse length and number concentration.
        
        sd['num'] : pd.DataFrame
            Number size distribution of welas single particle data in total counts.
        sd['pl'] : pd.DataFrame
            Pulse length size distribution of welas single particle data in µs.
        sd['cn'] : pd.DataFrame
            Number concentration size distribution of welas single particle data in cm⁻³.
            
        DataFrame structure: DatetimeIndex with freq=`dt` and `n_bins` columns logaritmically
        distributed over `size_range`.
    """
    import pandas as pd
    import numpy as np
    import matplotlib.dates as mdates
    from scipy import stats

    if verbose:
        print(get_ts(), f"Creating size distribution from {welas_sensortype} "
              f"single particle data with time bin width {dt} sec and "
              f"{n_bins} size bins ranging from {size_range[0]} to {size_range[1]} µm.")

    time_bins = pd.date_range(start=spd.index[0].replace(microsecond=0),
                              end=spd.index[len(spd) - 1].replace(microsecond=0),
                              freq=f'{dt}s')
    size_bin_boundaries = np.logspace(np.log10(size_range[0]), np.log10(size_range[1]), num=n_bins + 1)

    num_sd, x_edge, y_edge, binnumber = stats.binned_statistic_2d(x=mdates.date2num(spd.index),
                                                                  y=spd['Dp'],
                                                                  values=spd['Dp'],
                                                                  statistic='count',
                                                                  bins=[mdates.date2num(time_bins),
                                                                        size_bin_boundaries])
    pl_mean_sd, _, _, _ = stats.binned_statistic_2d(x=mdates.date2num(spd.index),
                                                    y=spd['Dp'],
                                                    values=spd['Ti'],
                                                    statistic='mean',
                                                    bins=[mdates.date2num(time_bins), size_bin_boundaries])

    # define geometric properties for specific welas sensors to calculate optical detection volume odv
    if welas_sensortype == 'welas1':
        if spd.index[0] < pd.to_datetime('2012-09-01', format='%Y-%m-%d'):
            length = 0.0280             # in cm
            crosssec = 0.0280 * 0.0280  # in cm²
            odv = length * crosssec     # in cm³
        else:  # valid for exp after Sep 2012
            length = 0.0140             # in cm
            crosssec = 0.0224 * 0.0224  # in cm²
            odv = length * crosssec     # in cm³

    if welas_sensortype == 'welas2':
        length = 0.0313                 # in cm
        crosssec = 0.0493 * 0.0493      # in cm²
        odv = length * crosssec         # in cm³

    if welas_sensortype == 'welas3':
        if spd.index[0] < pd.to_datetime('2012-09-01', format='%Y-%m-%d'):
            length = 0.0313             # in cm
            crosssec = 0.0493 * 0.0493  # in cm²
            odv = length * crosssec     # in cm³
        else:  # valid for exp after Sep 2012
            length = 0.0140             # in cm
            crosssec = 0.0224 * 0.0224  # in cm²
            odv = length * crosssec     # in cm³

    time_index = pd.DatetimeIndex(mdates.num2date(x_edge[:-1])).tz_localize(None)  # removes UTC timezone info
    sd = {}
    sd['num'] = pd.DataFrame(num_sd, index=time_index, columns=y_edge[:-1])
    sd['pl'] = pd.DataFrame(pl_mean_sd, index=time_index, columns=y_edge[:-1])
    sd['cn'] = sd['num'].mul(sd['pl'].mean(axis=1), axis=0) * 1e-6 / (odv * dt)

    if verbose:
        print(get_ts(), f"Finished creation of {welas_sensortype} size distribution.")

    return sd


def calc_welas_cn_from_sd(sd, welas_cf, uncertainty_welas, verbose=False):
    """Calculate total particle concentration per time bin from time resolved size distribution.
    
    Parameters
    ----------
    sd : dict
        Contains several size distributions for a certain welas device.
        Number SD, pulse length SD and concentration SD.
        
        sd.keys() = ['num', 'pl', 'cn'].
    welas_cf : int/float
        Correction factor for the number concentration size distribution sd['cn'].
        The correcion factor must match the welas device used for sd!
    uncertainty_welas : int/float
        Relative uncertainty of welas number concentration.
    verbose : bool, optional
        Print log information during code execution. The default is False.

    Returns
    -------
    cn : pd.DataFrame
        Contains the number concentration as a function of time bins.
        
        index type = DatetimeIndex, freq = 10 seconds
        unit of 'cn' = cm⁻³
    """
    if verbose:
        print(get_ts(), "Start 'calc_welas_cn_from_sd' for calculation of "
              "total number concentration from welas size distribution")
    
    cn = pd.DataFrame({'cn': sd['cn'].sum(skipna=True, axis=1).values * welas_cf}, index=sd['cn'].index)
    cn['cn_err'] = cn['cn'] * uncertainty_welas
    
    if verbose:
        print(get_ts(), "Finished 'calc_welas_cn_from_sd', total number "
              "concentration calculated.")
    
    return cn


def calc_welas_fraction(cn, pT, uncertainty_welas, uncertainty_cpc3010, in_ergebnisse):
    """Calculate fraction of detected particle concentration compared to initial CPC particle number concentration.

    Parameters
    ----------
    cn : pd.DataFrame
        Contains welas particle number concentration, in cm⁻³.
    pT : TYPE
        Contains temperature and pressure data, in K.
    uncertainty_welas : int/float
        Relative uncertainty of welas number concentration.
    uncertainty_cpc3010 : int/float
        Relative uncertainty of the CPC number concentration.
    in_ergebnisse : dict
        Contains experiment specific database entries from aida.in_ergebnisse.

    Returns
    -------
    fn : pd.DataFrame
        Ratio between number of particles seen by welas and the initial
        number concentration seen by the CPC at the experiment start.
    """
    # dilution of initial CPC concentration cn_0
    cn_0_dil = in_ergebnisse['cn_0'] * pT['p'] / (in_ergebnisse['p_0'] * 100)
    cn_0_dil_interp_to_welas = np.interp(cn.cn.index.to_julian_date(),
                                         cn_0_dil.index.to_julian_date(),
                                         cn_0_dil)
    
    # CPC concentration at experiment start
    cn_0_dil_t0 = cn_0_dil.at[in_ergebnisse['tref']]  # start concentration at ExpStart
    cn_0_dil_t0_err = cn_0_dil_t0 * uncertainty_cpc3010
            
    fn = pd.DataFrame({'fn': cn.cn / cn_0_dil_interp_to_welas}, index=cn.index)
    fn['fn_err'] = np.sqrt((uncertainty_welas * cn.cn / cn_0_dil_t0)**2
                           + (cn.cn / cn_0_dil_t0**2 * cn_0_dil_t0_err)**2)
    return fn


def calc_welas_cnice_from_sd(sd, cn, uncertainty_welas, welas_cf, modus_welas,
                             dice, p, uncertainty_p, experimentliste,
                             in_ergebnisse, verbose=False):
    """Calculate ice number concentration from total particle number concentration.
    
    Parameters
    ----------
    sd : dict
        Contains several size distributions for a certain welas device.
        Number SD, pulse length SD and concentration SD.
        
        sd.keys() = ['num', 'pl', 'cn'].
    cn : pd.DataFrame
        Welas number concentration, derived from welas number size distribution.
    uncertainty_welas : int/float
        Relative uncertainty of welas number concentration.
    welas_cf : int/float
        Correction factor for the number concentration size distribution sd['cn'].
        The correcion factor must match the welas device used for sd!
    modus_welas : int
        Flag to define how to determine a particle as ice:
            * 0: unused
            * 1: dmin_mode
            * 2: zcorr_mode
    dice : float
        Size threshold in µm defining from which size a particle is treated as ice crystal.
    p : pd.DataFrame
        Pressure, [p] = Pa.
    uncertainty_p : int/float
        Absolute error of pressure data, [uncertainty_p] = Pa.
    experimentliste : dict
        Contains experiment specific database entries from aida.experimentliste.
    in_ergebnisse : dict
        Contains experiment specific database entries from aida.in_ergebnisse.
    verbose : bool, optional
        Print log information during code execution. The default is False.

    Raises
    ------
    ValueError
        If zcorr method is defined (modus_welas==2), but not enough data is
        available before the experiment start.

    Returns
    -------
    cn_ice : pd.DataFrame
        Contains fraction of particle number concentration cn which was
        counted as ice
        
        Index type: DatetimeIndex.

    """
    if verbose:
        print(get_ts(), "Start 'calc_cnice_from_sd' to calculate ice number "
              "concentration")

    # Modus 'dice': criteria for usage oinw.f size threshold to determine ice
    if (modus_welas == 1) or (modus_welas == 3):
        if verbose:
            print(get_ts(), "Use modus 'dice' to determine ice number concentration")
        
        sd_transposed = sd['cn'].T
        sd_transposed_filtered_for_size_threshold = sd_transposed[sd_transposed.index > dice]
        sd_ice = sd_transposed_filtered_for_size_threshold.T

        cn_ice = pd.DataFrame({'cn_ice': sd_ice.sum(skipna=True, axis=1).values * welas_cf},
                              index=sd_ice.index)
        cn_ice['cn_ice_err'] = cn_ice.cn_ice * uncertainty_welas  # + d(dmin) ... TBD, how to treat uncertainty of threshold?
    
    # Modus 'zcorr': calculate ice by substracting background from before ice formation; not valid for all freezing modes!
    elif modus_welas == 2:
        if verbose:
            print(get_ts(), "Use modus 'zcorr' to calculate ice number concentration")
        
        tref = experimentliste['RefZeit']
        
        if cn.index.min() < tref - pd.Timedelta(seconds=60):  # enough data is available to calculate reference concentration
            cn0_welas = cn[tref - pd.Timedelta(seconds=120): tref - pd.Timedelta(seconds=5)].cn.mean()
        else:
            raise ValueError("Not enough data before experiment start available for zcorr method!")
        
        p_interpol_to_welas = np.interp(cn.cn.index.to_julian_date(), p.index.to_julian_date(), p)
        cn_welas_zcorr = cn.cn - (cn0_welas * p_interpol_to_welas / (in_ergebnisse['p_0'] * 100))
        # cn_welas_zcorr_err = np.sqrt( (cn.cn*uncertainty_welas)**2 + (cn0_welas * 0.2 * np.interp(cn.cn.index.to_julian_date(), pT.p.index.to_julian_date(), pT.p) / (in_ergebnisse['p_0'].item()*100) )**2)
        cn_welas_zcorr_err = np.sqrt((cn0_welas * p_interpol_to_welas * uncertainty_welas * cn.cn / (in_ergebnisse['p_0'] * 100))**2
                                     + (cn.cn * p_interpol_to_welas * (0.2 * cn0_welas) / (in_ergebnisse['p_0'] * 100))**2
                                     + (cn0_welas * uncertainty_p / (in_ergebnisse['p_0'] * 100))**2
                                     # +(cn0_welas*p_interpol_to_welas/(in_ergebnisse['p_0'].item()*100)**2 * uncertainty_p(t0) )**2 \  # TBD how to determine p(t0)?
                                     )
        cn_ice = pd.DataFrame({'cn_ice': cn_welas_zcorr, 'cn_ice_err': cn_welas_zcorr_err},
                              index=cn_welas_zcorr.index)
            
    else:
        raise ValueError("'modus_welas' in db table aida.in_ergebnisse for %s \
        has to be either 1 (dice) or 2 (zcorr). Actual value is '%s'" % (str(modus_welas)))
            
    if verbose:
        print(get_ts(), "Finished 'calc_cnice_from_sd', ice number "
              "concentration calculated.")
    
    return cn_ice


def calc_welas_cnice_het_hom(cn_ice, uncertainty_welas, t, freezing_mode, n_ae,
                             n_ae_err, n_d, n_d_err, verbose=False):
    """Separates ice number concentration in two heterogeneous and homogeneous modes.
    
    Parameters
    ----------
    cn_ice : pd.DataFrame
        Welas ice number concentration.
    uncertainty_welas : int/float
        Relative uncertainty of welas number concentration.
    t : dict
        Contains timestamps for different key events.
    freezing_mode : str
        Freezing mode of this experiment. Can be 'hom', 'het' or 'mixed'.
    n_ae : float
        Number concentration of aerosol in the chamber at experiment start.
    n_ae_err : float
        Uncertainty of ``n_ae``.
        FIXME: Error propagation of this parameter is not properly implemented!
    n_d : float
        Number concentration of solution droplets in the chamber at experiment start..
    n_d_err : float
        Uncertainty of ``n_d``.
        FIXME: Error propagation of this parameter is not properly implemented!
    verbose : bool, optional
        Print log information during code execution. The default is False.

    Raises
    ------
    Warning
        If the parameter ``freezing_mode`` is invalid.

    Returns
    -------
    cn_ice : pd.DataFrame
        The input variable ``cn`` extended with two additional columns for a
        distinction between homogeneously and heterogeneously formed ice.
    """
    # heterogeneous freezing
    if freezing_mode == 'het':
        cn_ice['cn_ice_het'] = cn_ice.cn_ice
        cn_ice['cn_ice_het_err'] = np.sqrt((cn_ice.cn_ice_het * uncertainty_welas)**2)
        # cn_ice['cn_ice_het_err'] = ?

    # homogeneous droplet freezing
    elif freezing_mode == 'hom':
        cn_ice['cn_ice_hom'] = cn_ice.cn_ice
        cn_ice['cn_ice_hom_err'] = np.sqrt((cn_ice.cn_ice_hom * uncertainty_welas)**2)
        # cn_ice['fn_ice_hom_err'] = abs(cn_ice.cn_ice*uncertainty_welas / n_d) + abs(- cn_ice.cn_ice / (n_d)**2 * n_d_err)

    # mixed experiment, heterogeneous freezing with homogeneous freezing
    elif freezing_mode == 'mixed':
        # treat heterogeneous freezing, only until t < t['t_ice_onset_hom']
        dfh = pd.DataFrame()  # help variable
        dfh['cn_ice_het'] = cn_ice.cn_ice[: t['RefZeit'] + pd.Timedelta(seconds=t['t_ice_onset_hom'])]
        cn_ice = pd.concat([cn_ice, dfh], axis=1)  # add help variable to welas DataFrame
        cn_ice['cn_ice_het_err'] = np.sqrt((cn_ice.cn_ice_het * uncertainty_welas)**2)

        # define the concentration of heterogeneously formed ice until t = t['t_ice_onset_hom']
        cn_ice_het_max = cn_ice.loc[cn_ice.index <= t['RefZeit'] + pd.Timedelta(seconds=t['t_ice_onset_hom']), 'cn_ice'][-1]
        cn_ice_het_max_err = cn_ice_het_max * uncertainty_welas
        
        # treat homogeneous freezing, up from t > t['t_ice_onset_hom']
        dfh = pd.DataFrame()  # help variable
        dfh['cn_ice_hom'] = cn_ice.cn_ice[t['RefZeit'] + pd.Timedelta(seconds=t['t_ice_onset_hom']):] - cn_ice_het_max
        cn_ice = pd.concat([cn_ice, dfh], axis=1)  # add help variable to welas DataFrame
        cn_ice['cn_ice_hom_err'] = np.sqrt((cn_ice.cn_ice_hom * uncertainty_welas)**2
                                           + (cn_ice_het_max_err)**2)
        
    else:
        raise Warning("No valid 'freezing_mode' specified. 'freezing_mode' "
                      "must be 'het', 'hom' or 'mixed'!")
    
    if verbose:
        print(get_ts(), "Separated total welas ice number concentration in "
              "homogeneous or heterogeneous formed portions.")
    
    return cn_ice


def calc_welas_ice_fraction(cn_ice, pT, uncertainty_welas, uncertainty_cpc3010, in_ergebnisse):
    """Calculate fraction of ice number concentration to initial CPC particle number concentration.
    
    Parameters
    ----------
    cn_ice : pd.DataFrame
        Contains welas ice number concentration, in cm⁻³
    pT : pd.DataFrame
        Contains temperature and pressure data, in Pa and K.
    uncertainty_welas : int/float
        Relative uncertainty of welas number concentration.
    uncertainty_cpc3010 : int/float
        Relative uncertainty of the CPC number concentration.
    in_ergebnisse : dict
        Contains experiment specific database entries from aida.in_ergebnisse.

    Returns
    -------
    fn_ice : pd.DataFrame
        Contains fraction between welas ice number concentration and initial
        CPC number concentration at experiment start.
    """
    cn_0_dil = in_ergebnisse['cn_0'] * pT['p'] / (in_ergebnisse['p_0'] * 100)
    cn_0_dil_interp_to_welas = np.interp(cn_ice.cn_ice.index.to_julian_date(),
                                         cn_0_dil.index.to_julian_date(),
                                         cn_0_dil)
    
    # CPC concentration at experiment start
    cn_0_dil_t0 = cn_0_dil.at[in_ergebnisse['tref']]  # start concentration at ExpStart
    cn_0_dil_t0_err = cn_0_dil_t0 * uncertainty_cpc3010
    
    fn_ice = pd.DataFrame({'fn_ice': cn_ice.cn_ice / cn_0_dil_interp_to_welas},
                          index=cn_ice.index)
    fn_ice['fn_ice_err'] = np.sqrt((uncertainty_welas * cn_ice.cn_ice / cn_0_dil_t0)**2
                                   + (cn_ice.cn_ice / (cn_0_dil_t0)**2 * cn_0_dil_t0_err)**2)
    return fn_ice


def calc_welas_ice_fraction_het_hom(cn_ice, t, freezing_mode, n_ae, n_ae_err,
                                    n_d, n_d_err, uncertainty_welas, verbose=False):
    """Calculate ice fraction with respect to heterogeneously and homogeneously formed ice.
        
    Parameters
    ----------
    cn_ice : pd.DataFrame
        Contains welas ice number concentration, in cm⁻³
    t : dict
        Contains timestamps for different key events.
    freezing_mode : str
        Freezing mode of this experiment. Can be 'hom', 'het' or 'mixed'.
    n_ae : float
        Number concentration of aerosol in the chamber at experiment start.
    n_ae_err : float
        Uncertainty of ``n_ae``.
    n_d : float
        Number concentration of solution droplets in the chamber at experiment start..
    n_d_err : float
        Uncertainty of ``n_d``.
    uncertainty_welas : int/float
        Relative uncertainty of welas number concentration.
    verbose : bool, optional
        Print log information during code execution. The default is False.

    Raises
    ------
    Warning
        If the parameter ``freezing_mode`` is invalid.

    Returns
    -------
    fn_ice : pd.DataFrame
        The input variable ``cn_ice`` extended with two additional columns for a
        distinction between homogeneously and heterogeneously formed ice.
        
        Contains fraction between welas ice number concentration and the
        corresponding CPC number concentration for the certain freezing mode
        (het/hom) at experiment start.
    """
    fn_ice = pd.DataFrame()
    
    # heterogeneous freezing
    if freezing_mode == 'het':
        fn_ice['fn_ice_het'] = cn_ice.cn_ice / n_ae
        fn_ice['fn_ice_het_err'] = np.sqrt((uncertainty_welas * cn_ice.cn_ice / n_ae)**2 + (cn_ice.cn_ice / (n_ae)**2 * n_ae_err)**2)
    
    # homogeneous droplet freezing
    elif freezing_mode == 'hom':
        fn_ice['fn_ice_hom'] = cn_ice.cn_ice / n_d
        fn_ice['fn_ice_hom_err'] = np.sqrt((cn_ice.cn_ice * uncertainty_welas / n_d)**2 + (-cn_ice.cn_ice / (n_d)**2 * n_d_err)**2)

    # mixed experiment, heterogeneous freezing with homogeneous freezing
    elif freezing_mode == 'mixed':
        # treat heterogeneous freezing, only until t < t['t_ice_onset_hom']
        dfh = pd.DataFrame()  # help variable
        dfh['fn_ice_het'] = cn_ice.cn_ice_het / n_ae
        dfh['fn_ice_het_err'] = np.sqrt((cn_ice.cn_ice_het * uncertainty_welas / n_ae)**2 + (-cn_ice.cn_ice_het / (n_ae)**2 * n_ae_err)**2)
        fn_ice = pd.concat([fn_ice, dfh], axis=1)  # add help variable to welas DataFrame
        
        # treat homogeneous freezing, up from t > t['t_ice_onset_hom']
        dfh = pd.DataFrame()  # help variable
        dfh['fn_ice_hom'] = cn_ice.cn_ice_hom / n_d
        dfh['fn_ice_hom_err'] = np.sqrt((cn_ice.cn_ice_hom * uncertainty_welas / n_d)**2 + (-cn_ice.cn_ice_hom / (n_d)**2 * n_d_err)**2)
        fn_ice = pd.concat([fn_ice, dfh], axis=1)  # add help variable to welas DataFrame
        
    else:
        raise Warning("No valid 'freezing_mode' specified. 'freezing_mode' \
                      must be 'het', 'hom' or 'mixed'!")
    return fn_ice


# %% calc_aida_all() main function

def calc_aida_all(Campaign, ExpNo, dt_average_time=-60,
                  fit_wall_flux=False,
                  verbose=False, savedata=False):
    """Run an analysis of AIDA experiments.
    
    Parameters
    ----------
    Campaign : str
        Campaign name, e.g. 'AWICIT04'
    ExpNo : str/int/float
        Experiment number corresponding to *Campaign*, e.g. '05' or '5' or 5 or 5.0
    dt_average_time : int, optional
        defines the number of seconds from expansion start to average over to
        determine start conditions
    fit_wall_flux : bool, optional
        If True the wall flux of heat and water between walls and gas is calculated.
        The default is False.
    savedata : bool or str, optional
        If True, saves data to './DataSets/Campaign/Campaign_ExpNo_aida.pkl'.
        If you pass a string it is interpreted as output path.
        Note, preexisting files will be overwritten.
    verbose : bool, optional
        Print informational statements during function execution
            
        
    Returns
    -------
    dict
        * includes all experiment data, inclusive raw data and analyzed data
        * includes additional information from experimentliste and in_ergebnisse
        * includes information 'freezing_mode'
        * includes a 'dataset_creation_date'
              
        
    Raises
    ------
    ValueError
        * if aerosol/droplet concentrations (n_ae or n_d) can not be read properly
        * if temperature can not be imported to calculate relative humidity
    
    
    See Also
    --------
    :mod:`calc_aida_all.py`
        All functions that are defined in that module are used for the
        main functin :func:`calc_aida_all`
    
    :mod:`tools_import.py`
        collection of helper functions to import data from different sources
    
    :mod:`tools_parametrizations.py`
        collection of helper functions to calculate variables through
        parameterizations
        
    :func:`sql_query`
        * read experimental information from mySQL database aida.in_ergebnisse
        * read experimental information from mySQL database aida.experimentliste
        
    :func:`get_ts`
        generates timestamp for verbose output
    
    
    Notes
    -----
    This script is related to the IDL-based data analysis of AIDA experiments 'calc_aida_all.pro'
    This script was mainly tested with experiments from 'AWICIT' campaigns.
    
    
    Examples
    --------
    >>> calc_aida_all('TROPIC02', '05', savedata=True, verbose=True)
    
    """
    if verbose:
        print(get_ts(), "Start script 'calc_aida_all.py' for full analysis"
              "of AIDA Experiment")
    
    ExpNo = str(int(ExpNo)).zfill(2)  # add leading zero for ExpNo < 10
    
    # read experiment data from mySQL database aida.in_ergebnisse
    in_ergebnisse = tools_import.sql_query(database='aida',
                                           query="SELECT * FROM in_ergebnisse \
                                                  WHERE Kampagne = '%s' AND ExperimentNr = %s" % (Campaign, ExpNo),
                                           output_mode='dict')

    # read experiment data from mySQL database aida.experimentliste
    experimentliste = tools_import.sql_query(database='aida',
                                             query="SELECT * FROM experimentliste \
                                                    WHERE Kampagne = '%s' AND ExperimentNr = %s" % (Campaign, ExpNo),
                                             output_mode='dict')
        
    if len(in_ergebnisse) == 0:
        raise ValueError(f"No results returned from database table 'in_ergebnisse' for {Campaign}_{ExpNo}. "
                         "It seems you did not populate the database with your experiment info.")
    if len(experimentliste) == 0:
        raise ValueError(f"No results returned from database table 'experimentliste' for {Campaign}_{ExpNo}. ")
        
    aerosol_type = experimentliste['Typ']  # read aerosol type name
    
    # read AIDA configuration
    calculator_config = tools_import.read_calculator_config(Campaign)
        
    # read AIDA event log
    event_log = tools_import.read_aida_event_log(experimentliste['ExpStart'], experimentliste['ExpEnde'], prev_time_h=12)
    
    # Retrieve information about pump power from ods sheet or in_ergebnisse
    pump_power = None  # init var
    try:
        # read data from excel or .ods-sheet, supplementary information to in_ergebnisse
        
        def read_pumppower_from_excel(ods_as_df, Campaign, ExpNo):
            """Read pump power from a sheet document.
            
            Parameters
            ----------
            ods_as_df : :obj:`pd.DataFrame`
                contains data imported via
                
                >>> ods_as_df = pd.read_excel('exp_concentrations.ods')
                
                only the column ods_as_df.pump_power is needed
                
                cells should contains timestaps *ts* and pump power descriptions *ppd* in the form
                    ts1, ppd1; ts2, ppd2; ts3, ppd3; ...
            
            Returns
            -------
            :obj:`list` of :obj:`tuple`
                tuple structure is (second, 'pump power description').
            
            """
            tuplelist = []  # to be filled with tuples of timestamp and pumppower
    
            try:
                pumppowers = ods_as_df['pump_power'].loc[(ods_as_df.Campaign == Campaign) & (ods_as_df.ExpNo == float(ExpNo))]
                pumppowers = pumppowers.item().split('; ')
                
            except AttributeError:
                print('Pump power not properly defined in excel sheet. Returning empty list about pump power.')
                
            for element in pumppowers:
                interval = element.split(', ')
                if len(interval) != 1:  # only for entries where a certain second is defined
                    interval[0] = int(interval[0])
                    t = tuple(interval)
                else:  # if no specific starting second is defined assume that pumping started at 0
                    t = (0, interval[0])
                tuplelist.append(t)
            
            return tuplelist
        
        conc_doc_path = './exp_concentrations.ods'
        conc_doc = pd.read_excel(conc_doc_path)
        conc_doc_entry = conc_doc[(conc_doc['Campaign'] == Campaign) & (conc_doc['ExpNo'] == int(ExpNo))]
        
        pump_power = read_pumppower_from_excel(conc_doc, Campaign, ExpNo)
    
    except ImportError as err:
        import os
        if os.path.isfile(conc_doc_path) and 'odfpy' in err.msg and verbose:
            print(get_ts(), f"Info: You specified to read pump power from an external .ods file but the dependency `odfpy` is not installed: \n{err}")
            
    except FileNotFoundError as err:
        if verbose:
            print(get_ts(), f"Info: You specified to read pump power from an external excel/ods file but the file is not found: \n{err}")
    
    except (IndexError, ValueError) as err:
        if verbose:
            print(get_ts(), f"Error in reading pump power value from .ods sheet: \n{err}")
        
    if not pump_power:
        if verbose:
            print(get_ts(), "Use pump power information from database table `in_ergebnisse`")
        pump_power = in_ergebnisse['Pumpstaerke']
    
    # %% read aerosol and droplet number concentrations from ods-sheet
    
    # for experiments with various types of aerosol at once
    # TODO: This is just a temporary solution focused on H2SO4 → find a general solution !!!
    try:
        # mixed het. and hom. freezing particles
        if ('H2SO4_' in in_ergebnisse['Typ']):
            # define aerosol concentration in #/cm³
            n_ae = conc_doc_entry['n_ae'].item()
            if (n_ae is None) or (np.isnan(n_ae)):
                n_ae = 0
            # define droplet concentration in #/cm³
            n_d = conc_doc_entry['n_drop'].item()
            if (n_d is None) or (np.isnan(n_d)):
                n_d = 0
        
        # pure homogeneous freezing experiments
        elif 'H2SO4' in in_ergebnisse['Typ']:
            try:
                n_d = conc_doc_entry['n_drop'].item()
                n_ae = conc_doc_entry['n_ae'].item()
                
                if pd.isna(n_ae):
                    n_ae = 0
                if pd.isna(n_d):
                    n_d = 0
            except:
                n_d = in_ergebnisse['cn_0']
                n_ae = 0
                
        # for experiments with one single type of aerosol
        else:
            n_ae = conc_doc_entry['n_ae'].item()
            n_d = conc_doc_entry['n_drop'].item()
            
            if pd.isna(n_ae):
                n_ae = 0
            if pd.isna(n_d):
                n_d = 0
                
    except (NameError, ValueError):  # if file has not been imported
        if verbose:
            print("Could not get aerosol number concentration from .ods "
                  "table. Fetch cn_0 from in_ergebnisse table.")
        n_ae = in_ergebnisse['cn_0']
        n_d = 0
    
    # Define error of cpc data as 10%; TBD check for a valid reasonable error analysis!
    n_ae_err = 0.1 * n_ae
    n_d_err = 0.1 * n_d
    
    # Define freezing mode (het / hom / mixed) and print an information
    if n_ae > 0 and n_d == 0:
        freezing_mode = 'het'
        if verbose:
            print('Freezing mode: pure heterogeneous freezing.')
    
    elif n_ae == 0 and n_d > 0:
        freezing_mode = 'hom'
        if verbose:
            print('Freezing mode: pure homogeneous freezing.')

    elif n_ae > 0 and n_d > 0:
        # if overseeding, then only het freezing
        if conc_doc_entry['t_ice_onset_hom'].item() in ['inf', np.inf]:
            conc_doc.loc[conc_doc_entry.index, 't_ice_onset_hom'] = 0  # set to integer
            freezing_mode = 'het'
            if verbose:
                print("Freezing mode: pure heterogeneous freezing (homogeneous "
                      "freezing mode not triggered).")
        elif pd.isna(conc_doc_entry['t_ice_onset_hom'].item()):
            conc_doc.loc[conc_doc_entry.index, 't_ice_onset_hom'] = 0  # set to integer
            freezing_mode = 'het'
            if verbose:
                print("Freezing mode: pure heterogeneous freezing (homogeneous "
                      "freezing onset wasn't specified).")
        else:
            freezing_mode = 'mixed'
            if verbose:
                print("Freezing mode: mixture of heterogeneous and homogeneous "
                      "freezing.")
    
    elif n_ae == 0 and n_d == 0:
        warnings.warn(
            get_ts() + ("WARNING: No initial aerosol number concentration "
            "specified (n_d and n_ae are zero). "
            "Fallback to treat `freezing_mode` as heteroeneous freezing."), UserWarning)
        freezing_mode = 'het'
    
    else:
        raise ValueError("Aerosol/droplet concentration (n_ae or n_d) seems "
                         "to be falsely defined!")
    
    # Check: compare initial CPC number concentration with sum(n_ae, n_d); max. deviation set to 10%
    
    if (n_ae + n_d) > 0:
        if 0.9 < in_ergebnisse['cn_0'] / (n_ae + n_d) < 1.1:
            pass
        else:
            print(get_ts(), "WARNING: \nCheck if concentrations from spreadsheet "
                  "are correct.\n"
                  "They differ from initial CPC concentration!\n")
    
    # %% print experimental parameters
    print('\n============ Basic experiment Info ============')
    print('Campaign: ' + Campaign
          + '\nExpNo: ' + ExpNo
          + '\nAerosol type: ' + aerosol_type)
    print('Aerosol concentration: ' + str(n_ae) + ' #/cm³')
    print('Solution droplet concentration: ' + str(n_d) + ' #/cm³')
    print('Start Temperature: %s K' % str(round(in_ergebnisse['tgas_0'], 2)))
    print('===============================================')
    
    # %% Import data from .sav-file
    
#    # TBD: differ path to .sav file depending on OS (linux vs windows vs iOS)
#
#    sav_file_path = '/run/user/1000/gvfs/smb-share:server=imkaaf-srv1,share=aida-mess/Messkampagnen/'+ Campaign + '/DataSets/' + Campaign + '_aida_sav/' + Campaign + '_' + ExpNo + '_aida.sav'
#    Sav = tools_import.sav2dict(sav_file_path)
#
#    ImportElements = ['cn_welas', 'cn_welas_dmin', 'trel_welas', 'cn_welas2', 'cn_welas2_dmin', 'trel_welas2']
#    Sav_dict = {}
#    for element in ImportElements:
#        Sav_dict[element] = Sav[element][0]*1
#    #del Sav
    
    # %% define timestamps
    
    # read special timestamps from aida.experimentsliste; ExpStart = Start of data recording, RefZeit = Start of expansion experiment, ExpEnde = end of data recording.
    t = tools_import.sql_query(database='aida',
                               query="SELECT RefZeit, ExpStart, ExpEnde FROM experimentliste \
                                       WHERE Kampagne ='%s' AND ExperimentNr = %s" % (Campaign, ExpNo),
                               output_mode='dict')

    #  read timestamp for onset of homogeneous freezing
    try:
        if freezing_mode == 'mixed' or freezing_mode == 'hom':
            t['t_ice_onset_hom'] = conc_doc_entry['t_ice_onset_hom'].item()
            
    except (UnboundLocalError, NameError):
        if verbose:
            print(get_ts(), "WARNING: Could not read ods sheet to extract timestamp of homogenoeus freezing!")
            
    # %% Import ambient conditions: temperature and pressure
        
    temperature = tools_import.read_logger_temperature(campaign=Campaign,
                                                       chamber='aida',
                                                       resolution='sec',
                                                       exp_num=ExpNo,
                                                       verbose=verbose)
    
    pressure = tools_import.read_logger_pressure(campaign=Campaign,
                                                 chamber='aida',
                                                 resolution='sec',
                                                 exp_num=ExpNo,
                                                 verbose=verbose)
    
    temperature_adiabatic = calc_T_adiabatic(pressure,
                                             temperature.loc[t['RefZeit'], 'tg'],
                                             pressure.loc[t['RefZeit']],
                                             verbose=verbose)
    
    # Deprecated variable for mean temperature and pressure.
    # It is recommended to build up all functionalities on `temperature` and `pressure`
    pT = tools_import.read_pT_seclogger(Campaign, ExpNo, t['ExpStart'], t['ExpEnde'], verbose=verbose)
    pT = pT.merge(temperature_adiabatic, left_index=True, right_index=True)
    
    
    # %% Import CPC
    
    if in_ergebnisse['modus_cpc3010'] == 1:
        cpc3010 = tools_import.read_cn_cpc3010(Campaign, t_start=t['ExpStart'], t_end=t['ExpEnde'], verbose=verbose)
    
    if in_ergebnisse['modus_cpc3776'] == 1:
        cpc3776 = tools_import.read_cn_cpc3776(Campaign, t_start=t['ExpStart'], t_end=t['ExpEnde'], verbose=verbose)

    # %% Import and process SIMONE data

    if in_ergebnisse['modus_simone'] == 1:
        simone = tools_import.read_simone(Campaign, t['ExpStart'], t['ExpEnde'], verbose=verbose)

    elif in_ergebnisse['modus_simone'] == 0:
        if verbose:
            print(get_ts(), "SIMONE flagged '0'; no data imported")
    
    # %% Import h2o data (multi path or single path TDL) and total h2O data (MBW or APET)
    
    # import total water measurement
    if in_ergebnisse['key_h2o_total'].upper() == 'MBW':
        h2o_total = tools_import.read_h2o_total_mbw(Campaign, t['ExpStart'], t['ExpEnde'], verbose=verbose)
    elif in_ergebnisse['key_h2o_total'].upper() == 'APET':
        h2o_total = tools_import.read_h2o_total_apet(Campaign, t['ExpStart'], t['ExpEnde'], pT.tg, verbose=verbose)
    elif in_ergebnisse['key_h2o_total'] == 0:
        print(get_ts(), "WARNING: Flag 'key_h2o_total' in aida.in_ergebnisse "
              "is zero. No import of total humidity data!")
    else:
        raise ValueError("\nWARNING! 'key_h2o_total' in database table "
                         "'aida.in_ergebnisse' not specified or invalid!")
        
    # import water measurement
    if in_ergebnisse['key_h2o'].upper() == 'APICT' and in_ergebnisse['modus_tdl'] == 1:
        h2o = tools_import.read_h2o_apict(Campaign, t['ExpStart'], t['ExpEnde'], verbose=verbose)
    elif in_ergebnisse['key_h2o'].upper() == 'SPAPICT' and in_ergebnisse['modus_tdl'] == 1:
        h2o = tools_import.read_h2o_spapict(Campaign, t['ExpStart'], t['ExpEnde'], verbose=verbose)
    elif in_ergebnisse['key_h2o'] == 0:
        print(get_ts(), "WARNING: Flag 'key_h2o_total' in aida.in_ergebnisse is zero. No import of TDL humidity data!")
    else:
        raise ValueError("\nWARNING! 'key_h2o' or 'modus_tdl' in database table 'aida.in_ergebnisse' not specified or invalid!")
        
    # %% Process TDL and MBW data
    
    # TDL offset correction to total h2O offset
    if in_ergebnisse['key_h2o'] != '0':
        
        h2o = calc_h2o_pw_offset_correction(h2o, h2o_total, t['RefZeit'], dt_average_time, verbose=verbose)
        h2o = calc_RH_from_pw(h2o, pT.tg, verbose=verbose)
        
        h2o_max = h2o.RHi_smooth[t['RefZeit']: t['ExpEnde']].max()  # define maximum of RHi in plot range
        t['h2o_max'] = h2o.RHi_smooth[h2o.RHi_smooth == h2o_max].index[0]  # define timestamp of h2o_max

    if in_ergebnisse['key_h2o_total'] != '0':
        h2o_total = calc_RH_from_pw(h2o_total, pT.tg, verbose=verbose)

    # %% Calculate adiabatic updraft velocity
    
    updraft_velocity = calc_adiabatic_updraft_velocity(pT.tg, humid=False, press=None, p_h2o=None)

    # %% Import welas single particle data
    
    # list of keys for every welas that was used in the experiment
    welas_list = []
    if in_ergebnisse['modus_welas'] != 0:
        welas_list.append('welas1')
    if in_ergebnisse['modus_welas2'] != 0:
        welas_list.append('welas2')
    if in_ergebnisse['modus_welas3'] != 0:
        welas_list.append('welas3')

    # welas_mode = {0: 'device unused', 1: 'use threshold dmin for ice determination', 2: 'use average background substraction for ice determination'}
    modus_welas = {}
    modus_welas['welas1'] = in_ergebnisse['modus_welas']
    modus_welas['welas2'] = in_ergebnisse['modus_welas2']
    modus_welas['welas3'] = in_ergebnisse['modus_welas3']

    # correction factor for concentration
    welas_cf = {}
    welas_cf['welas1'] = in_ergebnisse['welas_cf']
    welas_cf['welas2'] = in_ergebnisse['welas2_cf']
    welas_cf['welas3'] = in_ergebnisse['welas3_cf']
    
    # read single particle data for all used welas sensors
    spd = tools_import.read_welas_spdfile(Campaign, ExpNo, verbose=verbose)
    assert sorted(list(spd.keys())) == sorted(welas_list)

    # %%  Convert welas single particle data to size distribution

    # dict with different size distributions: 'cn', 'pl', 'num'; concentration sd, pulse length sd, number sd
    sd = {}
    for device in welas_list:
        sd[device] = convert_welas_spd2sd(spd[device], device, verbose=verbose)

    # %% Calculate total number concentration from welas data
    
    uncertainty_welas = 0.2  # relative error
    uncertainty_cpc3010 = 0.1  # relative error
    uncertainty_p = 0  # TODO: pressure uncertainty not quantified yet
    
    cn = {}
    for device in welas_list:
        cn[device] = calc_welas_cn_from_sd(sd[device], welas_cf[device], uncertainty_welas, verbose=verbose)
    
    # create dict of ice size thresholds for welas devices; note: for welas3 no entry in db yet (Jan 2019)
    dice = {}
    dice['welas1'] = in_ergebnisse['welas_dice']
    dice['welas2'] = in_ergebnisse['welas2_dice']

    # calculate ice number concentration: count every particle above size threshold dmin as ice
    cn_ice = {}
    for device in welas_list:
        cn_ice[device] = calc_welas_cnice_from_sd(sd[device], cn[device], uncertainty_welas, welas_cf[device], modus_welas[device], dice[device], pT.p, uncertainty_p, experimentliste, in_ergebnisse, verbose=verbose)

    for device in welas_list:
        # TODO: bad practice to change a variable as a function of itself. should be reimplemented differently
        cn_ice[device] = calc_welas_cnice_het_hom(cn_ice[device], uncertainty_welas, t, freezing_mode, n_ae, n_ae_err, n_d, n_d_err, verbose=verbose)
        
    # (i) fn: fraction welas_cn to cpc_cn
    fn = {}
    for device in welas_list:
        fn[device] = calc_welas_fraction(cn[device], pT, uncertainty_welas, uncertainty_cpc3010, in_ergebnisse)
    
    # (ii) fn_ice: fraction welas_ice to cpc_cn (hom. + het. ice)
    fn_ice = {}
    for device in welas_list:
        fn_ice[device] = calc_welas_ice_fraction(cn_ice[device], pT, uncertainty_welas, uncertainty_cpc3010, in_ergebnisse)
    
    # cn_ice separated in heterogeneous freezing and homogeneous freezing
    fn_ice_sep = {}
    for device in welas_list:
        fn_ice_sep[device] = calc_welas_ice_fraction_het_hom(cn_ice[device], t, freezing_mode, n_ae, n_ae_err, n_d, n_d_err, uncertainty_welas, verbose=verbose)
        
    # %% Combine welas dataframes

    welas = {}
    for device in welas_list:
        welas[device] = cn[device]
        welas[device] = pd.concat([welas[device], cn_ice[device]], axis=1)
        welas[device] = pd.concat([welas[device], fn[device]], axis=1)
        welas[device] = pd.concat([welas[device], fn_ice[device]], axis=1)
        welas[device] = pd.concat([welas[device], fn_ice_sep[device]], axis=1)

    # %% Add heat wall flux and water wall flux
    
    if fit_wall_flux:
        from py_aida.fitting.fit_aida_wall_flux import fit_wall_flux
        wall_flux = fit_wall_flux(Campaign, ExpNo)
    
    # %% Collect function output in dictionary
    
    dataset_creation_date = str(pd.Timestamp.now().date())
    
    output_dict = {'Campaign': Campaign,  # string
                   'ExpNo': ExpNo,  # string
                   'in_ergebnisse': in_ergebnisse,  # pd.Dataframe
                   'experimentliste': experimentliste,  # pd.DataFrame
                   'calculator_config': calculator_config,  # pd.DataFrame
                   'event_log': event_log,  # pd.DataFrame
                   'pump_power': pump_power,  # list
                   'aerosol_type': aerosol_type,  # string
                   'freezing_mode': freezing_mode,  # string
                   'n_ae': n_ae,  # float in #/cm³
                   'n_ae_err': n_ae_err,  # float in #/cm³
                   'n_d': n_d,  # float in #/cm³
                   'n_d_err': n_d_err,  # float in #/cm³
                   't': t,  # pd.Dataframe with single values
                   'dt_average_time': dt_average_time,  # int ins seconds
                   
                   'pT': pT,  # Deprecated: use 'temperature' and 'pressure' instead!
                   'temperature': temperature,  # pd.Dataframe, in K
                   'temperature_adiabatic': temperature_adiabatic,  # pd.Series, in K
                   'pressure': pressure,  # pd.Series, in Pa
                   'updraft_velocity': updraft_velocity,  # adiabatic updraft velocity, in m/s
                   
                   'welas': welas,  # dict, welas types as keys
                   'spd': spd,  # TEST new implementation; welas spd
                   'sd': sd,  # pd.DataFrame
                   'dataset_creation_date': dataset_creation_date  # day of file creation
                   }
    
    if fit_wall_flux:
        output_dict['wall_flux'] = wall_flux,  # dict of DataFrames with wall_flux['temp'] in K and wall_flux['h2o'] in pw
    
    if in_ergebnisse['modus_simone'] != 0:
        output_dict['simone'] = simone  # pd.pd.Dataframe
    
    if in_ergebnisse['key_h2o'] != 0:
        output_dict['h2o'] = h2o  # pd.Dataframe
    
    if in_ergebnisse['key_h2o_total'] != 0:
        output_dict['h2o_total'] = h2o_total  # pd.Dataframe
        
    if in_ergebnisse['modus_cpc3010'] != 0:
        output_dict['cpc3010'] = cpc3010  # pd.Dataframe
    
    if in_ergebnisse['modus_cpc3776'] != 0:
        output_dict['cpc3776'] = cpc3776  # pd.Dataframe
    
    # %% save data as .pkl file
    
    if savedata is not False:  # save output_dict as.pkl file
        if savedata is True:
            savedir = Path(__file__).parent.parent / 'DataSets' / 'calc_aida_all' / Campaign
            savepath = savedir / f'{Campaign}_{ExpNo}_aida.pkl'
        else:
            savepath = savedata  # if flag savedata was not bool, take flag as path of .pkl savefile
        
        # create directory if not existent yet
        from py_aida.toolbox import ensure_dir_existence
        ensure_dir_existence(savepath, verbose=verbose)
        
        from py_aida.toolbox import save_dict2pkl
        save_dict2pkl(output_dict, savepath)
    
    if verbose:
        print(get_ts(), "Finished script 'calc_aida_all.py'", '\n' + '-' * 79)
   
    return output_dict


# %% Example function call:
# ex = calc_aida_all('AWICIT04', '13', verbose=True, savedata=False)

if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser(prog='calc_aida_all',
                                     usage='python calc_aida_all.py [options]',
                                     description='Aggregate all AIDA experiment data including some postprocessing.')
    
    parser.add_argument('--campaign', '-c', type=str,
                        help=("Campaign name"))
    parser.add_argument('--expnum', '-e', type=int,
                        help=("Experiment number for a given campaign"))
    parser.add_argument('--dt_average_time', '-dt', type=int, default=-60,
                        help=("The number of seconds from expansion "
                              "start to average over to determine start conditions.\n"
                              "The default is `-60`."))
    parser.add_argument('--savedata', type=str, default=False,
                        help=("Specify a path to save output data as pickle."))
    
    args = parser.parse_args()
    
    # convert str to bool
    if isinstance(args.savedata, str):
        if args.savedata.lower() in ['true', 't', 'yes', 'y', 'ja', 'j']:
            args.savedata = True
        elif args.savedata.lower() in ['false', 'f', 'no', 'n', 'nein']:
            args.savedata = False
    
    calc_aida_all(args.campaign,
                  args.expnum,
                  dt_average_time=args.dt_average_time,
                  savedata=args.savedata)
    
