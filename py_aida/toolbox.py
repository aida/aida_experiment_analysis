#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Helper functions to be called by other scripts.

This file is part of the project `py_aida`.
Copyright (c) 2019-2023, GPL v3, KIT/IMK-AAF
(Karlsruhe Institute of Technology, Institute of Meteorology and
Climate Research, Atmospheric Aerosol Research)
For full license details see the file LICENSE.md or
go to https://www.gnu.org/licenses/gpl-3.0.html.
"""


# %% Generate formatted time string for print statements

def get_ts(mode='print', timezone=None):
    """
    Create a string representing the current timestamp.
    
    Parameters
    ----------
    mode : 'str', **optional**
        mode='print'
            Create timestamp in format HH:MM:SS.uu
        mode='RFC_1123'
            Create timestamp in format of RFC 1123
        mode='ISO_8601'
            Create timestamp in format of ISO 8601
    
    timezone : datetime.timezone object
        timeshift from UTC to custom local time, e.g.
        
        >>> timezone = datetime.timezone(datetime.timedelta(seconds=3600))
        
        Takes system timezone automatically if 'timezone==None'.
    
    Returns
    -------
    str
        String of timestamp. Format depends on `mode`
    
    See Also
    --------
    RFC 1123: tools.ietf.org/html/rfc1123

    ISO 8601: en.wikipedia.org/wiki/ISO_8601

    Examples
    --------
    >>> get_ts()
    '13:26:47.64'
    >>> get_ts(mode='ISO')
    '2020-01-07T13:38:38.208603+01:00'
    
    >>> from toolbox import get_ts
    >>> print(get_ts(), "This is a information about my running script ...")
    16:26:10.92 This is an information about my running script ...
    """
    import pandas as pd

    if timezone is None:
        import time
        import datetime
        
        seconds_from_UTC = - time.timezone
        timezone = datetime.timezone(datetime.timedelta(hours=seconds_from_UTC / 3600))

    ts = pd.Timestamp.now(tz=timezone)

    if mode == 'print':
        # Timestamp string for print statements in terminal
        ts_str = ts.strftime('%T.' + str(round(ts.microsecond / 10000)).zfill(2))

    if 'RFC' in mode:
        # Timestamp string according to RFC1123
        ts_str = ts.strftime('%a, %d %b %Y %H:%M:%S ' + str(ts.tz))

    if 'ISO' in mode:
        ts_str = ts.isoformat()

    return ts_str


# %% Load saved dictionary, e.g. experiment data created by 'calc_aida_all.py'

def save_dict2pkl(obj, pkl_path, parent=False, verbose=False):
    """
    Save dictionary to .pkl file.
    
    Parameters
    ----------
    obj : dict
        An arbitrary python object, e.g. dict.
    pkl_path : str|pathlib.Path
        Path where to save `obj`. Has to end with .pkl or .pickle
    parent : bool, optional
        Create parent directory, if it doesn't exist.
        The default is False.
    verbose : bool, optional
        Print informational statements.
        The default is False.
    
    Returns
    -------
    Generates pickle file at *pkl_path*
    
    Notes
    -----
    Old packages could cause compatibility issues.
    
    Examples
    --------
    >>> d = {'a' : 'some', 'b' : 'content'}
    >>> savepath = './subfolder/to/file.pkl'
    >>> save_dict2pkl(d, savepath)
    

    """
    from pathlib import Path
    import pickle
    
    DeprecationWarning("This function is deprecated. Please use `pandas.to_pickle()` instead.")
    
    # convert str to Path
    if isinstance(pkl_path, str):
        pkl_path = Path(pkl_path)
    
    if parent:
        if not pkl_path.parent.exists():
            pkl_path.parent.mkdir()           
    
    with open(pkl_path, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
    
    if verbose:
        print(get_ts(), f"Exported data to {pkl_path}.")


def load_pkl2dict(path, verbose=False):
    """
    Import .pkl file containing a python dictionary.
    
    Parameters
    ----------
    path_of_file_pkl : :obj:`str`
        Path of file to be imported. Has to end with .pkl or .pickle
    
    Returns
    -------
    :obj:`dict`
        arbitrary content
    
    
    See Also
    --------
    :func:`save_dict2pkl`

    Notes
    -----
    Old packages could cause compatibility issues.
    
    Examples
    --------
    >>> path_of_file = './subfolder/to/file.pkl'
    >>> ex = load_pkl2dict('path_of_file')
    >>> type(ex)
    dict
    """
    import pickle
    
    DeprecationWarning("This function is deprecated. Please use `pandas.read_pickle()` instead.")
    
    if verbose:
        print(get_ts(), "Import data from", path)
    
    with open(path, 'rb') as f:
        return pickle.load(f)


# %% Function to be called to generalize functions on different data types

def generalize_input(x):
    """
    Change an object to an iterable numpy.array.
    
    Hence functions can be written in a general way and still
    treat different kinds of input data (scalar/floats/np.arrays/
    pd.core.series) with the same code base.
    
    Parameters
    ----------
    x : str/float/list/np.array/pandas.core.series.Series
        Input parameter to be transformed to numpy array type.
    
    Returns
    -------
    numpy.array
        Input data as iterable numpy.array.
    
    See Also
    --------
    stack overflow
        https://stackoverflow.com/questions/29318459/python-function-that-handles-scalar-or-arrays
    """
    import numpy as np
    if np.isscalar(x):
        return np.asarray([x])
    else:
        return np.asarray(x)
    

# %% Generate directory if it is not already existing

def ensure_dir_existence(path, mkdir=True, verbose=False):
    """Generate the directory for a given path to file or folder."""
    import os
    
    dirname = os.path.dirname(path)
        
    if not os.path.exists(dirname):

        if mkdir:
            os.makedirs(dirname)
            if verbose:
                print(get_ts(), "Make new directory '%s'" % dirname)
        return False
    else:
        if verbose:
            print(get_ts(), "Directory '%s' already exists, no need for mkdir." % dirname)
            return True


# %% Determine goodness of fit

def evaluate_fit(y, y_model, n_params=None):
    """
    Calculate multiple measures to determine the goodness of fit.

    Parameters
    ----------
    y : array
        Observed values..
    y_model : array
        Model values fitted to `y`.
    n_params : int, optional
        Number of fit parameters. This is needed for adjusted R_square.
        The default is None.

    Returns
    -------
    dict
        Contains several measures to evaluate the goodness of fit.
        
        MAE:
            Mean absolute error, see `mean_abs_error()`.
            
        RMSE:
            Root mean squared error, see `root_mean_squared_error()`.
            
        RAE:
            Relative absolute error, see `relative_abs_error()`.
            
        MSE:
            Mean squared error, see `mean_squared_error()`.
            
        RSE:
            Relative squared error, see `relative_squared_error()`.
        
        correlation_coefficient:
            Correlation coefficient between `y` and `y_model`
            
        R_square
            Describes the fraction of scattering in `y` being described by `y_model`.
            R_square is **not** reliable for models with `n_params` > 1!
        
        R_square_adj
            Describes the fraction of scattering in `y` being described by `y_model`.
            R_square should be used for models with `n_params` > 1!
    """
    import numpy as np
    
    # different residual measures
    def mean_abs_error(y, y_model):
        """Mean absolute error."""
        dy = y - y_model
        MAE = 1 / len(y) * np.sum(np.abs(dy))
        return MAE

    def relative_abs_error(y, y_model):
        """Relative absolute error."""
        dy = y - y_model
        RAE = np.sum(dy.abs()) / np.sum((y - y_model.mean()).abs())  # Relative Absolute Error
        return RAE

    # mean errors
    def mean_squared_error(y, y_model):
        """Mean squared error."""
        dy = y - y_model
        MSE = 1 / len(y) * np.sum((dy)**2)
        return MSE

    def relative_squared_error(y, y_model):
        """Relative squared error."""
        dy = y - y_model
        RSE = np.sum(dy**2) / np.sum((y - np.mean(y_model))**2)
        return RSE

    def root_mean_squared_error(y, y_model):
        """Root mean squared error. Square root of the variance of the residuals."""
        dy = y - y_model
        RMSE = np.sqrt(1 / len(dy) * np.sum(dy**2))
        # aka "standard error of the estimate" or  "fit standard error" or "standard error of the regression"
        # equivalent to RMSqE = np.sqrt(mean_squared_error(y, y_model))
        return RMSE

    #  statistical measures
    def variance(y, base='y_mean', degrees_of_freedom='n'):
        """Variance."""
        if isinstance(base, str) and base == 'y_mean':
            base = np.mean(y)
        if isinstance(degrees_of_freedom, str) and degrees_of_freedom == 'n':
            degrees_of_freedom = len(y)
            
        variance = 1 / degrees_of_freedom * np.sum((y - base)**2)
        return variance

    def covariance_xy(x, y):
        """Calculate covariance between x and y."""
        assert len(x) == len(y)
        x_mean = np.mean(x)
        y_mean = np.mean(y)
        covariance = 1 / len(x) * sum((x - x_mean) * (y - y_mean))
        return covariance

    def standard_deviation(y, base='y_mean', degrees_of_freedom='n'):
        """Calculate standard deviation."""
        var = variance(y, base=base, degrees_of_freedom=degrees_of_freedom)
        std = np.sqrt(var)
        return std
      
    # =============== Goodness of fit
    def correlation_coefficient(x, y):
        """Correlation coefficient between x and y."""
        covar_xy = covariance_xy(x, y)
        std_x = standard_deviation(x)
        std_y = standard_deviation(y)
        r_xy = covar_xy / (std_x * std_y)
        return r_xy

    def R_square(y, y_model, method='residuals'):
        """Calculate R_square.
        
        R_square describes the fraction of residuals being described by the regression.
        R_square is typically only valid for linear relations.
        
        Parameters
        ----------
        y: numeric/list-like
            y-values of raw data.
        y_model: numeric/list-like
            y-values of model fitted to `y`.
        methods: str, optional
            Eqivalent methods to choose from ['residuals', 'correlation_coefficient', 'variance'].
        """
        if method == 'residuals':
            def squared_residuals_sum(y, y_model):
                """Residual sum of squares."""
                SS_res = np.sum((y - y_model)**2)
                return SS_res
        
            def squared_total_sum(y, y_model):
                """Total sum of squares."""
                SS_tot = np.sum((y - np.mean(y_model))**2)
                return SS_tot
            
            SS_res = squared_residuals_sum(y, y_model)
            SS_tot = squared_total_sum(y, y_model)
            R_square = 1 - SS_res / SS_tot
            
        elif method == 'correlation_coefficient':
            R_square = correlation_coefficient(y, y_model)**2
            
        elif method == 'variance':
            variance_model = variance(y_model)
            variance_y = variance(y)
            R_square = variance_model / variance_y
            
        return R_square

    def adjusted_R_square(y, y_model, n_params):
        n = len(y)
        R2 = R_square(y, y_model, method='residuals')
        R_square_adj = 1 - ((1 - R2) * (n - 1) / (n - n_params - 1))
        return R_square_adj
    
    assert len(y) == len(y_model)
    stats = dict()
    
    # errors
    stats['MAE'] = mean_abs_error(y, y_model)
    stats['RMSE'] = root_mean_squared_error(y, y_model)
    
    stats['RAE'] = relative_abs_error(y, y_model)
    
    stats['MSE'] = mean_squared_error(y, y_model)
    stats['RSE'] = relative_squared_error(y, y_model)
    
    # statistics
    stats['variance'] = variance(y, base=y_model, degrees_of_freedom=len(y) - 1)
    stats['std'] = standard_deviation(y, base=y_model, degrees_of_freedom=len(y) - 1)
    stats['correlation_coefficient'] = correlation_coefficient(y, y_model)
    stats['R_square'] = R_square(y, y_model)
    if n_params:
        stats['R_square_adj'] = adjusted_R_square(y, y_model, n_params)
    else:
        print("To determine adjusted R_square you have to pass the number of fit parameters `n_params`.")
    
    return stats


def get_error_combinations(params, errs, prev_combinations=None):
    """
    Return all combinations of `params` varied with +/- their errors.

    Parameters
    ----------
    params : numeric array
        Values, e.g. fit parameters.
    errs : numeric array
        Absolute (symmetric) errors of `values`.
    prev_combinations : list, optional
        This keyword is for recursive use and should usually not be set manually.
        The default is None.

    Returns
    -------
    list
        Contains lists of all variations of `values` +/- their `errors`.

    """
    assert len(params) == len(errs)
    new_combinations = list()
    
    # initialize new_combinations
    if not prev_combinations:
        new_combinations.append([params[0] - errs[0]])
        new_combinations.append([params[0]])
        new_combinations.append([params[0] + errs[0]])
        
    # append prev_combinations
    else:
        for i, old_combi in enumerate(prev_combinations):
            new_combinations.append(old_combi.copy() + [params[0] - errs[0]])
            new_combinations.append(old_combi.copy() + [params[0]])
            new_combinations.append(old_combi.copy() + [params[0] + errs[0]])
    
    # base case
    if len(params) == 1:
        return new_combinations
    
    # recursive standard case
    else:
        all_combinations = get_error_combinations(params[1:], errs[1:], prev_combinations=new_combinations)
        return all_combinations
    

def get_function_boundaries(fct, x, params, params_err):
    """
    Return upper and lower error boundaries for function `fct` along `x` axis.

    Parameters
    ----------
    fct : function
        Function which takes `x` and `*params` as arguments.
    x : numeric array
        1D numeric array.
    params : list
        List of parameters as input of `func`.
    params_err : list
        List of symmetric errors of `params`, e.g. their standard deviations.

    Returns
    -------
    pd.DataFrame
        DataFrame with columns `lower` and `upper` for confidence intervals
        along `x` index.

    """
    import pandas as pd
    
    params_variations = get_error_combinations(params, params_err)
    y_variations = pd.DataFrame(index=x.index)
    for i, p in enumerate(params_variations):
        y_err = fct(x, *p)
        y_variations = pd.concat([y_variations, pd.Series(y_err, index=x.index, name=i)], axis=1)
    
    y_lower = y_variations.min(axis=1).rename('lower')
    y_upper = y_variations.max(axis=1).rename('upper')
    
    return pd.concat([y_lower, y_upper], axis=1)
