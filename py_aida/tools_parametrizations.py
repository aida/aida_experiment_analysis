#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Implementations of different parametrizations as functions to be called from other scripts.

This file is part of the project `py_aida`.
Copyright (C) 2019-2023, GPL v3, KIT/IMK-AAF
(Karlsruhe Institute of Technology, Institute of Meteorology and
Climate Research, Atmospheric Aerosol Research)
For full license details see the file LICENSE.md or
go to https://www.gnu.org/licenses/gpl-3.0.html.
"""
import warnings

# %% Gas properties


def density_dry_air(temp_gas, press):
    """
    Calculate the density of dry air.

    Parameters
    ----------
    temp_gas : float
        Temperature in Kelvin.
    press : float
        Pressure in Pa.

    Returns
    -------
    float
        Air density in kg/m³.
    
    """
    import scipy.constants as const
    press_0 = const.atmosphere
    density_0 = 1.2754  # IUPAC standard dry air density at at 273.15 K and 1000 hPa, in kg/m3.
    density = density_0 * (press / press_0) * (const.zero_Celsius / temp_gas)  # dry air density, kg/m3
    return density


def heat_capacity_air_dry(temp):
    """
    Calculate the specific heat capacity of dry air.

    Parameters
    ----------
    temp : int/float/array
        Temperature in [K].

    Returns
    -------
    c_p : float/array (depends on input)
        Specific heat capacity of dry air in [J kg⁻¹ K⁻¹].

    """
    temp_C = temp - 273.15
    c_p = 1.00507 - 5.18275e-6 * temp_C + 8.90574e-7 * temp_C**2
    c_p = c_p * 1000  # convert from [kJ/kg/K] to SI unit [J/kg/K]
    return c_p


def heat_capacity_air_humid(temp, press, p_h2o):
    """
    Calculate the specific heat capacity of humid air.

    Parameters
    ----------
    temp : int/float/array
        Temperature in [K].
    press : int/float/array
        Pressure in [Pa].
    p_h2o : int/float/array
        Water vapor pressure in [Pa].

    Returns
    -------
    c_p_air_humid : : float/array (depends on input)
        Specific heat capacity of humid air in [J kg⁻¹ K⁻¹].

    """
    c_p_air_dry = heat_capacity_air_dry(temp) * ((press - p_h2o) / press)
    c_p_water_vapor = 1884.06 * (p_h2o / press)
    
    c_p_air_humid = c_p_air_dry + c_p_water_vapor
    return c_p_air_humid


def thermal_conductivity_air(temp):
    """
    Calculate thermal coductivity of air.

    Parameters
    ----------
    temp : int/float/array
        Temperature in [K].

    Returns
    -------
    float/array (depends on input)
        Thermal coductivity of air in [J s⁻¹ m⁻¹ K⁻¹].

    """
    temp_C = temp - 273.15
    lambd = (24.5286 + 7.6749e-2 * temp_C - 4.21645e-5 * temp_C**2) * 1e-3
    return lambd


def dynamic_viscosity_gas(temp, gas_type='air'):
    """
    Calculate gas viscosity as a function of temperature (Sutherland relation).

    Parameters
    ----------
    temp : int/float/array
        Temperature in [K].
    gas_type : str, optional
        Specify your gas type. The default is 'air'.

    Returns
    -------
    float/array (depends on input)
        Dynamic viscosity in [Pa s] == [kg/m/s].

    """
    # Formulation of Bunz @ MAID:
    # visc = 15.32209e-7 * temp**1.5 / (129.53414 + temp)
    
    if gas_type == 'air':
        C = 110.4  # K
        T_0 = 293.15  # K
        mu_0 = 18.19e-6  # Pa s
    # other entries taken from: https://de.wikipedia.org/wiki/Sutherland-Modell
    elif gas_type == 'Ar':
        C, T_0, mu_0 = (165, 273.15, 21e-6)
    elif gas_type == 'CO':
        C, T_0, mu_0 = (118, 288.15, 17.2e-6)
    elif gas_type == 'CO2':
        C, T_0, mu_0 = (240, 293.15, 14.8e-6)
    elif gas_type == 'H2':
        C, T_0, mu_0 = (72, 293.85, 8.76e-6)
    elif gas_type == 'He':
        C, T_0, mu_0 = (79.4, 273, 19e-6)
    elif gas_type == 'Kr':
        C, T_0, mu_0 = (233.15, 273.15, 23.4e-6)
    elif gas_type == 'N2':
        C, T_0, mu_0 = (111, 300.55, 17.81e-6)
    elif gas_type == 'NH3':
        C, T_0, mu_0 = (370, 293.15, 9.82e-6)
    elif gas_type == 'O2':
        C, T_0, mu_0 = (127, 292.25, 20.18e-6)
    elif gas_type == 'SO2':
        C, T_0, mu_0 = (416, 293.65, 12.54e-6)
    else:
        "Print: Please specify gas type."
    
    visc = mu_0 * (T_0 + C) / (temp + C) * (temp / T_0)**1.5
    return visc  # in [kg/m/s] == [Pa s]


# %% Water vapor: saturation pressure and diffusion coefficient

def p_sat_h2o(temp, over_surface, param='MurphyKoop2005'):
    """
    Calculate water vapor saturation pressure with respect to water or ice.

    Parameters
    ----------
    temp : int/float/list/np.array/pd.Series
        Temperature, in K.
    over_surface : str
        Define over which surface the saturation is parameterized. You can
        choose 'water' or 'ice'.
    param : str, optional
        Choose the parameterization.
        
        For over_surface = 'water':
            * HylandWexler1983 [HW1983]_
            * Sonntag1994 [Sonntag1994]_
            * Tabazadeh1997 [Tabazadeh1997]_
            * MurphyKoop2005 [MK2005]_
            * Nachbar2019 [Nachbar2019]_
        
        for over_surface = 'ice':
            * MartiMauersberger1993 [MM1993]_
            * MurphyKoop2005 [MK2005]_
        
        The default is 'MurphyKoop2005'.

    Raises
    ------
    ValueError
        DESCRIPTION.

    Returns
    -------
    float/np.array/pd.Series
        Water vapor saturation pressure or the specified surface (water/ice),
        in Pa.
    
    References
    ----------
    .. [HW1983] Hyland, R. W. and A. Wexler, Formulations for the Thermodynamic
        Properties of the saturated Phases of H2O from 173.15K to 473.15K,
        ASHRAE Trans, 89(2A), 500-519, 1983.
    .. [MM1993] Marti, J. and K Mauersberger, A survey and new measurements of
        ice vapor pressure at temperatures between 170 and 250 K,
        GRL 20, 363-366, 1993
        https://doi.org/10.1029/93GL00105
    .. [Sonntag1994] Sonntag, D., Advancements in the field of hygrometry,
        Meteorol. Z., N. F., 3, 51-66, 1994.
    .. [Tabazadeh1997] Tabazadeh, A., O. B. Toon, S. Clegg, and P. Hamill,
        A new parameterization for the H2SO4/H20 composition: Implications
        for heterogeneous reactivity and ice nucleation in the atmosphere,
        Geophys. Res. Lett., 24, 1931, 1997.
    .. [MK2005] Murphy, D. M. & Koop, T.;
        "Review of the vapour pressures of ice and supercooled water
        for atmospheric applications"; Quarterly Journal of the Royal
        Meteorological Society, Wiley, 2005 , 131 , 1539-1565
    .. [Nachbar2019] Mario Nachbar and Denis Duft and Thomas Leisner;
        "The vapor pressure of liquid and solid water phases at
        conditions relevant to the atmosphere"; The Journal of Chemical
        Physics, vol. 151, 2019, doi:10.1063/1.5100364
    
    """
    import numpy as np
    
    # Check input temperature
    original_dtype = None  # init default value of variable
    if any([isinstance(temp, numeric_type) for numeric_type in [float, int]]):  # case for int/float inputs
        original_dtype = type(temp)
        temp = np.array([temp])
    elif isinstance(temp, list):  # case for array like inputs
        temp = np.array([temp])
        
    # Check input argument over_surface
    if not any(over_surface == np.array(['water', 'ice'])):
        raise ValueError(f"Argument `over_surface` needs to be specified as 'water' or 'ice'! The invalid input was '{over_surface}'")
        
    # Calculate water saturation pressure over water
    if over_surface == 'water':
        if param == 'HylandWexler1983':
            p_sat = np.exp(-5800.2206 / temp
                           + 1.3914993
                           - 0.048640239 * temp
                           + 0.000041764768 * temp**2
                           - 0.000000014452093 * temp**3
                           + 6.5459673 * np.log(temp))
        
        elif param == 'Sonntag1994':
            exponent = (-6096.9385 / temp
                        + 16.635794
                        - 0.02711193 * temp
                        + 1.673952e-5 * temp**2
                        + 2.433502 * np.log(temp))
            p_sat = 100 * np.exp(exponent)
        
        elif param == 'Tabazadeh1997':
            p_sat = 100 * np.exp((((18.452406985 * temp - 3505.1578807)
                                  * temp - 330918.55082) * temp
                                 + 12725068.262) * (1 / temp**3))
        
        elif param == 'MurphyKoop2005':
            p_sat = np.exp(54.842763
                           - 6763.22 / temp
                           - 4.210 * np.log(temp)
                           + 0.000367 * temp
                           + np.tanh(0.0415 * (temp - 218.8))
                           * (53.878 - 1331.22 / temp - 9.44523
                              * np.log(temp) + 0.014025 * temp))
        
        elif param == 'Nachbar2019':
            # print warning if temperature is out of valid interval
            if any([(temp < 200) or (temp > 273.15) for temp in temp]):
                warn_msg = "Temperature exceeds valid range in wvsp_LSW_Nachbar2019()! \
                            Valid values are between valid between 200 and 273 K"
                warnings.warn(warn_msg, UserWarning)
            
            p_sat = np.exp(74.8727
                           - 7167.40548 / temp
                           - 7.77107 * np.log(temp)
                           + 0.00505 * temp)
    
    # Calculate water saturation pressure over ice
    elif over_surface == 'ice':
        
        if param == 'MartiMauersberger1993':
            p_sat = np.exp(-6132.935395 / temp + 28.867509)
        elif param == 'MurphyKoop2005':
            p_sat = np.exp(9.550426
                           - 5723.265 / temp
                           + 3.53068 * np.log(temp)
                           - 0.00728332 * temp)
        
    if original_dtype:  # convert to float if input type was float/int
        p_sat = float(p_sat)
    
    return p_sat  # in Pa


def wvsp_w_MC(temp):
    """
    Calculate water vapor saturation pressure w.r.t. water.
    
    This parameterization is based on [MC2005]_.
    
    Parameters
    ----------
    T : int or float
        Temperature, in Kelvin.
    
    Returns
    -------
    :obj:`float`
        Water vapor saturation pressure with respect to water, in Pa.
        
    See Also
    --------
    :func:`tools_parametrizations.wvsp_w_MC()`
        Calculates water vapor saturation pressure w.r.t. ice.
    
    References
    ----------
    .. [MC2005] Murphy, D. M. & Koop, T.;
        "Review of the vapour pressures of ice and supercooled water
        for atmospheric applications"; Quarterly Journal of the Royal
        Meteorological Society, Wiley, 2005 , 131 , 1539-1565
    
    """
    import numpy as np
    # 2020-11-10
    warn_msg = "The function 'wvsp_w_MC()' is deprecated and will be " \
               "replaced by 'tools_parametrizations.p_sat_h2o()' in the future."
    warnings.warn(warn_msg, DeprecationWarning)
    
    p0_w = np.exp(54.842763
                  - 6763.22 / temp
                  - 4.210 * np.log(temp)
                  + 0.000367 * temp
                  + np.tanh(0.0415 * (temp - 218.8))
                  * (53.878 - 1331.22 / temp - 9.44523 * np.log(temp) + 0.014025 * temp))
    return p0_w


def wvsp_i_MC(temp):
    """
    Calculate water vapor saturation pressure w.r.t. ice.
    
    This parameterization is based on [MC2005]_.
    
    Parameters
    ----------
    T : int or float
        Temperature, in Kelvin.
    
    Returns
    -------
    :obj:`float`
        Water vapor saturation pressure with respect to ice, in Pa.
    
    See Also
    --------
    :func:`tools_parametrizations.wvsp_w_MC()`
        Calculates water vapor saturation pressure w.r.t. water.
    
    References
    ----------
    .. [MC2005] Murphy, D. M. & Koop, T.;
        "Review of the vapour pressures of ice and supercooled water
        for atmospheric applications"; Quarterly Journal of the Royal
        Meteorological Society, Wiley, 2005 , 131 , 1539-1565
    """
    import numpy as np
    
    # 2020-11-10
    warn_msg = "The function 'wvsp_i_MC()' is deprecated and will be " \
               "replaced by 'tools_parametrizations.p_sat_h2o()' in the future."
    warnings.warn(warn_msg, DeprecationWarning)
    
    p0_i = np.exp(9.550426
                  - 5723.265 / temp
                  + 3.53068 * np.log(temp)
                  - 0.00728332 * temp)
    return p0_i  # in Pa


def wvsp_LSW_Nachbar2019(temp):
    """
    Calculate water vapor saturation pressure of supercooled liquid water.
    
    This parameterization is based on [Nachbar2019]_.
    
    Parameters
    ----------
    temp : int or float or array-like
        Temperature, in Kelvin.
    
    Returns
    -------
    :obj:`float` or `array-like`
        Water vapor saturation pressure with respect to water, in Pa.
        
    Notes
    -----
    Parametrization is only verfied in the **temperature interval 200 - 273 K !**
    
    See Also
    --------
    :func:`tools_parametrizations.wvsp_hex_ice_Nachbar2019()`
        Calculates water vapor saturation pressure w.r.t. ice.
    
    References
    ----------
    .. [Nachbar2019] Mario Nachbar, Denis Duft, Thomas Leisner;
        "The vapor pressure of liquid and solid water phases at conditions
        relevant to the atmosphere "; The Journal of Chemical Physics, 2019, Iss. 6,
        `Link to publication <https://doi.org/10.1063/1.5100364>`_
    """
    import numpy as np
    
    # 2020-11-10
    warn_msg = "The function 'wvsp_LSW_Nachbar2019()' is deprecated and will be " \
               "replaced by 'tools_parametrizations.p_sat_h2o()' in the future."
    warnings.warn(warn_msg, DeprecationWarning)
    
    # print warning if temperature is out of valid interval
    if any([(temp < 200) or (temp > 273.15) for temp in temp]):
        from toolbox import get_ts
        print(get_ts(), "!!! WARNING !!! in wvsp_LSW_Nachbar2019(): \
        \nTemperature exceeds valid range for parameterization of water vapor \
        saturation pressure w.r.t. supercooled water! (only valid between 200 \
        and 273 K)\n" + 79 // 4 * '`°´°')
    
    p0_LSW = np.exp(74.8727
                    - 7167.40548 / temp
                    - 7.77107 * np.log(temp)
                    + 0.00505 * temp)
    return p0_LSW  # in Pa


def diffusion_coefficient_h2o(temp, press, param='PruppacherKlett1997'):
    """
    Calculate the diffusion coefficent of h2o molecules in air.
    
    Parameters
    ----------
    temp : int/float/array_like
        Temperature, in [K].
    press : int/float/array_like
        Pressure, in [Pa].
    param : str, optional
        Currently the only available parameterization is 'PruppacherKlett1997'
        based on [PruppacherKlett1997]_.
    
    Returns
    -------
    :obj:`float` or array_like
        Diffusion Coefficient in [m² s⁻¹].
    
    Notes
    -----
    Parametrization only valid for temperatures between -40°C and +40°C.
    
    References
    ----------
    .. [PruppacherKlett1997] H. R. Pruppacher, J. D. Klett, R. D. Rosen (Ed.);
        Microphysics of Clouds and Precipitation;
        Springer Netherlands, 2010; Chapter 13, equation 13-3
    """
    import numpy as np
    import scipy.constants as const

    temp_0 = const.zero_Celsius  # K
    p_0 = const.atm  # Pa
    diff_0 = 2.11e-5  # m²s⁻¹
    
    def change_dtype_list_to_np_array(data):
        if isinstance(data, list):
            data = np.array(data)
        return data

    temp = change_dtype_list_to_np_array(temp)
    press = change_dtype_list_to_np_array(press)
        
    if param == 'PruppacherKlett1997':
        temp_min = min(temp) if not np.isscalar(temp) else temp
        temp_max = max(temp) if not np.isscalar(temp) else temp
        if any([(temp_min < temp_0 - 40) or (temp_max > temp_0 + 40)]):
            warn_msg = "Temperature exceeds valid range in " \
                       "diffusion_coefficient_h2o(): Valid interval " \
                       "(min,max) is (233,313), your input " \
                       f"contained ({round(temp_min, 2)},{round(temp_max, 2)})."
            warnings.warn(warn_msg, Warning)
        
        temp_0 = const.zero_Celsius  # K
        p_0 = const.atm  # Pa
        diff_0 = 2.11e-5  # m²s⁻¹
        
        diff_coeff = diff_0 * (temp / temp_0)**1.94 * (p_0 / press)  # m²s⁻¹
    
    return diff_coeff


def DiffCoeff_H2O_PruppacherKlett1997(p, temp):
    """
    Calculate diffusion coefficent of h2o molecules in air.
    
    This parameterization is based on [PruppacherKlett1997]_.
    
    Parameters
    ----------
    T : int/float/array_like
        Temperature, in [K].
    p : int/float/array_like
        Pressure, in [Pa].
    
    Returns
    -------
    :obj:`float` or array_like
        Diffusion Coefficient in [m² s⁻¹].
    
    Notes
    -----
    Parametrization only valid for temperatures between -40°C and +40°C.
    
    References
    ----------
    .. [PruppacherKlett1997] Pruppacher, H. R. & Klett, J. D.
        Rosen, R. D. (Ed.)
        Microphysics of Clouds and Precipitation
        Springer Netherlands, 2010; Chapter 13
    """
    import scipy.constants as const
    from toolbox import get_ts
    
    # 2020-11-10
    warn_msg = "The function 'DiffCoeff_H2O_PruppacherKlett1997()' is deprecated and will be " \
               "replaced by 'tools_parametrizations.diffusion_coefficient_h2o()' in the future."
    warnings.warn(warn_msg, DeprecationWarning)
    
    temp_0 = const.zero_Celsius  # K
    p_0 = const.atm  # Pa
    diff_0 = 2.11e-5  # m²s⁻¹
    
    if any([(temp < temp_0 - 40) or (temp > temp_0 + 40) for temp in temp]):
        print(get_ts(), "!!! WARNING !!! in calc_n_ice_hom_Kaercher2002():\
        \nTemperature exceeds valid range for parameterization of \
        diffusion coefficient D! (only valid between 233 and 313 K)")
    
    diff_coeff = diff_0 * (temp / temp_0)**1.94 * (p_0 / p)  # m²s⁻¹
    return diff_coeff


# %% Homogeneous freezing threshold

def S_cr_hom_ssp(temp, param):
    """
    Homogeneous freezing threshold for Sulfuric acid soluable particles (SSPs).
    
    Parameters
    ----------
    temp : numeric
        Temperature in Kelvin.
    param : str
        Select the parameterization.
        
        `AIDA_calculator_config`:
            This parameterization is used in the AIDA database to
            plot the hom. freezing threshold in the control room.
            No further information on how it was derived is available!
        `Kaercher_Lohmann_2002`:
            Assumes diameter of 0.25 µm and a nucleation rate of 10e10 cm-3 s-1.
        `Kaercher_Lohmann_2003`:
            Assumees diameter of 0.25 µm and a nucleation time of dt=1s,
        `Schneider_2021`:
            Derived from AIDA experiments (see ref. Schneider_2021).
        

    Returns
    -------
    S_cr : numeric
        Saturation ratio w.r.t. ice for homogeneous freezing threshold of SSPs.
    
    References
    ----------
    
    * Kaercher_Lohmann_2002:
        Kärcher, B. & Lohmann, U.; "A parameterization of cirrus cloud
        formation: Homogeneous freezing of supercooled aerosols";
        Journal of Geophysical Research, American Geophysical Union (AGU),
        2002, 107; doi:10.1029/2001jd000470
    * Kaercher_Lohmann_2003:
        Kärcher, B. & Lohmann, U.; "A parameterization of cirrus cloud
        formation: Heterogeneous freezing";
        Journal of Geophysical Research, American Geophysical Union (AGU),
        2003, 108; doi:10.1029/2002jd003220
    * Schneider_2021:
        Schneider, J. et al.; "High homogeneous freezing onsets of
        sulfuric acid aerosol at cirrus temperatures"; Copernicus GmbH,
        2021, 21, 14403-14425; doi:10.5194/acp-21-14403-2021
    
    """
    import numpy as np
    
    if param == 'AIDA_calculator_config':
        # no details known about the origin of this parameterization!
        temp_celsius = temp - 273.15
        S_cr = 1.29612 - temp_celsius / 251.889169
    elif param == 'Kaercher_Lohmann_2002':
        # see eq.4 in doi:10.1029/2001jd000470
        S_cr = 2.583 - temp / 207.83
    elif param == 'Kaercher_Lohmann_2003':
        # see eq. 5 in doi:10.1029/2002jd003220
        S_cr = 2.418 - temp / 245.68
    elif param == 'Schneider_2021':
        a = -1.4
        b = 390
        S_cr = np.exp(a + b / temp)
    else:
        raise ValueError(f"You chose '{param}' as parameterization which is not implemented.")
    return S_cr


def S_Koop_C(temp_celsius):
    """Parametrization of Koop-line taken from aida.calculator_config."""
    
    # 2022-06-08
    warn_msg = "The function 'S_Koop_C()' is deprecated and will be " \
               "replaced by 'tools_parametrizations.S_cr_hom_ssp()' in the future."
    warnings.warn(warn_msg, DeprecationWarning)
    
    S_hom = 1.29612 - temp_celsius / 251.889169
    return S_hom


def S_cr_Koop_K_ETH(temp_kelvin):
    """Parametrization of Koop-line taken from ETH box model, valid for 250nm particles."""
    
    # 2022-06-08
    warn_msg = "The function 'S_cr_Koop_K_ETH()' is deprecated and will be " \
               "replaced by 'tools_parametrizations.S_cr_hom_ssp()' in the future."
    warnings.warn(warn_msg, DeprecationWarning)
    
    S_hom = 2.418 - temp_kelvin / 245.68
    return S_hom


def S_cr_Kaercher_K_2002(temp_kelvin):
    """
    Parametrization of Koop-line from Kärcher2002.
    
    Numerical fit for 250 nm particles, valid in interval 190-240 K.
    """
    
    # 2022-06-08
    warn_msg = "The function 'S_cr_Kaercher_K_2002()' is deprecated and will be " \
               "replaced by 'tools_parametrizations.S_cr_hom_ssp()' in the future."
    warnings.warn(warn_msg, DeprecationWarning)
    
    S_hom = 2.583 - temp_kelvin / 207.83  # Kärcher2002, eq. 4
    return S_hom


# %%

def n_ice_hom_Kaercher2002(T, p, S_ice, dT_dt):
    """
    Calculate the ice number concentration from homogeneous freezing of solution droplets.
    
    This parameterization is based on [Kärcher2002]_.
    
    Parameters
    ----------
    T : int/float/array_like
        Temperature at homogeneous freezing onset, in [K]
    p : int/float/array_like
        Pressure at homogeneous freezing onset, in [Pa]
    S_ice : int/float/array_like
        Ice saturation at homogeneous freezing onset.
    dT_dt : int/float/array_like
        Change of temperature at homogeneous freezing onset, in [K s⁻¹]
    
    Returns
    -------
    numpy.array
        Ice number concentration of homogeneously formed ice. Unit [m⁻³].
    
    See Also
    --------
    :py:func:`S_cr_Kaercher_K_2002()`
        Calculates critical saturation ratio w.r.t. ice for homogeneous
        freezing after [Koop2000]_ as done in [Kärcher2002]_.
    
    References
    ----------
    .. [Kärcher2002] Kärcher, B. & Lohmann, U.;
        "A parameterization of cirrus cloud formation:
        Homogeneous freezing of supercooled aerosols";
        Journal of Geophysical Research, American Geophysical Union (AGU),
        2002 , 107
    
    .. [Koop2000] Koop, T.; Luo, B.; Tsias, A. & Peter, T.;
        "Water activity as the determinant for homogeneous ice nucleation
        in aqueous solutions"; Nature, Springer Nature, 2000 , 406 , 611-614
        
    """
    import numpy as np
    import scipy.constants as const
    from tools_parametrizations import diffusion_coefficient_h2o
    
    # adjust input data to numpy arrays to support mutable input data (lists, arrays, pd.Dataframes)
    from toolbox import generalize_input
    T = generalize_input(T)
    p = generalize_input(p)
    S_ice = generalize_input(S_ice)
    dT_dt = generalize_input(dT_dt)
    
    # DEBUG: ensure the length for all input arrays is the same
    assert all(len(T) == len(input_variable) for input_variable in [T, p, S_ice, dT_dt])
    
    M_w = 18.01528e-3  # kg mol⁻¹, molar mass of water
    m_w = M_w / const.Avogadro  # kg, water molecule mass
    M = 29e-3  # kg mol⁻¹, molar mass of air
    c_p = 1005  # J kg⁻¹ K⁻¹, specific heat capacity of air
    rho_i = 0.925e3  # kg m⁻³, ice particle mass density
    L_s = 2.836e6  # J kg⁻¹, latent heat of sublimation
    alpha = 0.5  # deposition coefficient for H2O molecules on an ice surface
    
    def vertical_velocity(dT_dt):
        g = const.g  # m s⁻², gravity accelleration
        c_p = 1005  # J kg⁻¹ K⁻¹, specific heat capacity of air
        
        dh_dt = -c_p / g * dT_dt
        return dh_dt
    
    def deriv_J_at_T_hom(T):  # input temperature in Kelvin
        """
        Change of nucleation rate as a function of temperature.
        
        Approximation from Kärcher 2002.
        Homog freezing assumed at S_cr as in Koop2000.
        Nucleation rate J also based on water activity.
        """
        d_ln_J_dt = 4.37 - 0.03 * T  # Kärcher2002, eq. 5
        return d_ln_J_dt  # output in [1/K]
    
    def timescale_tau(T, dT_dt):
        """Calculate freezing timescale as in Kärcher et al., 2002."""
        c = np.array([])
        for temp in T:
            if temp < 216:  # in K
                c = np.append(c, 100 * (22.6 - 0.1 * temp))  # Kärcher2002, eq. 6d
            elif temp >= 216:  # in K
                c = np.append(c, 100)
                
        # ??? should deriv dJ/dt be taken as absolute value? but then tau is negative?
        inverse_tau = c * deriv_J_at_T_hom(T) * dT_dt  # Kärcher2002, eq 6c
        return 1 / inverse_tau  # in s
    
    def v_th(m, T):  # thermal speed of a molecule
        """Molecular thermal velocity /w mass m [kg] at temperature T [K]."""
        k_b = const.k  # Boltzmann constant
        
        v_th = np.sqrt(k_b * T / m)
        return v_th  # m s⁻¹
    
    def param_a1(T, L_s, M_w, c_p, M):
        """According to publication Kärcher2002, eq 1."""
        g = const.g
        R = const.R
        
        a1 = L_s * M_w * g / (c_p * R * T**2) - M * g / (R * T)
        return a1
    
    def param_a2(T):
        """
        According to publication Kärcher2002, eq 1.
        
        a2 = 1/n_sat = the inverse H2O number density at ice saturation
        """
        from tools_parametrizations import wvsp_i_MC
        
        # from ideal gas law: n_sat = molecular_num / V = p_sat * N_A / (RT)
        p_sat = wvsp_i_MC(T)
        n_sat = p_sat * const.Avogadro / (const.R * T)
        
        a2 = 1 / n_sat
        return a2
    
    def param_a3(T, p, L_s, M_w, m_w, c_p, M):
        """According to publication Kärcher2002, eq 1."""
        a3 = L_s**2 * M_w * m_w / (c_p * p * T * M)
        return a3
    
    def param_b1(T, S_ice, m_w, alpha, rho_i):
        """Derived from Kärcher2002, eq 7."""
        v_th_water = v_th(m_w, T)
        n_sat = 1 / param_a2(T)
        
        b1 = m_w * alpha * v_th_water * n_sat * (S_ice - 1) / (rho_i * 4)
        return b1
    
    def param_b2(T, p, m_w, alpha):
        """Derived from Kärcher2002, eq 7."""
        v_th_water = v_th(m_w, T)
        diff_coeff = diffusion_coefficient_h2o(T, p)
        
        b2 = alpha * v_th_water / (4 * diff_coeff)
        return b2
    
    # (1) calculate parameters used for calculation of n_ice_hom
    tau = timescale_tau(T, dT_dt)
    a1 = param_a1(T, L_s, M_w, c_p, M)
    a2 = param_a2(T)
    a3 = param_a3(p, T, L_s, M_w, m_w, c_p, M)
    b1 = param_b1(T, S_ice, m_w, alpha, rho_i)
    b2 = param_b2(T, p, m_w, alpha)
    
    # (2) calculate n_ice_hom
    n_ice_hom = (m_w / rho_i * (b2 / (2 * np.pi * b1))**(3 / 2) * a1 * S_ice
                 / (a2 + a3 * S_ice) * vertical_velocity(dT_dt)
                 / np.sqrt(abs(tau)))
    return n_ice_hom

# %% INAS density (n_s) parametrization


def n_s_Ullrich2017(T, S_i, param, alpha=None, beta=None, gamma=None, kappa=None, lambd=None):
    """
    Calculate inas density n_s following Ullrich 2017.
    
    Parameters
    ----------
    T : int/float/array-like
        Temperature in [K].
    S_i : int/float/array-like
        Supersaturation ratio with respect to ice.
    param : str/array-like
        'soot':
            Use parameters derived for soot (Ullrich, 2017)
        'dust':
            Use parameters derived for desert dust (Ullrich, 2017)
        'dust_updated':
            Use parameters derived for desert dust with additional data after the publication
        array-like/tuple:
            give an custom set of parameters ordered like
            (alpha, beta, gamma, kappa, lambd)
        
    Returns
    -------
    n_s : int/float/array-like
        Ice Nucleating Active Site density in [m⁻²].
    """
    import numpy as np
    
    if param == 'dust':
        alpha = 285.692
        beta = 0.017
        gamma = 256.692
        kappa = 0.080
        lambd = 200.745
    elif param == 'dust_updated':
        alpha = 297.521
        beta = 0.0088
        gamma = 320.525
        kappa = 0.042
        lambd = 201.424
    elif param == 'soot':
        alpha = 46.021
        beta = 0.011
        gamma = 248.560
        kappa = 0.148
        lambd = 237.570
    elif param == 'custom':
        if any([x is None for x in [alpha, beta, gamma, kappa, lambd]]):
            raise ValueError("At least one parameter in function n_s_Ullrich2017() in undefined!")
        
    n_s = np.exp(alpha * (S_i - 1)**(1 / 4)
                 * np.cos(beta * (T - gamma))**2
                 * (np.pi / 2 - np.arctan(kappa * (T - lambd))) / np.pi)
    
    return n_s


def n_s_Steinke2015(T, S_i, param='ATD'):
    """
    Calculate the ice-nucleating active site density for arizona test dust (ATD).
    
    This calculation is based in [Steinke2015]_

    Parameters
    ----------
    T : numeric/array-like
        Temperature in Kelvin.
    S_i : numeric/array-like
        Water vapor saturation ratio with respect to ice.
    param : str, optional
        Define the parameterization.
        The default is 'ATD'.

    Returns
    -------
    n_s : numeric/array-like
        Ice-nucleating active site density in m$^{-3}$.
        
    References
    ----------
    .. [Steinke2015] Steinke, I.; Hoose, C.; Möhler, O.; Connolly, P. & Leisner, T.;
        "A new temperature- and humidity-dependent surface site density
        approach for deposition ice nucleation ";
        Atmospheric Chemistry and Physics, Copernicus GmbH, 2015, 15, 3703-3717
    """
    import numpy as np
    x_therm = -(T - 273.2) + (S_i - 1) * 100
    n_s = 1.88 * 1E5 * np.exp(0.2659 * x_therm)
    return n_s
