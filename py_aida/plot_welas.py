#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot welas single particle data to define ice threshold 'dmin'.

This file is part of the project `py_aida`.
Copyright (c) 2019-2023, GPL v3, KIT/IMK-AAF
(Karlsruhe Institute of Technology, Institute of Meteorology and
Climate Research, Atmospheric Aerosol Research)
For full license details see the file LICENSE.md or
go to https://www.gnu.org/licenses/gpl-3.0.html.
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import py_aida
from py_aida.toolbox import get_ts


def plot_welas(Campaign, ExpNo, dmin_welas1=None, dmin_welas2=None, plot_flag_dmin=True, plot_flag_zcorr=True):
    """
    Create an overview plot for OPC data.

    Parameters
    ----------
    Campaign : str
        Name of the campaign.
    ExpNo : int
        Experiment number in `Campaign`.
    dmin_welas1 : float, optional
        Size threshold for welas1 sensor to count particles as ice.
        The default is None.
    dmin_welas2 : float, optional
        Size threshold for welas2 sensor to count particles as ice.
        The default is None.
    plot_flag_dmin : bool, optional
        Use `dmin` method to determine ice number concentration.
        The default is True.
    plot_flag_zcorr : bool, optional
        Use `zcorr` method to determine ice number concentration.
        The default is True.

    Returns
    -------
    matplotlib.figure.Figure
        Visualization of OPC data with ice thresholds.

    """
    experimentliste = py_aida.tools_import.sql_query(
        database='aida',
        query="SELECT * FROM experimentliste \
                WHERE Kampagne = '%s' AND ExperimentNr = %s" % (Campaign, ExpNo),
        output_mode='dict')
    
    in_ergebnisse = py_aida.tools_import.sql_query(
        database='aida',
        query="SELECT * FROM in_ergebnisse \
                WHERE Kampagne = '%s' AND ExperimentNr = %s" % (Campaign, ExpNo),
        output_mode='dict')
        
    t_start = experimentliste['ExpStart']
    t_end = experimentliste['ExpEnde']
    
    # import single particle data and create size distribution
    try:
        ex = pd.read_pickle(f'./DataSets/calc_aida_all/{Campaign}/{Campaign}_{ExpNo}_aida.pkl')
        spd = ex['spd']
        sd = ex['sd']
        pT = ex['pT'].loc[t_start: t_end]
        
    except FileNotFoundError:
        print(get_ts(), "Could not import welas data from .pkl file. Calculate directly!")
        spd = py_aida.tools_import.read_welas_spdfile(Campaign, ExpNo)  # import single particle data
        sd = {}
        for device in spd:  # calculate size distribution for single particle data
            sd[device] = py_aida.calc_aida_all.convert_welas_spd2sd(spd[device], device)
        
        pT = py_aida.tools_import.read_pT_seclogger(Campaign, ExpNo, t_start, t_end, verbose=False)
    
    dmin_database = {'welas1': in_ergebnisse['welas_dice'],
                     'welas2': in_ergebnisse['welas2_dice']}
    dmin_testing = {'welas1': dmin_welas1,
                    'welas2': dmin_welas2}
    
    modus_welas = {}  # flag determines how ice is defined; 0:unused, 1:dmin, 2:zcorr
    modus_welas['welas1'] = in_ergebnisse['modus_welas']
    modus_welas['welas2'] = in_ergebnisse['modus_welas2']
    modus_welas['welas3'] = in_ergebnisse['modus_welas3']
    
    modus_name = {1: 'dmin', 2: 'zcorr', 3: 'dmin'}
    
    welas_cf = {}  # correction factor for concentration
    welas_cf['welas1'] = in_ergebnisse['welas_cf']
    welas_cf['welas2'] = in_ergebnisse['welas2_cf']
    welas_cf['welas3'] = in_ergebnisse['welas3_cf']
    
    welas_uncertainty = 0.2  # relative uncertainty
    cn, cn_ice_dmin, cn_ice_zcorr = {}, {}, {}
    for device in spd:
        cn[device] = py_aida.calc_aida_all.calc_welas_cn_from_sd(
            sd[device], welas_cf[device], welas_uncertainty)
        
        if plot_flag_dmin:
            cn_ice_dmin[device] = py_aida.calc_aida_all.calc_welas_cnice_from_sd(
                sd[device], cn[device], welas_uncertainty, welas_cf[device], 1, dmin_testing[device], pT.p, 0, experimentliste, in_ergebnisse)
        if plot_flag_zcorr is not False:
            try:
                cn_ice_zcorr[device] = py_aida.calc_aida_all.calc_welas_cnice_from_sd(
                    sd[device], cn[device], welas_uncertainty, welas_cf[device], 2, dmin_testing[device], pT.p, 0, experimentliste, in_ergebnisse)
            except ValueError as error_msg:
                print(error_msg, " Skipping zcorr method!")
                plot_flag_zcorr = "device zcorr-error"  # flag showing that error occurred for device
    
    if len(cn_ice_zcorr) == 0:  # set flag=False if z_corr could not be calculated
        plot_flag_zcorr = False
    
    fig, axes = plt.subplots(nrows=2, ncols=len(spd), sharex='col', sharey='row', figsize=[10, 10])
    fig.suptitle(f"{Campaign}, exp. no. {ExpNo},\n"
                 f"{round(in_ergebnisse['cn_0'], 2)} cm$^{-3}$ {in_ergebnisse['Typ']} @ {round(in_ergebnisse['tgas_0'], 1)} K")
    
    # Plot single particle data in upper row
    ax_row_spd = axes[0]
    
    hline = {}
    dmin_hline_database = {}
    
    for col in range(len(spd)):
        if len(spd) > 1:
            ax = ax_row_spd[col]
        else:
            ax = ax_row_spd
            
        device = sorted(spd.keys())[col]  # select welas device
        
        ax.pcolormesh(sd[device]['cn'].loc[t_start: t_end].index,
                      sd[device]['cn'].loc[t_start: t_end].columns,
                      sd[device]['cn'].loc[t_start: t_end].T.replace(0, np.nan))
        
        dmin_hline_database[device] = ax.hlines(dmin_database[device], t_start, t_end,
                                                linestyle='-', color='orange',
                                                label='d$_{min}$ in DB')
        if dmin_testing[device]:
            hline[device] = ax.hlines(dmin_testing[device], t_start, t_end,
                                      linestyle='--', color='r',
                                      label='d$_{min}$ testing')
        
        ax.legend()
        ax.axvline(experimentliste['RefZeit'], color='grey', ls='--')
        ax.set_xlim([t_start, t_end])
        ax.set_ylabel('Particle diameter / (µm)')
        ax.set_yscale('log')
        ax.grid(color='lightgrey', ls='--')
        
        title_text = (f"{device}\n"
                      f"database ice threshold: {dmin_database[device]} µm")
        if dmin_testing[device]:
            title_text += f"\ntesting ice threshold: {dmin_testing[device]} µm"
        ax.set_title(title_text)
    
    # Plot number concentration data in bottom row
    ax_row_cn = axes[1]
    
    for col in range(len(spd)):
        if len(spd) > 1:
            ax = ax_row_cn[col]
        else:
            ax = ax_row_cn
            
        device = sorted(spd.keys())[col]  # select welas device
           
        ax.plot(cn[device].loc[t_start:t_end].index, cn[device].loc[t_start:t_end].cn, c='k', label='total')
        if plot_flag_dmin:
            ax.plot(cn_ice_dmin[device].loc[t_start:t_end].index, cn_ice_dmin[device].loc[t_start:t_end].cn_ice, 'cD', markeredgecolor='grey', label='ice dmin')
        if (plot_flag_zcorr):
            ax.plot(cn_ice_zcorr[device].loc[t_start:t_end].index, cn_ice_zcorr[device].loc[t_start:t_end].cn_ice, 'b*', markeredgecolor='grey', label='ice zcorr')
        
        ax.axvline(experimentliste['RefZeit'], color='grey', ls='--')
        
        ax.plot()
        
        ax.set_xlim([t_start, t_end])
        ax.set_ylabel('Number concentration  / (cm$^{-3}$)')
        ax.set_yscale('log')
    
        ax.grid(color='lightgrey', ls='--')
        ax.legend(loc=2)
        
        ax.set_title('correction factor: ' + str(welas_cf[device]) + '\nice modus: ' + modus_name[modus_welas[device]])
    
    # Add creation timestamp
    fig.text(0.99, 0.01, 'created: ' + pd.Timestamp.now().strftime('%Y-%m-%d %H:%M'),
             color='grey',
             horizontalalignment='right',
             verticalalignment='bottom')
    
    fig.autofmt_xdate()
    
    # check that dmin and zcorr are not mixed
    if 2 in modus_welas.values():
        assert not any([modus_id in modus_welas.values() for modus_id in [1, 3]])
    
    # save figure
    savepath = './Plots/plot_welas/%s/plot_welas_%s_%s_%s.png' % (Campaign, Campaign, ExpNo, modus_name[modus_welas[device]])
    py_aida.toolbox.ensure_dir_existence(savepath)
    plt.savefig(savepath, bbox_inches='tight', dpi=150)
    
    return fig
