#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Quality check on py_aida code by comparing output with output from old IDL routines.

This file is part of the project `py_aida`.
Copyright (c) 2019-2023, GPL v3, KIT/IMK-AAF
(Karlsruhe Institute of Technology, Institute of Meteorology and
Climate Research, Atmospheric Aerosol Research)
For full license details see the file LICENSE.md or
go to https://www.gnu.org/licenses/gpl-3.0.html.
"""

import os
import warnings
from pathlib import Path
import pickle
from collections import namedtuple

import getpass
from easypysmb import EasyPySMB

import numpy as np
import pandas as pd
from scipy.io import readsav

import matplotlib.pyplot as plt
import mplcursors


# %% define experiment class to hold and handle comarison of .sav and .pkl data

class AIDAExperiment:
    """Wrapper for AIDA datasets and comparison metrics.
    
    Init paratemters
    ----------------
    
    campaign : str
        Name of the campaign
    
    exp_num : int
        Experiment number in the given campaign.
        
        
    Methods
    -------
    
    self.load_sav_data(path):
        Load .sav file as dictionary-like object into attribute `self.sav`.
        
        path: str|Path
            Path to .sav file.
    
    self.load_pkl_data(path):
        Load .pkl file as dictionary into attribute `self.exp`.
        
        path: str|Path
            Path to .pkl file.
    
    self.get_sav_series(data_key):
        Get array data fields as pd.Series from .sav file.
        
        data_key: str
            A specifier from `self.sav.dtype.names` which leads to array-like data.
            
    self.get_sav_string(data_key):
        Get string element from .sav dataset as decoded utf8 string.
        
        data_key: str
            A specifier from `self.sav.dtype.names` which leads to a string.
    
    self.get_pkl_key_iterated(nested_key_list):
        Get the lowest object under several dictionary levels.
        The last level can also be the column name of a pd.DataFrame.
        
        nested_key_list: list of str
            Contains several keys, ordered from the highest to the lowest level.
    
    self.get_pkl_series(nested_key_list):
        Get array data as pd.Series from .pkl file.
        Wraps self.get_pkl_key_iterated(nested_key_list) to return a pd.Series.
    
        nested_key_list: list of str
            Contains several keys, ordered from the highest to the lowest level.
    """
    
    def __init__(self, campaign: str, exp_num: int):
        self.campaign = campaign
        self.exp_num = exp_num
        
        self.dataset_sources = []
        self.pkl = None
        self.sav = None
                
        self.data_comparison = dict()
        
        
    def __repr__(self) -> str:
        return f"Campaign: {self.campaign}; Experiment number:{self.exp_num}"
    
    
    def load_sav_data(self, path: Path) -> np.recarray:
        """Load .sav file."""
        
        def smb_retrieve_file(username:str, password:str, host:str, share:str, path_on_share:str, domain:str='kit.edu'):
            """Import .sav file through SMB/CIFS file transfer protocol."""
            
            def prompt_smb_username():
                username = input("Please enter user name (like 'ab1234') for data import via SMB:\n")
                return username

            def prompt_smb_password():
                password = getpass.getpass('\nPlease enter password for data import via SMB:\n')
                return password
            
            # define login data for SMB connection: user name + user password
            if not username:
                if 'SMB_USER' in os.environ:
                    username = os.environ.get('SMB_USER')
                else:
                    os.environ['SMB_USER'] = str(prompt_smb_username())
                    username = os.environ.get('SMB_USER')
            if not password:
                if 'SMB_PW' in os.environ:
                    password = os.environ.get('SMB_PW')
                else:
                    os.environ['SMB_PW'] = str(prompt_smb_password())
                    password = os.environ.get('SMB_PW')
            
            smb_conn = EasyPySMB(host, username=username, password=password, share_name=share, domain=domain)
            file = smb_conn.retrieve_file(path_on_share)
            smb_conn.close()
            return file
        
        if 'smb:' in str(path):  # read .sav files via smb
            print("Loading sav file via smb ...")
            
            # use saved credentials if found in environment variables, otherwise set to None
            username = None if not os.environ.get('SMB_USER') else os.environ.get('SMB_USER')
            password = None if not os.environ.get('SMB_PW') else os.environ.get('SMB_PW')
            
            _, host, share, path_on_share = str(path).replace('//', '/').split('/', 3)
            print("Info: interpreting connection details from given smb path:")
            print("    host: ", host)
            print("    share: ", share)
            print("    path_on_share: ", path_on_share)
            
            file = smb_retrieve_file(username, password, host, share, path_on_share)
            path = file.name

        sav = readsav(path, verbose=False)
        sav = sav.data
        
        self.sav = sav
        if 'sav' not in self.dataset_sources:
            self.dataset_sources.append('sav')
            
        return None
        
        
    def load_pkl_data(self, path: Path) -> dict:
        """Load .pkl file."""
        
        def loadpkl(path:Path):
            with open(path, 'rb') as f:
                return pickle.load(f)
            
        pkl_dataset = loadpkl(path)
        
        self.pkl = pkl_dataset
        if 'pkl' not in self.dataset_sources:
            self.dataset_sources.append('pkl')
            
        return None
                

    def get_sav_series(self, data_key: str) -> pd.Series:
        """Get array data fields as pd.Series from .sav file."""
        
        if 'welas2' in data_key.lower():
            index_key = 'TREL_WELAS2'
        elif 'welas' in data_key.lower():
            index_key = 'TREL_WELAS'
        elif '_simone' in data_key.lower():
            index_key = 'TREL_SIMONE'
        else:
            index_key = 'TGRID'
        
        sav_time_offset = pd.Timestamp(*self.sav['TS_0'][0][0])  # offset for relative time
        sav_absolute_tgrid = sav_time_offset + pd.TimedeltaIndex(self.sav[index_key][0], unit='s')
        sav_series = pd.Series(data=self.sav[data_key][0], index=sav_absolute_tgrid) * 1  # convert from big endian to little endian
        
        # correct NaNs in *_err series when original data has zero's
        if 'n_welas' in data_key.lower() and '_err' in data_key.lower():
            sav_series = sav_series.fillna(0)
        
        # harmonize pressure units
        if data_key == 'P':
            sav_series = sav_series * 100  # convert pressure from hPa to Pa
        
        return sav_series
    
    def get_sav_string(self, data_key: str) -> str:
        """Get string element from .sav dataset as decoded utf8 string."""
        raw_string = self.sav[data_key][0]
        
        if isinstance(raw_string, bytes):
            string = self.sav[data_key][0].decode('utf8')
        elif isinstance(raw_string, str):
            string = raw_string
        else:
            raise TypeError(f"Expected encoded string with type 'bytes' from "
                            f"data key '{data_key}', but received an object "
                            f"with type {type(raw_string)}.")
        
        assert isinstance(string, str)
        return string
        
    
    def get_pkl_key_iterated(self, nested_key_list: list):
        """Get the lowest object under several dictionary levels."""
        iterated = self.pkl.copy()
        for key in nested_key_list:
            iterated = iterated[key]
        return iterated
    
    
    def get_pkl_series(self, nested_key_list: list) -> pd.Series:
        """Wraps self.get_pkl_key_iterated() to get pd.Series."""
        return self.get_pkl_key_iterated(nested_key_list)
    
    
# %% manipulate data for comparability

def convert_sav_array_to_indexed_series(sav, data_key: str, index_key: str) -> pd.Series:
    """Transform array from sav file to pd.Series with absolute time index."""
    sav_time_offset = pd.Timestamp(*sav['TS_0'][0][0])  # offset for relative time
    sav_absolute_tgrid = sav_time_offset + pd.TimedeltaIndex(sav[index_key][0], unit='s')
    return pd.Series(data=sav[data_key][0], index=sav_absolute_tgrid)


def iterate_nested_dict(source: dict, nested_key_list: list):
    """Get the lowest object under several dictionary levels."""
    iterated = source
    for key in nested_key_list:
        iterated = iterated[key]
    return iterated


def interpolate_series(target_index: np.array or pd.DatetimeIndex, series: pd.Series) -> pd.Series:
    """Interpolate a pd.Series to another index."""
    
    # check for monotonic index arrays
    assert series.index.is_monotonic_increasing
    if isinstance(target_index, pd.DatetimeIndex):
        assert all(np.diff(target_index).astype(float) > 0)
    elif isinstance(target_index, [list, np.array, np.ndarray]):
        assert all(np.diff(target_index) > 0)
    
    return pd.Series(np.interp(target_index, series.index, series), index=target_index)


# %% level 1 comparison metrics: compare series of single data fields

class AvailableMetrics:
    """Wraps the implemented metrics of class SeriesComparator."""
    def __init__(self):
        self.continuous = [
            'absolute_difference',
            'relative_difference'
            ]
        self.scalar = [
            'mean_error',
            'mean_absolute_error',
            'mean_normalized_bias',
            'mean_absolute_relative_error',
            'fractional_bias',
            'fractional_absolute_error',
            'root_mean_square'
            ]

class SeriesComparator:
    """Compare two pd.Series with respect to specific metrics."""
    def __init__(self):
        self.available_metrics = AvailableMetrics()
        
    def get_joined_DatetimeIndex(self, i1: pd.DatetimeIndex, i2: pd.DatetimeIndex) -> pd.DatetimeIndex:
        """Get the intersection of two DatetimeIndex objects."""
        
        if not (isinstance(i1, pd.DatetimeIndex) and isinstance(i2, pd.DatetimeIndex)):
            raise TypeError("Both arguments have or be of type pd.Datetimeindex. "
                            f"You passed {type(i1)} and {type(i2)}.")
            
        joined_index = pd.DatetimeIndex.join(i1, i2, how='inner')
        
        for i in [i1, i2]:
            if (len(i) != len(joined_index)) and (len(joined_index) < 0.9 * len(i) or  len(joined_index) > 1.1 * len(i)):
                warnings.warn("Length of joined DatetimeIndex varies by more than 10% from the input array.")
            break
        
        return joined_index
        
    def continuous(self, s1: pd.Series, s2: pd.Series, metric: str) -> pd.Series:
        """Compare pd.Series continuously."""
        if metric == 'absolute_difference':
            compared_series = s1 - s2
        elif metric == 'relative_difference':
            compared_series = s1 / s2
        else:
            raise ValueError('Unimplemented metric.')
        return compared_series
    
    def scalar(self, s1: pd.Series, s2: pd.Series, metric: str) -> float:
        """Compare two pd.Series and derive scalar metric."""
        if metric == 'mean_error':  # ME
            parameter = 1 / len(s1) * (s1 - s2).sum()
        elif metric == 'mean_absolute_error':  # MAE
            parameter = 1 / len(s1) * abs(s1 - s2).sum()
            
        elif metric == 'mean_normalized_bias':  # MNB
            parameter = 1 / len(s1) * ((s1 - s2) / s1).sum()  # best match 0, (-1 < x < inf)
        elif metric == 'mean_absolute_relative_error':  # MARE
            parameter = 1 / len(s1) * (abs(s1 - s2) / abs(s1)).sum()  # best match 0, (-1 < x < inf)
            
        elif metric == 'fractional_bias':  # FB
            parameter = 1 / len(s1) * (2* (s1 - s2) / (s1 + s2)).sum()  # best match 0, (-2 < x < 2)
        elif metric == 'fractional_absolute_error':  # FAE
            parameter = 1 / len(s1) * (2 * abs(s1 - s2) / (abs(s1) + abs(s2))).sum()  # best match 0, (0 < x < 2)
            
        elif metric == 'root_mean_square':
            parameter = np.sqrt(1 / len(s1) * ((s1 - s2)**2).sum())
        else:
            raise ValueError('Unimplemented metric.')
        return parameter


# %% level 2: compare all data_fields of a single experiment

class ExperimentEvaluator:
    """Compare data field categories between .sav and .pkl dataset"""
    def __init__(self):
        pass
    
    def check_exp_data_source_availability(self, exp: AIDAExperiment):
        for dataset_source in ['sav', 'pkl']:
            if dataset_source not in exp.dataset_sources:
                raise AttributeError(f"Data source '{dataset_source}' not found in "
                                     "attribute `exp.data_sources`. "
                                     f"Please use method `exp.load_{dataset_source}_data()`.")
    
    def compare_array(self, exp: AIDAExperiment, field: str, metrics :list=None) -> AIDAExperiment:
        """
        Compare an array-type field between the .sav and .pkl datasets.
        Use all metrics if none is specified.

        Parameters
        ----------
        exp : AIDAExperiment
            Wrapper for multiple AIDA datasets.
        field_key : str
            Key in `mapped_array_fields` to a namedtuple with arguments 'pkl' and
            'sav', mapping the fields of both datasets.
        metrics : list, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        AIDAExperiment
            A modified instance of input AIDAExperiment with edited self.data_comparison[field][metric] field.

        """
        
        self.check_exp_data_source_availability(exp)
        SC = SeriesComparator()
        
        if not metrics:
            metrics = SC.available_metrics.scalar
        
        if not field in exp.data_comparison:
            exp.data_comparison[field] = dict()
        
        pkl_data = exp.get_pkl_series(mapped_array_fields[field].pkl)
        sav_data = exp.get_sav_series(mapped_array_fields[field].sav)
        
        # interpolate one index if they don't match
        if any([x in field.lower() for x in ['welas', 'simone']]):
            sav_data = interpolate_series(pkl_data.index, sav_data)

        # use only timestamps which are present in both datasets
        joined_index = SC.get_joined_DatetimeIndex(pkl_data.index, sav_data.index)
        
        for metric in metrics:
            exp.data_comparison[field][metric] = SC.scalar(sav_data.reindex(joined_index),
                                                                  pkl_data.reindex(joined_index),
                                                                  metric)
        return exp
    
    def compare_string(self, exp: AIDAExperiment, field: str) -> AIDAExperiment:
        """
        Compare a string-type field between the .sav and .pkl datasets.

        Parameters
        ----------
        exp : AIDAExperiment
            Wrapper for multiple AIDA datasets.
        field : str
            Key in `mapped_string_fields` to a namedtuple with arguments 'pkl' and
            'sav', mapping the fields of both datasets.

        Raises
        ------
        TypeError
            Check type of object from .pkl file, since AIDAExperiment.get_pkl_key_iterated()
            is generic about the returned object types.

        Returns
        -------
        AIDAExperiment
            A modified instance of input AIDAExperiment with edited self.data_comparison[field] field.

        """
        self.check_exp_data_source_availability(exp)
        
        if not field in exp.data_comparison:
            exp.data_comparison[field] = dict()
            
        sav_str = exp.get_sav_string(mapped_string_fields[field].sav)
        pkl_str = exp.get_pkl_key_iterated(mapped_string_fields[field].pkl)
        
        if not isinstance(pkl_str, str):
            raise TypeError(f"Expected string type element in .pkl file for field {field}. "
                            f"Found type `{type(pkl_str)}` instead.")
        
        bool_str_comparison = (pkl_str == sav_str)
        exp.data_comparison[field] = bool_str_comparison
        
        return exp
    
    def compare_numeric(self, exp: AIDAExperiment, field: str)-> AIDAExperiment:
        """
        Compare a numeric-type field between the .sav and .pkl datasets.

        Parameters
        ----------
        exp : AIDAExperiment
            Wrapper for multiple AIDA datasets.
        field : str
            Key in `mapped_numeric_fields` to a namedtuple with arguments 'pkl' and
            'sav', mapping the fields of both datasets.

        Raises
        ------
        TypeError
            If value behind 'field' is not numeric.

        Returns
        -------
        AIDAExperiment
            A modified instance of input AIDAExperiment with edited self.data_comparison[field] field.

        """
        self.check_exp_data_source_availability(exp)
        
        if not field in exp.data_comparison:
            exp.data_comparison[field] = dict()
            
        sav_num = exp.sav[mapped_numeric_fields[field].sav][0] * 1
        pkl_num = exp.get_pkl_key_iterated(mapped_numeric_fields[field].pkl)
        
        if not isinstance(sav_num, (float, int, np.int64)):
            raise TypeError(f"Expected numeric type in .sav file in field {field}. "
                            f"Found type `{type(sav_num)}` instead.")
        if not isinstance(pkl_num, (float, int, np.int64)):
            raise TypeError(f"Expected numeric type in .pkl file in field {field}. "
                            f"Found type `{type(pkl_num)}` instead.")
        
        exp.data_comparison[field] = (sav_num == pkl_num)
        
        return exp
        

# %%

if __name__ == '__main__':
    # These directories are user specific! Please adjust to your data locations!
    data_dir_sav = Path('./DataSets/idl/')
    if not data_dir_sav.exists():
        raise FileNotFoundError("Could not find the .sav file subdirectory './DataSets/idl/'. "
                                "Please create and populate it with your .sav files.")
    data_dir_pkl = Path('./DataSets/calc_aida_all/')
    if not data_dir_pkl.exists():
        raise FileNotFoundError("Can't find the directory './DataSets/calc_aida_all/', "
                                "which must contain campaign-specific directories with .pkl files.")
    
    metrics_for_arrays = [
        'fractional_bias',
        'fractional_absolute_error'
        ]
    
    
    # create experiment list using all available .sav files
    experiments = list()
    experiment_mapper = namedtuple('experiment_mapper', 'campaign exp_num')
    for file in data_dir_sav.glob('*.sav'):
        campaign, exp_num_str, _ = file.name.split('_')
        
        experiments_to_ignore = [
            ('AWICIT05', '19'),  # failed at .pkl generation
            ('AWICIT03', '14'),  # failed at .pkl generation
            ]
        to_be_ignored = False
        for experiment_to_ignore in experiments_to_ignore:
            if campaign == experiment_to_ignore[0] and exp_num_str == experiment_to_ignore[1]:
                to_be_ignored = True
        if not to_be_ignored:
            experiments.append(experiment_mapper(campaign, int(exp_num_str)))
    experiments = sorted(experiments)
        
    
    # map data structure of .sav and .pkl fields
    data_keys = namedtuple('data_keys', 'sav pkl')
    mapped_array_fields = {
        # 'field': data_keys(data_key_sav, [data_key_pkl1, data_key_pkl2, ...])
        'gas_temperature': data_keys('TG', ['temperature', 'tg']),
        'wall_temperature': data_keys('TW', ['temperature', 'tw']),
        'pressure': data_keys('P', ['pressure']),
        'cn_cpc3010': data_keys('CN_CPC3010', ['cpc3010', 'Particles_per_ccm']),
        
        'mbw_vmr_ppm': data_keys('CW_MBW', ['h2o_total', 'MBW_VMR_ppm']),
        'mbw_pw': data_keys('PW_MBW', ['h2o_total', 'pw']),
        'mbw_rhw': data_keys('RHW_MBW', ['h2o_total', 'RHw']),
        'mbw_rhi': data_keys('RHI_MBW', ['h2o_total', 'RHi']),
        
        'welas_cn': data_keys('CN_WELAS', ['welas', 'welas1', 'cn']),
        'welas_cn_err': data_keys('CN_WELAS_ERR', ['welas', 'welas1', 'cn_err']),
        'welas_cn_ice': data_keys('CN_WELAS_ICE', ['welas', 'welas1', 'cn_ice']),
        'welas_cn_ice_err': data_keys('CN_WELAS_ICE_ERR', ['welas', 'welas1', 'cn_ice_err']),
        'welas_fn_ice': data_keys('FN_WELAS_ICE', ['welas', 'welas1', 'fn_ice']),
        'welas_fn_ice_err': data_keys('FN_WELAS_ICE_ERR', ['welas', 'welas1', 'fn_ice_err']),
        
        'welas2_cn': data_keys('CN_WELAS2', ['welas', 'welas2', 'cn']),
        'welas2_cn_err': data_keys('CN_WELAS2_ERR', ['welas', 'welas2', 'cn_err']),
        'welas2_cn_ice': data_keys('CN_WELAS2_ICE', ['welas', 'welas2', 'cn_ice']),
        'welas2_cn_ice_err': data_keys('CN_WELAS2_ICE_ERR', ['welas', 'welas2', 'cn_ice_err']),
        'welas2_fn_ice': data_keys('FN_WELAS2_ICE', ['welas', 'welas2', 'fn_ice']),
        'welas2_fn_ice_err': data_keys('FN_WELAS2_ICE_ERR', ['welas', 'welas2', 'fn_ice_err']),

        # '': data_keys('', ['']),
        }
    
    mapped_string_fields = {
        # 'field': data_keys(data_key_sav, [data_key_pkl1, data_key_pkl2, ...])
        'campaign': data_keys('KAMPAGNE', ['Campaign']),
        'exp_num_str': data_keys('STR_EXP_NUM', ['ExpNo']),
        'exp_type': data_keys('EXP_TYPE', ['aerosol_type']),
        'pump_power': data_keys('EXP_PUMPSPEED', ['pump_power']),
        'key_h2o_total': data_keys('KEY_TW', ['in_ergebnisse', 'key_h2o_total']),
        #'': data_keys('', ['']),
        }
    
    mapped_numeric_fields = {
        # 'field': data_keys(data_key_sav, [data_key_pkl1, data_key_pkl2, ...])
        'exp_num': data_keys('EXP_NUM', ['experimentliste', 'ExperimentNr']),
        'trel_ice': data_keys('TREL_ICE', ['in_ergebnisse', 'tice_sec']),
        #'': data_keys('', ['']),
        }
    
    mapped_date_fields = {
        # 'field': data_keys(data_key_sav, [data_key_pkl1, data_key_pkl2, ...])
        'ts_exp_date': data_keys('EXP_DATE', ['t', 'RefZeit']),
        'TS_0': data_keys('TS_0', ['t', 'RefZeit']),
        'ts_exp_start': data_keys('TS_START', ['t', 'ExpStart']),
        'ts_exp_end': data_keys('TS_ENDE', ['t', 'ExpEnde']),
        #'': data_keys('', ['']),
        }
    
    
    # check user inputs
    if not data_dir_sav.exists():
        print("WARNING: The given directory for .sav files does not exist.")
        print(f"Given directory: '{data_dir_sav}'")
        data_dir_sav = Path(input("\nPlease enter a valid directory of your "
                                  ".sav files:\n"))
    if not data_dir_pkl.exists():
        print("WARNING: The given directory for .pkl files does not exist.")
        print(f"Given directory: '{data_dir_pkl}'")
        data_dir_pkl = Path(input("\nPlease enter a valid directory of your "
                                  ".pkl files:\n"))
    
    
    # compare .sav vs .pkl datasets for all given experiments
    EE = ExperimentEvaluator()
    SC = SeriesComparator()
    
    collected_metrics = dict()
    raised_exceptions = dict()
    for experiments_item in experiments:
        exp = AIDAExperiment(experiments_item.campaign, experiments_item.exp_num)
        exp_identifier = experiments_item.campaign + '_' + str(experiments_item.exp_num).zfill(2)
        
        exp.load_pkl_data(data_dir_pkl/f"{exp.campaign}/{exp_identifier}_aida.pkl")
        exp.load_sav_data(data_dir_sav/f"{exp_identifier}_aida.sav")
        
        # compare arrays
        for field in mapped_array_fields:
            try:
                exp = EE.compare_array(exp, field, metrics=metrics_for_arrays)
            except Exception as ex:
                ex_type = type(ex).__name__
                if not ex_type in raised_exceptions:
                    raised_exceptions[ex_type] = list()
                raised_exceptions[ex_type].append((exp.campaign, exp.exp_num, field, ex.args))
            if field not in collected_metrics:
                collected_metrics[field] = dict()
            collected_metrics[field][exp_identifier] = exp.data_comparison[field]
        
        # compare strings
        for field in mapped_string_fields:
            try:
                exp = EE.compare_string(exp, field)
            except Exception as ex:
                ex_type = type(ex).__name__
                if not ex_type in raised_exceptions:
                    raised_exceptions[ex_type] = list()
                raised_exceptions[ex_type].append((exp.campaign, exp.exp_num, field, ex.args))
            if field not in collected_metrics:
                collected_metrics[field] = dict()
            collected_metrics[field][exp_identifier] = exp.data_comparison[field]
        
        # compare numeric values
        for field in mapped_numeric_fields:
            try:
                exp = EE.compare_numeric(exp, field)
            except Exception as ex:
                ex_type = type(ex).__name__
                if not ex_type in raised_exceptions:
                    raised_exceptions[ex_type] = list()
                raised_exceptions[ex_type].append((exp.campaign, exp.exp_num, field, ex.args))
            if field not in collected_metrics:
                collected_metrics[field] = dict()
            collected_metrics[field][exp_identifier] = exp.data_comparison[field]
        
    # warn about orccurred exceptions
    for ex_type in raised_exceptions:
        print(f"ERROR Info: For {len(raised_exceptions[ex_type])} fields a {ex_type} occured!\n"
              "Look into the object `raised_exceptions` to see where they occurred.")
    
    import matplotlib.dates as mdates
    
    # Generate graphs of array-metrics
    plt.style.use('./stylelib/ownstyle.mplstyle')
    figures = dict()
    for field in mapped_array_fields:
        df = pd.DataFrame(index=collected_metrics[field].keys(), columns=metrics_for_arrays)
        for exp_identifier, derived_metrics in collected_metrics[field].items():
            if derived_metrics:
                df.loc[exp_identifier] = derived_metrics
            
        def get_timestamps_for_exp_identifiers(exp_identifiers:list) -> list:
            ts_list = list()
            for exp_identifier in exp_identifiers:
                campaign, exp_num = exp_identifier.split('_')
                
                AE = AIDAExperiment(campaign, exp_num)
                AE.load_pkl_data(data_dir_pkl / campaign / f"{exp_identifier}_aida.pkl")
                ts_list.append(AE.pkl['t']['RefZeit'])
            return ts_list
        
        exp_timestamps = get_timestamps_for_exp_identifiers(df.index.to_list())
        mapped_mdates_to_exp_identifier = dict(zip([round(mdates.date2num(x), 2) for x in exp_timestamps], df.index.to_list()))
        
        def show_campaign_expnum_annotation(sel):
            x = round(sel.target[0], 2)
            y = round(sel.target[1], 2)
            
            experiment_identifier = mapped_mdates_to_exp_identifier[x]
            campaign, exp_num = experiment_identifier.split('_')
                    
            sel.annotation.set_text(f"{campaign}\n"
                                    f"Experiment: {exp_num}\n"
                                    f"y-value: {y}")
            sel.annotation.get_bbox_patch().set(alpha=0.2)
        
        figures[field], axes = plt.subplots(len(df.keys()), 1,
                                            sharex=True,
                                            figsize=(5, 7), dpi=200)
        figures[field].suptitle("Quality control:\nCompare py_aida vs. plot_aida_all.pro datasets (.pkl vs .sav)"
                                "\n\n"
                                f"Data: '{field}'.")
        for i, metric in enumerate(df.keys()):
            ax = axes[i]
            
            if metric == 'fractional_bias':
                min_best_max = (-2, 0, 2)
            elif metric == 'mean_normalized_bias':
                min_best_max = (-1, 0, None)
            elif metric == 'fractional_absolute_error':
                min_best_max = (0, 0, 2)
            else:
                min_best_max = (None, None, None)
            
            ax.set_ylabel(metric.replace('_', ' ').capitalize())
            
            if min_best_max[0] is not None:  # lower boundary
                ax.set_ylim(bottom=min_best_max[0])
            if min_best_max[2] is not None:  # upper boundary
                ax.set_ylim(top=min_best_max[2])
            if min_best_max[1] is not None:  # best value
                ax.axhline(min_best_max[1], ls='--')
                
            ax.plot(exp_timestamps, df[metric],
                    ls='', marker='o',
                    alpha=0.5)
            
            figures[field].autofmt_xdate()
        
        cursor = mplcursors.cursor(hover=False)
        cursor.connect('add', show_campaign_expnum_annotation)
        axes[-1].set_xlabel('Date of AIDA experiments')
        
    plt.show()
    input("Press return to finish and close all figures...")
