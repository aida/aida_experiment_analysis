#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Calculate heat and water wall flux for AIDA.

This file is part of the project `py_aida`.
Copyright (c) 2019-2023, GPL v3, KIT/IMK-AAF
(Karlsruhe Institute of Technology, Institute of Meteorology and
Climate Research, Atmospheric Aerosol Research)
For full license details see the file LICENSE.md or
go to https://www.gnu.org/licenses/gpl-3.0.html.
"""

import numpy as np
import scipy.constants as const

from py_aida.tools_parametrizations import p_sat_h2o
from py_aida.tools_parametrizations import diffusion_coefficient_h2o
from py_aida.tools_parametrizations import heat_capacity_air_humid
from py_aida.tools_parametrizations import thermal_conductivity_air
from py_aida.tools_parametrizations import dynamic_viscosity_gas
from py_aida.tools_parametrizations import density_dry_air


# %% NEW Approach: Split wall flux into single functions; separate heat and water flux

def get_aida_volume(radius):
    """Calculate AIDA volume with a Koepper geometry for head + bottom."""
    h_v = 5.445  # m, height of vertical parallel walls, defined in technical drawing
    V_vertical = 2 * np.pi * radius * h_v
    
    R = 4  # m, koepper_radius_big, defined in technical drawing
    a = 0.4  # m, koepper_radius_small, defined in technical drawing
    h = 0.77445  # m, height of koepper head, derived from technical drawing
    
    c = np.sqrt((R - a)**2 - (R - h)**2)
    V_head = np.pi / 3 * (2 * h * R**2 - (2 * a**2 + c**2 + 2 * a * R) * (R - h) + 3 * a**2 * c * np.arcsin((R - h) / (R - a)))
    V_bottom = V_head
    return V_vertical + V_head + V_bottom


def Prandtl_number(viscosity, c_p, therm_conduct):
    """Prandtl_number."""
    Pr = viscosity * c_p / therm_conduct
    return Pr


def Schmidt_number(viscosity, density, diff_coeff):
    """Schmidt_number."""
    Sc = viscosity / (density * diff_coeff)
    return Sc


def Grashof_number(d_char, temp_gas, temp_wall, density):
    """Grashof_number."""
    viscosity = dynamic_viscosity_gas(temp_gas)  # in kg/m/s
    beta = 1 / temp_gas  # volume expansion coefficient of air, ???? to be checked !!!
    Gr = const.g * density**2 * beta * abs(temp_wall - temp_gas) / viscosity**2 * d_char**3
    return Gr
    

def Raighley_number(Gr, Pr):
    """Raighley_number."""
    Ra = Gr * Pr
    return Ra


def Nusselt_number_vertical_plate(Ra_v, Pr):
    """Averaged Nusselt number for vertical plate, dimensionless.
    
    See "Fundamentals of Heat and Mass Transfer"", 7th ed., eq. 9.26
    """
    Nu_v = (0.825 + 0.387 * Ra_v**(1 / 6) / (1 + (0.492 / Pr)**(9 / 16))**(8 / 27))**2
    return Nu_v


def Nusselt_number_horizontal_plate_top_bottom(Ra_h):
    """Return averaged Nusselt number for horizontal plate (top+bottom), dimensionless."""
    Nu_h = 0.14 * Ra_h**(1 / 3) + 0.27 * Ra_h**(1 / 4)
    return Nu_h


def wall_heat_flux(temp_gas, temp_wall, d_char, Nu, surface_area):
    """Calculate heat flux between gas and wall."""
    therm_conduct = thermal_conductivity_air(temp_gas)  # thermal conductivity, in W/m/K
    dQ = therm_conduct * Nu / d_char * (temp_wall - temp_gas) * surface_area  # heat flux, in J/s
    return dQ


def temperature_change_from_heat_flux(dQ, c_p, volume, density, factor):
    """Translate heat dQ change to temperature change dT."""
    return factor * dQ / (c_p * volume * density)


def wall_water_flux(temp_wall, p_h2o, diff_coeff, d_char, Sc, Pr, iced_surface_area):
    """Calculate water flux between gas and wall as change in water vapor pressure."""
    p_h2o_0_wall = p_sat_h2o(temp_wall, 'ice')
    return diff_coeff / d_char * (Sc / Pr)**(1 / 3) * (p_h2o_0_wall - p_h2o) * iced_surface_area  # in kg*m2/s3 = J/s


def get_AIDA_Nusselt_numbers_v_h(temp_gas, temp_wall, density, c_p, d_char_heat_v, d_char_heat_h):
    """Return list of Nusselt numbers for vertical and horizonzal AIDA wall components."""
    # calculate physical parameters
    viscosity = dynamic_viscosity_gas(temp_gas)  # in kg/m/s
    therm_conduct = thermal_conductivity_air(temp_gas)  # thermal conductivity, in W/m/K
    
    # calculate fluid dynamic characteristic numbers
    Pr = Prandtl_number(viscosity, c_p, therm_conduct)
    Gr_v = Grashof_number(d_char_heat_v, temp_gas, temp_wall, density)
    Gr_h = Grashof_number(d_char_heat_h, temp_gas, temp_wall, density)
    Ra_v = Raighley_number(Gr_v, Pr)
    Ra_h = Raighley_number(Gr_h, Pr)
    Nu_avg_v = Nusselt_number_vertical_plate(Ra_v, Pr)
    Nu_avg_h = Nusselt_number_horizontal_plate_top_bottom(Ra_h)
    
    return [Nu_avg_v, Nu_avg_h]


def wall_flux_temp_fluiddynamic(temp_gas, temp_wall, press, c_p, dt, factor=1.6):
    """
    Calculate the heat flux between wall and volume of AIDA. This approach is based on fluiddynamics.

    Parameters
    ----------
    temp_gas : array-like
        Gas temperature, in K.
    temp_wall : array-like
        Wall temperature, in K.
    press : array-like
        Pressure, in Pa.
    c_p : array-like or float/int
        Specific heat capacity of air
    dt : int
        Time increment, in seconds.
    factor : float, optional
        Quantifies the contribution of forced convection.
        This factor scales with the mixing fan speed and increases with decreasing temperature.
        The default is 1.6.

    Returns
    -------
    delta_T_heat_flux : array-like
        Gas temperature change per time interval due to heat flux between walls and volume, in K.

    """
    radius = 1.98  # m, AIDA inner radius, defined in technical drawing
    volume = get_aida_volume(radius)  # AIDA volume, in m3
    height = volume / (np.pi * radius**2)  # in m
    surface_v = height * 2 * np.pi * radius  # vertical surface, in m2
    surface_h = 2 * (np.pi * radius**2)  # horizontal top and bottom surface, in m2
    
    # characteristic length for convective heat transport, vertical and horizontal, in m
    d_char_heat_v = height
    d_char_heat_h = 0.5 * radius
    
    density = density_dry_air(temp_gas, press)  # dry air density, kg/m3
    
    [Nu_avg_v, Nu_avg_h] = get_AIDA_Nusselt_numbers_v_h(temp_gas, temp_wall, density, c_p, d_char_heat_v, d_char_heat_h)
    
    wall_flux_heat_v = wall_heat_flux(temp_gas, temp_wall, d_char_heat_v, Nu_avg_v, surface_v) * dt  # transferred heat, in J
    wall_flux_heat_h = wall_heat_flux(temp_gas, temp_wall, d_char_heat_h, Nu_avg_h, surface_h) * dt  # transferred heat, in J
    delta_T_heat_flux = temperature_change_from_heat_flux(wall_flux_heat_v + wall_flux_heat_h, c_p, volume, density, factor)  # dT from convective heat transport, in K
    return delta_T_heat_flux
    

def wall_flux_h2o_fluiddynamic(temp_gas, temp_wall, press, p_h2o, dt, factor=1.6,
                               ice_coverage_total=0.3, ice_coverage_v=None, ice_coverage_h=None):
    """
    Calculate the water flux between iced walls and volume of AIDA. This approach is based on fluiddynamics.

    Parameters
    ----------
    temp_gas : array-like
        Gas temperature, in K.
    temp_wall : array-like
        Wall temperature, in K.
    press : array-like
        Pressure, in Pa.
    p_h2o : array-like
        Water vapor pressure, in Pa.
    dt : int
        Time increment, in seconds.
    factor : float, optional
        Quantifies the contribution of forced convection.
        This factor scales with the mixing fan speed and increases with decreasing temperature.
        The default is 1.6.
    ice_coverage_total : float, optional
        Fraction of AIDA walls being covered with ice.
        The default is 0.3.
    ice_coverage_v : float, optional
        Fraction of vertical AIDA walls being covered with ice, assuming a cylinder.
        This parameter is not used if `ice_coverage_total` is not None.
        The default is None.
    ice_coverage_h : float, optional
        Fraction of horizontal AIDA walls being covered with ice, assuming a cylinder.
        This parameter is not used if `ice_coverage_total` is not None.
        The default is None.

    Returns
    -------
    delta_p_h2o_water_flux : array-like
        Water vapor change per time interval due to water flux between walls and volume, in Pa.

    """
    def distribute_total_wall_ice_coverage_on_vertical_and_horizontal(f, surface_vertical, surface_horizontal):
        """Distribute total wall ice coverage to vertical and horizontal component."""
        surface_total = surface_vertical + surface_horizontal  # in m2
                
        # cases of freezing levels
        if f <= (surface_horizontal / 2) / surface_total:  # only coldest horizontal wall is ice covered
            A_iced_h = f * surface_total
            fA_iced_h = A_iced_h / surface_horizontal
            A_iced_v = 0
            fA_iced_v = 0
        elif f <= (surface_horizontal / 2 + surface_vertical) / surface_total:  # also vertical wall is ice covered
            A_iced_h = surface_horizontal / 2
            fA_iced_h = 0.5
            A_iced_v = (f - A_iced_h / surface_total) * surface_total
            fA_iced_v = A_iced_v / surface_vertical
        elif f > (surface_horizontal / 2 + surface_vertical) / surface_total:  # also warmest horizontal wall is ice covered
            A_iced_v = surface_vertical
            fA_iced_v = 1
            A_iced_h = surface_horizontal / 2 + (f - (surface_horizontal / 2 + surface_vertical) / surface_total) * surface_total
            fA_iced_h = A_iced_h / surface_horizontal
        return (fA_iced_v, fA_iced_h)
    
    radius = 1.98  # m, AIDA inner radius, defined in technical drawing
    volume = get_aida_volume(radius)  # AIDA volume, in m3
    height = volume / (np.pi * radius**2)  # in m
    surface_v = height * 2 * np.pi * radius  # vertical surface, in m2
    surface_h = 2 * (np.pi * radius**2)  # horizontal top and bottom surface, in m2
    
    if ice_coverage_total not in [False, None]:
        ice_coverage_v, ice_coverage_h = distribute_total_wall_ice_coverage_on_vertical_and_horizontal(ice_coverage_total, surface_v, surface_h)
    
    # characteristic length for convective heat transport, vertical and horizontal, in m
    d_char_heat_v = height
    d_char_heat_h = 0.5 * radius
    
    density = density_dry_air(temp_gas, press)  # dry air density, kg/m3
    c_p = heat_capacity_air_humid(temp_gas, press, p_h2o)  # in J/kg/K
    viscosity = dynamic_viscosity_gas(temp_gas)  # in kg/m/s
    therm_conduct = thermal_conductivity_air(temp_gas)  # thermal conductivity, in W/m/K
    diff_coeff = diffusion_coefficient_h2o(temp_gas, press)  # in m2/s
    
    Pr = Prandtl_number(viscosity, c_p, therm_conduct)
    Sc = Schmidt_number(viscosity, density, diff_coeff)
    [Nu_avg_v, Nu_avg_h] = get_AIDA_Nusselt_numbers_v_h(temp_gas, temp_wall, density, c_p, d_char_heat_v, d_char_heat_h)
    
    # characteristic length for convective mass transport, vertical and horizontal, in m
    d_char_mass_v = d_char_heat_v / Nu_avg_v / factor
    d_char_mass_h = d_char_heat_h / Nu_avg_h / factor
    
    wall_flux_water_v = wall_water_flux(temp_wall, p_h2o, diff_coeff, d_char_mass_v, Sc, Pr, ice_coverage_v * surface_v)  # in J/s
    wall_flux_water_h = wall_water_flux(temp_wall, p_h2o, diff_coeff, d_char_mass_h, Sc, Pr, ice_coverage_h * surface_h)  # in J/s
    delta_p_h2o_water_flux = (wall_flux_water_v + wall_flux_water_h) * dt / volume  # in J/m3 = Pa
    
    return delta_p_h2o_water_flux


# %% Wall flux according to Cotton, 2007

def wall_flux_temp_cotton2007(temp_gas, temp_wall, c_p, dt, k_temp):
    """Calculate heat flux between gas and wall."""
    delta_T_heat_flux = k_temp / c_p * (temp_wall - temp_gas) * dt
    return delta_T_heat_flux


def wall_flux_h2o_cotton2007(temp_gas, temp_wall, press, p_h2o, dt, k_vapor):
    """Calculate water flux between gas and wall as change in water vapor pressure."""
    density_air = density_dry_air(temp_gas, press)
    
    delta_VMR_h2o_water_flux = k_vapor * 0.622 / press / density_air * (p_sat_h2o(temp_wall, over_surface='ice') - p_h2o) * dt
    delta_p_h2o_water_flux = delta_VMR_h2o_water_flux * press
    return delta_p_h2o_water_flux


# %%

def plot_compare_aida_wall_flux_to_exp(ex, factor, ice_coverage_total):
    """
    Overview plot to compare calculated AIDA wall fluxes with experimental data.

    Parameters
    ----------
    ex : dict
        Contains AIDA experiment data, generated by calc_aida_all().
    factor : float
        Quantifies the contribution of forced convection.
    ice_coverage_total : float
        Fit parameter for the water wall flux. Describes the ratio of AIDA walls being covered with ice.

    Returns
    -------
    Figure

    """
    from scipy.signal import savgol_filter
    import pandas as pd
    import matplotlib.pyplot as plt
    
    if isinstance(ex, str) and '.pkl' in ex:
        ex = pd.read_pickle(ex)
    elif isinstance(ex, dict):
        pass
    else:
        raise TypeError("Your input to specify the experiment data seems to be invalid. Only a path or dictionary are implemented options.")
    
    t0 = ex['t']['RefZeit']
    t_end = ex['pT'].p.idxmin() + pd.Timedelta(minutes=1)
    
    ex['pT'] = ex['pT'].loc[t0: t_end]
    h2o_tot = ex['h2o_total'].loc[t0:].pw
    
    delta_tg = ex['pT'].tg.rolling(5).mean() - ex['pT'].tg_adiabatic
    d_tg_dt = delta_tg[delta_tg.index.floor('1s')].diff() / 1
    
    # separated implementation
    flux_heat = wall_flux_temp_fluiddynamic(ex['pT']['tg'], ex['pT']['tw'], ex['pT']['p'], ex['h2o'].pw, dt=1, factor=factor)
    # flux_water = wall_flux_h2o_fluiddynamic(ex['pT']['tg'], ex['pT']['tw'], ex['pT']['p'], ex['h2o'].pw, dt=1, factor=factor, ice_coverage_total=ice_coverage_total, ice_coverage_v=False, ice_coverage_h=False)  # flux in Pa
    flux_water = wall_flux_h2o_fluiddynamic(ex['pT']['tg'], ex['pT']['tw'], ex['pT']['p'], ex['h2o'].pw, dt=1, factor=factor, ice_coverage_total=ice_coverage_total, ice_coverage_v=False, ice_coverage_h=False)  # flux as delta(VMR)
    
    # dilution Pa
    flux_water_sum_diluted = pd.Series(data=0, index=flux_water.index)
    for t in flux_water[flux_water.notna()].index:
        flux_water_sum_diluted.loc[t:] = flux_water_sum_diluted.loc[t:] + flux_water.loc[t] * ex['pT'].p.loc[t:] / ex['pT'].p.loc[t]
    flux_water_sum_diluted = flux_water_sum_diluted.loc[t0:]
    
    # Create figure
    fig, (ax_T, ax_dT, ax_pw, ax_dpw) = plt.subplots(4, 1, sharex=True, figsize=[10, 10])
    fig.suptitle(f"{ex['Campaign']}, #{ex['ExpNo']}")
    
    model_color = 'k'
    model_lw = 5
    model_alpha = 0.5
    
    # Axis 1: temperature
    ax_T.plot(ex['pT'].tw, label='wall')
    ax_T.plot(ex['pT'].tg, label='gas')
    ax_T.plot(ex['pT'].tg_adiabatic, label='gas, adiabatic profile')
    
    ax_T.plot(ex['pT'].tg_adiabatic + flux_heat.cumsum(), label='MODEL: adiabatic + wall flux',
              lw=model_lw, alpha=model_alpha, c=model_color)
    
    ax_T.set_ylabel("Temperature [K]")
    ax_T.grid(ls='--')
    ax_T.legend()
    
    # Axis 2: dT/dt
    ax_dT.plot(d_tg_dt, ls='', marker='.', label='dT/dt')
    ax_dT.plot(d_tg_dt.index, d_tg_dt.rolling(10).mean(), lw=4, label='rolling mean, 10s', alpha=0.8)
    ax_dT.plot(d_tg_dt.index, savgol_filter(d_tg_dt.replace(np.nan, 0), 201, 2), lw=4, label='savgol filter', alpha=0.8)
    
    ax_dT.plot(flux_heat, label='MODEL: wall flux',
               lw=model_lw, alpha=model_alpha, c=model_color)
    
    ax_dT.set_ylabel("dT/dt [K/s]")
    ax_dT.grid(ls='--')
    ax_dT.legend()
    
    # Axis 3: water vapor pressure pw
    mbw_0 = ex['h2o_total'].pw.loc[t0]
    mbw0_with_flux = (mbw_0 + flux_water_sum_diluted)  # water wall flux
    mbw_undiluted = ex['h2o_total'].pw * 1 / (ex['pT'].p / ex['pT'].p.loc[t0])
    
    ax_pw.plot(ex['h2o'].pw_corr.loc[t0: t_end] * 1 / (ex['pT'].p / ex['pT'].p.loc[t0]), label='TDL, undiluted')
    ax_pw.plot(mbw_undiluted.loc[t0: t_end], label='MBW, undiluted')
    ax_pw.plot(mbw0_with_flux, label='MODEL: MBW$_0$ + flux, undiluted')
    
    ax_pw.grid(ls='--')
    ax_pw.legend()
    
    # Axis 4: water vapor change d_pw
    ax_dpw.set_ylabel(r'Change in water vapor pressure $\Delta$ $p_w$ [Pa]')
    ax_dpw.plot(h2o_tot * 1 / (ex['pT'].p / ex['pT'].p.loc[t0]) - h2o_tot.loc[t0], label=r'$\Delta$ $p_w$ from MBW')
    ax_dpw.plot(flux_water_sum_diluted, label='MODEL: water flux',
                lw=model_lw, alpha=model_alpha, c=model_color)
    
    ax_dpw.grid(ls='--')
    ax_dpw.legend()
    
    fig.axes[-1].set_xlabel("Time")
    
    plt.show()
    return fig
