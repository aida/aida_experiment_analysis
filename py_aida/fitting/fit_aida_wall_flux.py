#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Determine the fit parameters of `calc_aida_wall_flux.py`.

Fit AIDA wall flux to profiles:
    * gas_temperature = t_adiabatic + temp_flux
    * total water = MBW(t0) + water_flux (only valid for reference activations)

This file is part of the project `py_aida`.
Copyright (c) 2019-2023, GPL v3, KIT/IMK-AAF
(Karlsruhe Institute of Technology, Institute of Meteorology and
Climate Research, Atmospheric Aerosol Research)
For full license details see the file LICENSE.md or
go to https://www.gnu.org/licenses/gpl-3.0.html.
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from scipy.optimize import curve_fit

import py_aida

# from py_aida.wall_flux.calc_aida_wall_flux import wall_flux_temp_fluiddynamic
# from py_aida.wall_flux.calc_aida_wall_flux import wall_flux_h2o_fluiddynamic
# from py_aida.wall_flux.calc_aida_wall_flux import wall_flux_temp_cotton2007
# from py_aida.wall_flux.calc_aida_wall_flux import wall_flux_h2o_cotton2007


class WallFluxFitter(object):
    """Derive fit parameters fot AIDA wall flux."""
    
    def __init__(self, parameterization=None):
        self.parameterization = parameterization
    
    def fit_temperature_flux(self, input_dataframe, parameterization=None):
        """Fit temperature wall flux for AIDA chamber."""
        if not parameterization:
            parameterization = self.parameterization
            
        target_curve = input_dataframe.tg
        
        if parameterization == 'fluiddynamic':
            def wrap_wall_flux_temp_fluiddynamic(X: pd.DataFrame, factor: float) -> pd.Series:
                if not all([column in X.keys() for column in ['tg', 'tg_adiabatic', 'tw', 'press', 'c_p', 'dt']]):
                    raise KeyError("Not all necessary keys in input DataFrame.")
                
                dT_dt = py_aida.wall_flux.calc_aida_wall_flux.wall_flux_temp_fluiddynamic(
                    X.tg, X.tw, X.press, X.c_p, X.dt, factor=factor)
                return X.tg_adiabatic + dT_dt.cumsum()
            fit_function = wrap_wall_flux_temp_fluiddynamic
        
        elif parameterization == 'Cotton2007':
            def wrap_wall_flux_temp_cotton2007(X, k_temp):
                if not all([column in X.keys() for column in ['tg', 'tg_adiabatic', 'tw', 'c_p', 'dt']]):
                    raise KeyError("Not all necessary keys in input DataFrame.")
                dT_dt = py_aida.wall_flux.calc_aida_wall_flux.wall_flux_temp_cotton2007(
                    X.tg, X.tw, X.c_p, X.dt, k_temp=k_temp)
                return X.tg_adiabatic + dT_dt.cumsum()
            fit_function = wrap_wall_flux_temp_cotton2007
            
        else:
            raise ValueError('You chose an invalid parameterization.')
            
        return curve_fit(fit_function, input_dataframe, target_curve)
        
    def fit_water_flux(self, input_dataframe, parameterization=None):
        """Fit water wall flux for AIDA chamber."""
        if not parameterization:
            parameterization = self.parameterization
            
        target_curve = input_dataframe.p_h2o_undiluted
    
        if parameterization == 'fluiddynamic':
            def wrap_wall_flux_h2o_fluiddynamic(X, ice_coverage_total):
                if not all([column in X.keys() for column in ['tg', 'tw', 'press', 'p_h2o', 'dt', 'factor']]):
                    raise KeyError("Not all necessary keys in input DataFrame.")
                dpw_dt = py_aida.wall_flux.calc_aida_wall_flux.wall_flux_h2o_fluiddynamic(
                    X.tg, X.tw, X.press, X.p_h2o, dt=X.dt, factor=X.factor, ice_coverage_total=ice_coverage_total)
                p_h2o_0 = X.p_h2o.iloc[0]
                return p_h2o_0 + dpw_dt.cumsum()
            fit_function = wrap_wall_flux_h2o_fluiddynamic
        
        elif parameterization == 'Cotton2007':
            def wrap_wall_flux_h2o_cotton2007(X, k_h2o):
                if not all([column in X.keys() for column in ['tg', 'tw', 'press', 'p_h2o', 'dt']]):
                    raise KeyError("Not all necessary keys in input DataFrame.")
                dpw_dt = py_aida.wall_flux.calc_aida_wall_flux.wall_flux_h2o_cotton2007(
                    X.tg, X.tw, X.press, X.p_h2o, X.dt, k_vapor=k_h2o)
                p_h2o_0 = X.p_h2o.iloc[0]
                return p_h2o_0 + dpw_dt.cumsum()
            fit_function = wrap_wall_flux_h2o_cotton2007
            
        else:
            raise ValueError('You chose an invalid parameterization.')
            
        return curve_fit(fit_function, input_dataframe, target_curve)
    

# %% def fit function


def get_aida_dataset(campaign, exp_num):
    """Read AIDA dataset from file, or alternativly fetch from databse."""
    
    # import experiment data
    exp_num = str(exp_num).zfill(2)
    data_file = f'./DataSets/calc_aida_all/{campaign}/{campaign}_{exp_num}_aida.pkl'
    if os.path.isfile(data_file):
        ex = pd.read_pickle(data_file)
    
    else:
        from py_aida import tools_import
        from py_aida.calc_aida_all import calc_h2o_pw_offset_correction
        ex = dict()
        query = f"SELECT * FROM in_ergebnisse WHERE (Kampagne = '{campaign}') AND (ExperimentNr = {exp_num})"
        ex['in_ergebnisse'] = tools_import.sql_query('aida', query, output_mode='dict')
        
        query = f"SELECT * FROM experimentliste WHERE (Kampagne = '{campaign}') AND (ExperimentNr = {exp_num})"
        ex['experimentliste'] = tools_import.sql_query('aida', query, output_mode='dict')
        
        from py_aida.calc_aida_all import calc_T_adiabatic
        ex['pT'] = tools_import.read_pT_seclogger(campaign, exp_num)
        ex['pT'].loc[:, 'tg_adiabatic'] = calc_T_adiabatic(ex['pT'].p,
                                                           ex['pT'].loc[ex['experimentliste']['RefZeit'], 'tg'],
                                                           ex['pT'].loc[ex['experimentliste']['RefZeit'], 'p'])
        
        # import total water measurement
        if isinstance(ex['in_ergebnisse']['key_h2o_total'], str):
            if ex['in_ergebnisse']['key_h2o_total'].lower() == 'mbw':
                ex['h2o_total'] = tools_import.read_h2o_total_mbw(campaign, ex['experimentliste']['ExpStart'], ex['experimentliste']['ExpEnde'])
            elif ex['in_ergebnisse']['key_h2o_total'].lower() == 'apet':
                ex['h2o_total'] = tools_import.read_h2o_total_apet(campaign, ex['experimentliste']['ExpStart'], ex['experimentliste']['ExpEnde'], ex['pT'].tg)
        elif ex['in_ergebnisse']['key_h2o_total'] == 0:
            print("WARNING: Flag 'key_h2o_total' in aida.in_ergebnisse "
                  "is zero. No import of total humidity data!")
        else:
            raise ValueError(f"'key_h2o_total' in database table 'aida.in_ergebnisse' not specified or invalid! (Experiment {campaign}#{exp_num})")
            
        # import TDL data
        if isinstance(ex['in_ergebnisse']['key_h2o'], str):
            if ex['in_ergebnisse']['key_h2o'].lower() == 'apict' and ex['in_ergebnisse']['modus_tdl'] == 1:
                ex['h2o'] = tools_import.read_h2o_apict(campaign, ex['experimentliste']['ExpStart'], ex['experimentliste']['ExpEnde'])
            elif ex['in_ergebnisse']['key_h2o'].lower() == 'spapict' and ex['in_ergebnisse']['modus_tdl'] == 1:
                ex['h2o'] = tools_import.read_h2o_spapict(campaign, ex['experimentliste']['ExpStart'], ex['experimentliste']['ExpEnde'])
        elif ex['in_ergebnisse']['key_h2o'] == 0:
            print("WARNING: Flag 'key_h2o_total' in aida.in_ergebnisse is zero. No import of TDL humidity data!")
        else:
            raise ValueError(f"'key_h2o' or 'modus_tdl' in database table 'aida.in_ergebnisse' not specified or invalid! (Experiment {campaign}#{exp_num})")
        ex['h2o'] = calc_h2o_pw_offset_correction(ex['h2o'], ex['h2o_total'], ex['experimentliste']['RefZeit'], dt_average_time=-60, verbose=False)
    
    return ex


def fit_wall_flux(campaign, exp_num, absolute_sigma=True, parameterization='fluiddynamic'):
    """
    Fit parameters for AIDA temperature flux and water flux.
    
    Fit factor for heat flux `popt_temp`, represents the influence of forced convection.
    Fit factor for water flux `popt_h2o` represents AIDA wall ice coverage.

    Parameters
    ----------
    campaign : str
        Campaign name, e.g. 'AWICIT04'
    exp_num : str or int
        Experiment number corresponding to `campaign`, e.g. '05' or '5' or 5 or 5.0
    absolute_sigma : bool, optional
        Keyword to be passed to `scipy.optimize.curve_fit()`.
    parameterization: str, optional
        Choose the parameterization to calculate wall flux.
        Possible strings are 'fluiddynamic' or 'Cotton2007'.
        Default is 'fluiddynamic'.

    Raises
    ------
    ValueError
        Raised when flags for water measurement devices in database are not set properly.

    Returns
    -------
    fit_results : dict
        Contains
        
        * basic experiment information
        * fitted parameters
        * statistical values about the goodness of fit.

    """
    import warnings
    
    exp_num = str(exp_num).zfill(2)
    ex = get_aida_dataset(campaign, exp_num)
    
    # plausibility check to apply water wall flux only on reference activations
    if 'ref' not in ex['experimentliste']['Typ'].lower():
        warn_msg = ("Fitting water wall flux makes only sense for reference activations, e.g. when no additional water sink like ice nucleation exists. "
                    f"IN type `{ex['experimentliste']['Typ']}` indicates experiment {campaign}#{exp_num} is not a reference activation!")
        warnings.warn(warn_msg, UserWarning)
    
    # define time interval for data to be fitted
    t0 = ex['experimentliste']['RefZeit']
    t1 = ex['pT']['p'].loc[t0:].idxmin()
    if t1 > max(ex['h2o'].index):  # prevent chamber cleaning to be interpreted as pressure minimum
        t1 = max(ex['h2o'].index)
    
    # combine all variables used for fit functions
    input_vars = pd.DataFrame()
    input_vars['tg'] = ex['pT']['tg']
    input_vars['tw'] = ex['pT']['tw']
    input_vars['press'] = ex['pT']['p']
    input_vars['tg_adiabatic'] = ex['pT']['tg_adiabatic']
    if 'pw_corr' in ex['h2o_total'].keys():
        pw = ex['h2o_total']['pw_corr']
    else:
        pw = ex['h2o_total']['pw']
    input_vars['p_h2o'] = pw
    # pd.Series(savgol_filter(pw, 41, 3), index=pw.index)  # TODO: the smoothing parameters need to be justified!
    input_vars['p_h2o_undiluted'] = ex['h2o_total'].pw * ex['pT']['p'].loc[t0] / ex['pT']['p']
    input_vars['c_p'] = py_aida.tools_parametrizations.heat_capacity_air_humid(
        ex['pT']['tg'], ex['pT']['p'], input_vars['p_h2o'])
    input_vars['dt'] = pd.Series(ex['pT'].index, index=ex['pT'].index).diff().dt.total_seconds().fillna(0)
    
    input_vars = input_vars.loc[t0: t1]
        
    # fit wall fluxes
    wall_flux_fitter = WallFluxFitter(parameterization=parameterization)
    popt_temp, pcov_temp = wall_flux_fitter.fit_temperature_flux(input_vars)
    input_vars['factor'] = popt_temp[0]
    popt_h2o, pcov_h2o = wall_flux_fitter.fit_water_flux(input_vars)
    
    # derive wall fluxes from fitted parameters
    if parameterization == 'fluiddynamic':
        wall_flux_temp = py_aida.wall_flux.calc_aida_wall_flux.wall_flux_temp_fluiddynamic(
            temp_gas=input_vars['tg'],
            temp_wall=input_vars['tw'],
            press=input_vars['press'],
            c_p=input_vars['c_p'],
            dt=input_vars['dt'],
            factor=popt_temp[0])
        wall_flux_h2o = py_aida.wall_flux.calc_aida_wall_flux.wall_flux_h2o_fluiddynamic(
            temp_gas=input_vars['tg'],
            temp_wall=input_vars['tw'],
            press=input_vars['press'],
            p_h2o=input_vars['p_h2o'],
            dt=input_vars['dt'],
            factor=popt_temp[0],
            ice_coverage_total=popt_h2o[0])
    elif parameterization == 'Cotton2007':
        wall_flux_temp = py_aida.wall_flux.calc_aida_wall_flux.wall_flux_temp_cotton2007(
            temp_gas=input_vars['tg'],
            temp_wall=input_vars['tw'],
            c_p=input_vars['c_p'],
            dt=input_vars['dt'],
            k_temp=popt_temp[0])
        wall_flux_h2o = py_aida.wall_flux.calc_aida_wall_flux.wall_flux_h2o_cotton2007(
            temp_gas=input_vars['tg'],
            temp_wall=input_vars['tw'],
            press=input_vars['press'],
            p_h2o=input_vars['p_h2o'],
            dt=input_vars['dt'],
            k_vapor=popt_h2o[0])
    
    # derive modeled curves from wall flux
    tg_model = input_vars['tg_adiabatic'] + wall_flux_temp.cumsum()
    h2o_model = input_vars['p_h2o'].loc[t0] + wall_flux_h2o.cumsum()
    
    # extend time series data with modeled curves
    input_vars['wall_flux_temp'] = wall_flux_temp
    input_vars['wall_flux_h2o'] = wall_flux_h2o
    input_vars['tg_model'] = tg_model
    input_vars['h2o_model'] = h2o_model
    
    # evaluate goodness of fit
    fit_err_temp = py_aida.toolbox.evaluate_fit(
        y=input_vars['tg'], y_model=tg_model, n_params=1)
    fit_err_h2o = py_aida.toolbox.evaluate_fit(
        y=input_vars['p_h2o_undiluted'], y_model=h2o_model, n_params=1)
    if fit_err_temp['R_square'] < 0.9:
        warn_msg = f"R_square of the temperature wall flux fit parameter is only {round(fit_err_temp['R_square'], 2)}!"
        warnings.warn(warn_msg, UserWarning)
    if fit_err_h2o['R_square'] < 0.9:
        warn_msg = f"R_square of the water wall flux fit parameter is only {round(fit_err_h2o['R_square'], 2)}!"
        warnings.warn(warn_msg, UserWarning)
       
    fit_results = dict()
    fit_results['campaign'] = campaign
    fit_results['exp_num'] = exp_num
    fit_results['parameterization'] = parameterization
    fit_results['RefZeit'] = ex['experimentliste']['RefZeit']
    fit_results['t_end'] = t1
    fit_results['pump_power'] = ex['in_ergebnisse']['Pumpstaerke']
    fit_results['tg_0'] = ex['in_ergebnisse']['tgas_0']
    
    fit_results['popt_temp'] = popt_temp
    fit_results['pcov_temp'] = pcov_temp
    fit_results['popt_h2o'] = popt_h2o
    fit_results['pcov_h2o'] = pcov_h2o
    
    fit_results['fit_evaluation_temp'] = fit_err_temp
    fit_results['fit_evaluation_h2o'] = fit_err_h2o
    
    # extend fit dataset with modeled curves
    fit_results['dataset'] = input_vars
    
    return fit_results
    

def plot_compare_fit_wall_flux(fit_results: dict):
    """
    Compare experimental data with wall flux fit.

    Parameters
    ----------
    fit_results : dict
        Contains all fit results and time depended data which was used for fitting.

    See Also
    --------
        `fit_wall_flux()` creates the `fit_results` input dictionary.

    Returns
    -------
    fig : matplotlib.figure.Figure
        A figure showing a graphical comparison between experimental and modelled wall flux.
        
    """
    from toolbox import get_function_boundaries
    
    if fit_results['parameterization'] == 'fluiddynamic':
        def model_temp(data, factor):
            return (data['tg_adiabatic']
                    + py_aida.wall_flux.calc_aida_wall_flux.wall_flux_temp_fluiddynamic(
                        temp_gas=data['tg'], temp_wall=data['tw'], press=data['press'], c_p=data['c_p'], dt=data['dt'], factor=factor).cumsum())
        
        def model_h2o(data, factor, ice_coverage_total):
            return (data['p_h2o'].iloc[0]
                    + py_aida.wall_flux.calc_aida_wall_flux.wall_flux_h2o_fluiddynamic(
                        temp_gas=data['tg'], temp_wall=data['tw'], press=data['press'], p_h2o=data['p_h2o'], dt=data['dt'], factor=factor, ice_coverage_total=ice_coverage_total).cumsum())
    
    elif fit_results['parameterization'] == 'Cotton2007':
        def model_temp(data, k_temp):
            return (data['tg_adiabatic']
                    + py_aida.wall_flux.calc_aida_wall_flux.wall_flux_temp_cotton2007(
                        temp_gas=data['tg'], temp_wall=data['tw'], c_p=data['c_p'], dt=data['dt'], k_temp=k_temp).cumsum())
    
        def model_h2o(data, k_vapor):
            return (data['p_h2o'].iloc[0]
                    + py_aida.wall_flux.calc_aida_wall_flux.wall_flux_h2o_cotton2007(
                        temp_gas=data['tg'], temp_wall=data['tw'], press=data['press'], p_h2o=data['p_h2o'], dt=data['dt'], k_vapor=k_vapor).cumsum())
    
    data = fit_results['dataset']  # all time dependend data which was used for fitting
    
    # extract fit parameters from `fit_results`
    params_temp = fit_results['popt_temp']
    params_temp_err = np.sqrt(fit_results['pcov_temp'].diagonal())
    if fit_results['parameterization'] == 'fluiddynamic':
        params_h2o = [fit_results['popt_temp'][0], fit_results['popt_h2o'][0]]
        params_h2o_err = [np.sqrt(fit_results['pcov_temp'].diagonal()[0]), np.sqrt(fit_results['pcov_h2o'].diagonal()[0])]
    elif fit_results['parameterization'] == 'Cotton2007':
        params_h2o = [fit_results['popt_h2o'][0]]
        params_h2o_err = [np.sqrt(fit_results['pcov_h2o'].diagonal()[0])]
    
    # calculate max deviation of modeled temp/water vapor due to standard deviation of fit parameters
    bounds_temp = get_function_boundaries(fct=model_temp, x=data, params=params_temp, params_err=params_temp_err)
    bounds_h2o = get_function_boundaries(fct=model_h2o, x=data, params=params_h2o, params_err=params_h2o_err)
    
    # calculate residuals
    rel_residual_temp = (data['tg_model'] / data['tg'] - 1) * 100
    rel_residual_h2o = (data['h2o_model'] / data['p_h2o_undiluted'] - 1) * 100
    abs_residual_temp = data['tg_model'] - data['tg']
    abs_residual_h2o = data['h2o_model'] - data['p_h2o_undiluted']
    
    # PLOT
    fig, axes = plt.subplots(figsize=[14, 10], nrows=2, ncols=2, sharex=True)
    (axTall, axh2oall, axTcomp, axh2ocomp) = axes.flatten()
    fig.subplots_adjust(hspace=0.35, wspace=0.35)
    fig.suptitle(f"Fit results for wall flux (parameterization='{fit_results['parameterization']}')\n{fit_results['campaign']}, no. {fit_results['exp_num']}")
    for ax in axes.flatten():
        ax.set_xlabel("Time")
    
    # define plot colors for residuals twinx
    color_cycle = list(plt.rcParams['axes.prop_cycle'])
    color_rel_residal = color_cycle[0]['color']
    color_abs_residal = color_cycle[1]['color']
    color_model = 'r'
    
    # Subplot: temperature curves
    axTall.set_ylabel("Temperature T [K]")
    axTall.set_title("Temperature, observed and model")
    axTall.plot(data['tg'], label='T$_{gas}$')
    axTall.plot(data['tw'], label='T$_{wall}$')
    axTall.plot(data['tg_adiabatic'], label='T$_{adiab}$')
    axTall.plot(data['tg_model'], lw=4, c=color_model, alpha=0.5,
                label="T$_{adiab}$ + heat wall flux\n ($x_{fit}^{temp}$" + f" = {round(fit_results['popt_temp'][0], 2)})")
    axTall.fill_between(bounds_temp.index, y1=bounds_temp.lower, y2=bounds_temp.upper, color='grey', label=r'model +/- 1$\sigma_x$', alpha=0.5)
    axTall.legend()
    
    # Subplot: water vapor curves
    axh2oall.set_title("Water vapor pressure, observed and model")
    axh2oall.set_ylabel("Water vapor pressure p$_{H2O}$ [Pa]")
    axh2oall.plot(data['p_h2o_undiluted'], label='p$_{w}^{MBW}$ (undiluted)')
    axh2oall.plot(data['h2o_model'], lw=4, c=color_model, alpha=0.5,
                  label="p$_{w,t0}^{MBW}$ + water wall flux\n($x_{fit}^{H2O}$" + f" = {round(fit_results['popt_h2o'][0], 2)})")
    axh2oall.fill_between(bounds_h2o.index, bounds_h2o.upper, bounds_h2o.lower, color='grey', label=r'model +/- 1$\sigma_{x_{i}}$', alpha=0.5)
    axh2oall.legend()
    
    # Subplot: temperature residuals
    axTcomp.set_title("Temperature residuals (observed vs model)")
    axTcomp.set_ylabel(r'Relative $\Delta T$ [\%] (Fit / Exp)', color=color_rel_residal)
    axTcomp.tick_params(axis='y', colors=color_rel_residal)
    axTcomp.plot(data['tg'].index, rel_residual_temp, color=color_rel_residal)
    axTcomp.axhline(0, color='k', ls='--')
    
    axTcomp_twin = axTcomp.twinx()
    axTcomp_twin.set_ylabel(r"Absolute $\Delta T$ [K] (Fit - Exp)", color=color_abs_residal)
    axTcomp_twin.tick_params(axis='y', colors=color_abs_residal)
    axTcomp_twin.plot(data['tg'].index, abs_residual_temp, color=color_abs_residal)
    
    # Subplot: water vapor residuals
    axh2ocomp.set_title("Water vapor residuals (observed vs model)")
    axh2ocomp.set_ylabel(r'Relative $\Delta$ p$_{H2O}$ [\%] (Fit / Exp)', color=color_rel_residal)
    axh2ocomp.tick_params(axis='y', colors=color_rel_residal)
    axh2ocomp.plot(data['p_h2o_undiluted'].index, rel_residual_h2o, color=color_rel_residal)
    axh2ocomp.axhline(0, color='k', ls='--')
    
    axh2ocomp_twin = axh2ocomp.twinx()
    axh2ocomp_twin.set_ylabel(r'Absolute $\Delta$ p$_{H2O}$ [Pa] (Fit - Exp)', color=color_abs_residal)
    axh2ocomp_twin.tick_params(axis='y', colors=color_abs_residal)
    axh2ocomp_twin.plot(data['p_h2o_undiluted'].index, abs_residual_h2o, color=color_abs_residal)
    
    # Print table about goodness of fit
    ax_table = fig.add_axes(rect=[0.3, -0.25, 0.5, 0.25])  # [left, bottom, width, height]
    ax_table.set_title('Goodness of fit measures')
    ax_table.axis('off')  # Hide axes
    
    table_content = pd.concat([pd.Series(fit_results['fit_evaluation_temp'], name='temp'),
                               pd.Series(fit_results['fit_evaluation_h2o'], name='H$_2$O')],
                              axis=1)
    
    ax_table.table(
        cellText=table_content.round(4).values,
        colLabels=table_content.columns,
        rowLabels=table_content.index,
        colColours=plt.cm.BuPu(np.full(len(table_content.columns), 0.1)),
        rowColours=plt.cm.BuPu(np.full(len(table_content.index), 0.1)),
        loc='upper center',
        cellLoc='center',)
    
    import matplotlib.dates as mdates
    for ax in fig.axes:
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
        ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))
        plt.setp(ax.get_xticklabels(), ha="right", rotation=45)
        
    return fig


# %% example call
    
# campaign = 'TROPIC02'
# exp_num = '16'

# for parameterization in ['fluiddynamic', "Cotton2007"]:
#     fit_results = fit_wall_flux(campaign, exp_num, parameterization=parameterization)
#     fig = plot_compare_fit_wall_flux(fit_results)
#     fig.show()
