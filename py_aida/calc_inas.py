#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Calculate ice nucleation active site (INAS) density n_s for an AIDA experiment.

This file is part of the project `py_aida`.
Copyright (c) 2019-2023, GPL v3, KIT/IMK-AAF
(Karlsruhe Institute of Technology, Institute of Meteorology and
Climate Research, Atmospheric Aerosol Research)
For full license details see the file LICENSE.md or
go to https://www.gnu.org/licenses/gpl-3.0.html.
"""
import warnings
import pandas as pd
import numpy as np
from scipy.optimize import minimize
from scipy.signal import savgol_filter

import matplotlib.pyplot as plt
import matplotlib.dates as mdates


# %% Class to manipulate ice number concentration data

class Concentration(pd.Series):
    """Contains particle concentration data with extra methods for data manipulation."""
    
    def floor_DatetimeIndex(self, scale='s'):
        """Reduce DatetimeIndex accuracy to `scale`."""
        assert type(self.index) == pd.DatetimeIndex
        self.index = pd.DatetimeIndex(self.index).floor(scale)
        return self
    
    def smooth(self, window_length=9, polyorder=1):
        """Use Savitzky-Golay filter for smoothing."""
        self = self.dropna()
        self.loc[:] = savgol_filter(self, window_length, polyorder)
        return Concentration(self)
    
    def eliminate_dilution_effect(self, p, p0):
        """Correct concentration data for dilution effect."""
        self.loc[:] = self * p0 / p.reindex(self.index)
        return self
    
    def remove_nonmonotonic_datapoints(self):
        """Remove datapoints which are smaller than the previous datapoints."""
        x = self.to_numpy()
        idx_to_be_deleted = list()
        max_val = 0
        for i in range(len(x)):
            if x[i] >= max_val:
                max_val = x[i]
            elif x[i] < max_val:
                idx_to_be_deleted.append(i)
        self = self.drop(self.index[idx_to_be_deleted])
        
        assert self.is_monotonic_increasing
        return Concentration(self)


# %% functions to be called in INAS calculation

def get_time_interval_of_heterogeneous_freezing(ex):
    """Retrieve time interval of homogeneous ice formation."""
    t0 = ex['experimentliste']['RefZeit']
    t_exp_end = ex['experimentliste']['ExpEnde']
    
    if ex['freezing_mode'] == 'het':
        t1 = ex['h2o'].loc[t0: t_exp_end, 'RHi_smooth'].idxmax() + pd.Timedelta(seconds=10)
    elif ex['freezing_mode'] == 'mixed':
        t1 = t0 + pd.Timedelta(seconds=ex['t']['t_ice_onset_hom'])
    elif ex['freezing_mode'] == 'hom':
        warnings.warn("No INAS density calculated, since there is no heterogeneous nucleation", UserWarning)
        t1 = False
    return (t0, t1)


def calc_binned_n_ice_from_n_s(dN, dA, n_s):
    """Calculate het. ice formed according to INAS density."""
    single_aerosol_surface = dA / dN
    return dN * (1 - np.exp(-n_s * single_aerosol_surface))


def calc_n_s_with_linear_apprixmation(n_ice_exp, aerosol_surface):
    """Calculate INAS density approximated for very low ice fractions."""
    return (n_ice_exp / aerosol_surface)


def reduce_aerosol_sd_bin_wise_according_to_n_s(dN, dA, n_s_opt):
    """Change aerosol SD wrt bin-wise frozen aerosol."""
    binned_n_ice = calc_binned_n_ice_from_n_s(dN, dA, n_s_opt)
    reduced_dN = dN - binned_n_ice
    return reduced_dN


def reduce_aerosol_sd_by_freezing_biggest_size(dN_old, dN_to_subtract):
    """Change aerosol SD assuming biggest aerosol freeze first."""
    dN = dN_old.copy()
    
    while dN_to_subtract > 0 and dN.sum() > 0:
        
        if dN_to_subtract > dN.sum():
            warnings.warn(f"WARNING! {dN_to_subtract} more ice crystals than aerosol particles!", Warning)

        j = dN[dN > 0].index.max()  # largest_bin_idx
        N_in_largest_bin = dN.loc[j]
        
        if N_in_largest_bin < dN_to_subtract:  # standard case
            dN.loc[j] = 0
            dN_to_subtract -= N_in_largest_bin
        elif N_in_largest_bin >= dN_to_subtract:  # final case
            dN.loc[j] -= dN_to_subtract
            dN_to_subtract = 0
    return dN


# %% main functions

def get_lognormal_sd_params_awicit(aerosol_type):
    """Lognormal averaged SD parameters for certain aerosol types."""
    # remark: derived by plot_Dms.py on 2019-11-20 15:40
    sd_params = dict()
    sd_params['Sil'] = {'Dm': 0.1005e-6, 'sigma': 1.7828}
    sd_params['BCR'] = {'Dm': 0.3004e-6, 'sigma': 1.8162}
    sd_params['CaCO3'] = {'Dm': 0.2906e-6, 'sigma': 1.9730}
    
    for ae in sd_params.keys():
        if ae in aerosol_type:
            aerosol_type = ae
    
    return (sd_params[aerosol_type]['Dm'], sd_params[aerosol_type]['sigma'])


def aerosol_size_distribution(n_ae, Dm, sigma, min_size=1e-9, max_size=1e-5, bin_number=200):
    """Generate aerosol size distributions (number and surface), assuming spherical particles."""
    
    def lognormal_distribution(x, mu, sig):
        """Lognormal distribution with median `mu` and geometric standard deviation `sig`."""
        return ((1 / (np.log10(sig) * np.sqrt(2 * np.pi)))
                * np.exp(-((np.log10(x / mu))**2) / (2 * np.log10(sig)**2)))

    bin_boundaries = pd.Series(np.logspace(np.log10(min_size), np.log10(max_size), bin_number + 1))  # upper boundaries for bin
    sd = pd.DataFrame()
    sd['lower_bins'] = bin_boundaries
    sd['upper_bins'] = bin_boundaries.shift(-1)
    sd['bin_mean_diameter'] = (sd['upper_bins'] + sd['lower_bins']) / 2
    sd = sd.drop(sd.index[-1])
        
    sd['dNdlogD'] = n_ae * lognormal_distribution(sd['bin_mean_diameter'], Dm, sigma)
    sd['dAdlogD'] = 4 * np.pi * (sd['bin_mean_diameter'] / 2)**2 * sd['dNdlogD']
    sd['dN'] = sd['dNdlogD'] * (np.log10(sd['upper_bins']) - np.log10(sd['lower_bins'].replace(0, np.nan)))
    sd['dA'] = sd['dAdlogD'] * (np.log10(sd['upper_bins']) - np.log10(sd['lower_bins'].replace(0, np.nan)))
    return sd


def calc_inas(ex, sd, device='welas1', t_normalize=None,
              inas_mode='exact', adjust_sd_mode='bin_wise_by_inas',
              plots=False):
    """
    Calculate time resolved INAS density for AIDA experiments.

    Parameters
    ----------
    ex : dict
        AIDA data from .pkl file generated by calc_aida_all().
    sd : pd.DataFrame
        Aerosol size distribution.
    device : str
        Select opc type. 'welas1' or 'welas2'
    t_normalize: pd.Timestamp or int
        Define a timestamp on which to normalize the ice number
        concentration and start the analysis.
        You can pass a pd.Timestamp or an integer which is interpreted as absolute seconds from the experiment start.
    inas_mode : str, optional
        Define how to calculate n_s.
        Choose between `'exact'` and `'linear_approximation'`
        The default is `'exact'`.
    adjust_sd_mode : str, optional
        Define how the aerosol size distribution gets affected from freezing.
        Choose between `'bin_wise_by_inas'`, `'biggest_freeze_first'` or `None`
        The default is `'bin_wise_by_inas'`.
    plots : bool, optional
        Define if plots should be generated. The default is False.

    Raises
    ------
    ValueError
        if an invalid input was given for `inas_mode` or `adjust_sd_mode`.

    Returns
    -------
    pd.DataFrame
        DataFrame over DatetimeIndex with the columns
            * 'n_s': Ice-nucleating active site (INAS) density n_s in [m⁻²]
            * 'tg' : Temperature, in [K].
            * 'S_i': Saturation ratio wrt ice.
        
    See Also
    --------
        * :func:`calc_aida_all.calc_aida_all()` which generates the .pkl file containing experimental AIDA data.
        * :func:`calc_inas.aerosol_size_distribution()` to generate input size distribution `sd`.
    
    Examples
    --------
    >>> ex = pd.read_pickle(f'path/to/{campaign}_{exp_num}_aida.pkl')
    >>> sd = calc_inas.aerosol_size_distribution(n_ae, Dm, sigma, min_size=1e-9, max_size=1e-5, bin_number=200)
    >>> n_s = calc_inas.calc_inas(ex, sd, device='welas1',
                                  inas_mode='exact', adjust_sd_mode='bin_wise_by_inas',
                                  plots=True)
    
    """
    n_ice_exp = Concentration(ex['welas'][device].cn_ice * 1e6)  # m⁻³
    n_ice_exp = n_ice_exp.floor_DatetimeIndex()
    n_ice_exp = n_ice_exp.eliminate_dilution_effect(ex['pT']['p'], ex['in_ergebnisse']['p_0'] * 100)
    n_ice_exp = n_ice_exp.smooth()
    
    (t_start, t_end) = get_time_interval_of_heterogeneous_freezing(ex)
    if t_normalize:
        if isinstance(t_normalize, int):
            t_start = t_start + pd.Timedelta(seconds=t_normalize)
        if isinstance(t_normalize, pd.Timestamp):
            t_start = t_normalize
        nearest_idx = n_ice_exp.index.get_loc(t_start, method='nearest')
        t_start = n_ice_exp.index[nearest_idx]
        n_ice_exp = Concentration(n_ice_exp - n_ice_exp.loc[t_start])
         
    n_ice_exp = Concentration(n_ice_exp.loc[t_start: t_end])
    n_ice_exp = n_ice_exp.remove_nonmonotonic_datapoints()
    
    if any(n_ice_exp > sd.dN.sum()):
        warnings.warn(f"More ice than aerosol particles starting from `{(n_ice_exp > sd.dN.sum()).idxmax()}` "
                      f"(Ratio up to {round(n_ice_exp.max() / sd.dN.sum(), 3)})!", Warning)
    
    n_s_over_time = pd.Series(dtype='float64', name='n_s')  # init var
    n_s_0 = 1e9  # start parameter for n_s, in m⁻²
    for timestamp in n_ice_exp.index:  # time loop
    
        if n_ice_exp.loc[timestamp] > sd.dN.sum():
            warnings.warn('Reached timestamp with more ice than aerosol. Abort determination of ns!')
            break
    
        # calc inas
        if inas_mode == 'exact':
            
            def minimize_n_ice_diff_though_ns(n_s, n_ice_exp_timestamp, dN, dA):
                n_ice_calc = calc_binned_n_ice_from_n_s(dN, dA, n_s).sum()
                n_ice_diff = abs(n_ice_exp_timestamp - n_ice_calc)
                return n_ice_diff
            
            n_ice_exp_timestamp = n_ice_exp.loc[timestamp]
            dN = sd['dN']
            dA = sd['dA']
            
            ns_opt_result = minimize(fun=minimize_n_ice_diff_though_ns,
                                     x0=n_s_0,
                                     args=(n_ice_exp_timestamp, dN, dA),
                                     # bounds=[(1, np.inf)],
                                     method='Nelder-Mead')
            
            popt = ns_opt_result.x
            
            if ns_opt_result.success:
                n_s_0 = ns_opt_result.x[0]
                n_s_over_time.loc[timestamp] = ns_opt_result.x[0]
            else:
                warnings.warn(f"INAS fit didn't succeed at timestamp {timestamp} "
                              f"with error `{ns_opt_result.message}`")

        elif inas_mode == 'linear_approximation':

            n_s_over_time.loc[timestamp] = calc_n_s_with_linear_apprixmation(n_ice_exp.loc[timestamp], sd['dA'].sum())
            popt = [n_s_over_time.loc[timestamp]]
            
        else:
            raise ValueError("You passed an invalid argument for `inas_mode`.")
        
        # reduce aerosol sd for frozen aerosol particles
        if adjust_sd_mode == 'bin_wise_by_inas':
            sd['dN'] = reduce_aerosol_sd_bin_wise_according_to_n_s(sd['dN'], sd['dA'], popt[0])
        elif adjust_sd_mode == 'biggest_freeze_first':
            dn_ice_binned = calc_binned_n_ice_from_n_s(sd.dN, sd.dA, popt[0])
            sd['dN'] = reduce_aerosol_sd_by_freezing_biggest_size(sd['dN'], dn_ice_binned.sum())
        elif adjust_sd_mode is None:
            pass
        else:
            msg = ("You passed an invalid argument for `adjust_sd_mode` or "
                   "combined `inas_mode='linear_approximation'` with a bin-wise"
                   "freezing of the aerosol size distribution.")
            raise ValueError(msg)
        
        # use dN to derive all other SD measures for each time step
        sd['dNdlogD'] = sd['dN'] / (np.log10(sd['upper_bins']) - np.log10(sd['lower_bins']))
        sd['dAdlogD'] = 4 * np.pi * (sd['bin_mean_diameter'] / 2)**2 * sd['dNdlogD']
        sd['dA'] = sd['dAdlogD'] * (np.log10(sd['upper_bins']) - np.log10(sd['lower_bins']))
        
        if plots:
            vmin = mdates.date2num(n_ice_exp.index.min())
            vmax = mdates.date2num(n_ice_exp.index.max())
            norm = plt.Normalize(vmin=vmin, vmax=vmax)
            cmap = plt.get_cmap('viridis').copy()
            
            plt.semilogx(sd.bin_mean_diameter, sd.dNdlogD,
                         color=cmap(norm(mdates.date2num(timestamp))),
                         alpha=0.5)
            
    if plots:
        plt.title(f"Temporal evolution of aerosol size distribution \n{ex['Campaign']} no. {str(ex['ExpNo']).zfill(2)}")
        plt.ylabel('dN/dlog(D) / (m$^{-3}$)')
        plt.xlabel('Particle diameter D / (µm)')
        plt.show()
        plt.close()
    
    if plots:
        plt.plot(np.log10(n_s_over_time), ls='--', marker='o')
        plt.title(f"n$_s$ over time \n{ex['Campaign']} no. {str(ex['ExpNo']).zfill(2)}")
        plt.ylabel("log$_{10}$(n$_s$) / (m$^{-3}$)")
        plt.xlabel("Time")
        plt.show()
        plt.close()
    
    tg = ex['pT'].tg.reindex(n_s_over_time.index)
    S_i = (ex['h2o'].RHi_smooth.rename('S_i') / 100).reindex(n_s_over_time.index)
    combined_output = pd.concat([n_s_over_time, tg, S_i], axis=1, join='inner')
    
    if plots:
        ns_scatter = plt.scatter(tg, S_i, c=np.log10(n_s_over_time), ec='k',
                                 label='log$_{10}$(n$_s$)')
        plt.title(f"n$_s$ in the S$_i$/T parameter space \n{ex['Campaign']} no. {str(ex['ExpNo']).zfill(2)}")
        plt.ylabel('S$_{ice}$')
        plt.xlabel('Temperature / (K)')
        plt.colorbar(ns_scatter, label='log$_{10}$(n$_s$) / (m$^{-2}$)')
        plt.legend()
        plt.show()
        plt.close()
    
    return combined_output


# %% call from cli

if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser(prog='calc_inas',
                                     usage='python calc_inas.py [options]',
                                     description='Calculate INAS density of an AIDA experiment')
    
    arg_ex = parser.add_argument('--ex',  # key to define variable from cli
                                 dest='ex',  # attribute name containing input value
                                 help='Path to an .pkl file created by `calc_aida_all.py`',
                                 type=str)
    arg_sigma = parser.add_argument('-s', '--sigma', dest='sigma', type=float,
                                    help="Width of lognormal aerosol size distribution")
    arg_Dm = parser.add_argument('-D', '--Dm', dest='Dm', type=float,
                                 help="Median diameter of lognormal size distribution, in m**-3")
    parser.add_argument('-d', '--device', dest='device', default='welas1',
                        help=("Specify the device to provide experimental ice number concentration.\n"
                              "Default is 'welas1'"))
    parser.add_argument('-im', '--inas_mode', dest='inas_mode', default='exact',
                        help=("Method to calculate ice-nucleating active site density.\n"
                              "Choose between `'exact'` and `'linear_approximation'`. Default is 'exact'."))
    parser.add_argument('-sdm', '--sd_mode', dest='sd_mode', default='bin_wise_by_inas',
                        help=("Select method to adjust the aerosol surface over time.\n"
                              "Choose between `'bin_wise_by_inas'`, `'biggest_freeze_first'` or `None`.\n"
                              "The default is `'bin_wise_by_inas'`."))
    parser.add_argument('-p', '--plots', dest='plots', default=False, type=bool,
                        help=("Choose if plots should be generated. The default is `False`."))
    parser.add_argument('-o', '--output', dest='out_path', default='', type=str,
                        help="Define a path if you want to save the output as csv.")
    args = parser.parse_args()
    
    if not args.ex:
        raise argparse.ArgumentError(arg_ex, "Please specify the path to an experiment .pkl file.")
    if not args.sigma:
        raise argparse.ArgumentError(arg_sigma, "Please specify a valid sigma parameter for lognormal size distribution.")
    if not args.Dm:
        raise argparse.ArgumentError(arg_Dm, "Please specify a valid median diameter for lognormal size distribution.")
    
    ex = pd.read_pickle(args.ex)
    sd = aerosol_size_distribution(ex['n_ae'] * 1e6, args.Dm, args.sigma, min_size=1e-9, max_size=1e-5, bin_number=200)
    ns = calc_inas(ex,
                   sd,
                   device=args.device,
                   inas_mode=args.inas_mode,
                   adjust_sd_mode=args.sd_mode,
                   plots=args.plots)
    
    if args.out_path:
        ns.to_csv(args.out_path, sep='\t')
    
    print(ns)  # print result to stdout
