.. py_aida documentation master file, created by
   sphinx-quickstart on Wed Sep 13 11:42:14 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to py_aida's documentation!
===================================

**py_aida** is a python project to analyze experiments performed at the AIDA cloud chamber facility at `KIT <https://www.kit.edu/>`_/`IMK-AAF  <https://www.imk-aaf.kit.edu/>`_.

It was written to replace and extend the priviously used IDL scripts.
The code is hosted on a remote `git repository <https://codebase.helmholtz.cloud/aida/aida_experiment_analysis/>`_ under a GPL v3 license.


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   readme
   modules
   contributing

   
   
..
   _ Exclude the default section about indices and tables.
   _ With the current project size such a section doesn't seem to be necessary.
..
   _Indices and tables
   _==================
..
   _* :ref:`genindex`
   _* :ref:`modindex`
   _* :ref:`search`
